function initialise_front_page(){
   // login start
    $("#frontpage-process-login-button").addClass("disabled");
    $("#frontpage-process-login-button").click(function() {frontpage_check_login();});

    $("#frontpage-input-username").change(function() {frontpage_validate_inputs();});
    $("#frontpage-input-password").change(function() {frontpage_validate_inputs();});
    $("#frontpage-input-username").keydown(function(event) {frontpage_check_login_return_hit(event);});
    $("#frontpage-input-password").keydown(function(event) {frontpage_check_login_return_hit(event);});

    frontpage_check_for_valid_session();

    $("#intro-button").click(function() {
        $("#hidden-tabs a[href='#tab-intro']").tab('show');
    });

    $("#my-data-block").click(function() {
        $( "#main-tabs a[href='#tab-data']" ).tab('show');
    });
    $("#tools-block").click(function() {
        $( "#main-tabs a[href='#tab-tools']" ).tab('show');
    });
    $("#help-block").click(function() {
        $( "#main-tabs a[href='#tab-help']" ).tab('show');
    });

    // $("#help-block").click(function() {
    //     $( "#a[href='https://apply.pawsey.org.au/']" );
    // });

    // $("#task-view-toggle-nav").click(function(){
    //     toggle_task_view();
    // });
}

function frontpage_check_login_return_hit(e){
    //rehide error if the user stasrts typing again
    $("#frontpage-error").addClass("hidden");

    if(e.keyCode == 13){
        frontpage_check_login();
    } else {
        frontpage_validate_inputs();
    }
}

function frontpage_check_login() {
    var path_to_open = $("#hidden_path_to_open").text();
    var loginIsSuccessful = frontpage_livearc_logon($("#frontpage-input-username").val(), $("#frontpage-input-password").val());

    if(!loginIsSuccessful) {
        $("#frontpage-error").removeClass("hidden");
        $("#frontpage-error").addClass("alert alert-danger animated fadeInUp").html("Login failed, please check your username and password");

    }
}

function frontpage_validate_inputs() {

    if ($("#frontpage-input-username").val().length > 0 && $("#frontpage-input-password").val().length > 0){
        $("#frontpage-process-login-button").removeClass("disabled");
    } else {
        $("#frontpage-process-login-button").addClass("disabled");
    }
}

function frontpage_livearc_logon(username, password) {
    // livearc logon routine.  Does an initial login and load the UI with the
    // root directory. hardwired to iVEC domain users.

    var domain = "ivec";

    global_domain = domain;

    // need to add 1. warn if all caps

    var login_status = true;

    try {
        arc.mf.client.RemoteServer.logon(domain,username,password);
    } catch (e) {
        login_status = false;
    }

    //don't need this anymore
    password = null;

    //save the current session ID for later usage.
    if (login_status) {
        var session_token = arc.mf.client.RemoteServer.sessionId();
        document.cookie="sessionid="+session_token;

        check_for_system_message();

        //from datapage.js
        var valid_session = check_for_valid_session();
        if(valid_session) {
            check_for_system_message();
            init_interface_for_path("/");
            $( "#main-tabs a[href='#tab-data']" ).tab('show');
            initialize_interface_after_login(); //datapage.js
        }
        return true;
    }

    return false;
}

function frontpage_check_for_valid_session(){
    var valid_session = simple_check_for_valid_session();
    if(valid_session) {
        $("#frontpage-login-container").addClass("hidden");
        $("#frontpage-loggedin-container").removeClass("hidden");
    } else {
        $("#frontpage-login-container").removeClass("hidden");
        $("#frontpage-loggedin-container").addClass("hidden");

    }
}


function simple_check_for_valid_session() {

    // See if we have a session cookie and if so, test it and update the UI accordingly.

    var status = false;

    var cookie = get_cookie("sessionid");


    if (cookie !== null && cookie.length > 1){
        arc.mf.client.RemoteServer.setSessionId(cookie);

        var command = livearc_execute_handler("system.session.self.describe", null, true);

        if (command === "arc.mf.client.ExNoSession" || command === "arc.mf.server.Services$ExServiceError" || command === "error"){
            //user needs to login, or there's something wrong with the server
            status = false;
        } else {
            var user_data = parse_command_xml(command);
            var username = user_data.session.user;
            $("#frontpage-loggedin-container").empty();
            $("#frontpage-loggedin-container").append("Welcome back,<br><strong>"+username+"</strong>");
            status = true;
        }
    }
    return status;
}
