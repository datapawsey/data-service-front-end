function initialise_profile_page(){
    $(window).resize(function(){profile_check_layout();});
    profile_check_layout();
    setup_profile_tabs();
}


function check_for_applications(){
    // load the application review interface
    // This is loaded is theuser has access to the /requests namespace.  This should only happen if the user has
    // the required role.

    function check_for_applications_callback(response){
        var result = xmlToJson(response._xml);
        if (result.exists["#text"] === "true"){
            $("#application-tool-listentry").removeClass("hidden");
            update_new_application_count();
        }
    }

    function check_for_application_callback_error(response){
        console.log('check_for_application_callback_error',response);
    }

    $("#application-tool-listentry").addClass("hidden");

    var args = new arc.xml.XmlStringWriter();
    args.add("namespace","/requests");
    livearc_async_generic_handler("asset.namespace.exists", args, check_for_applications_callback, check_for_application_callback_error);
}


function check_for_admin_interface() {


    //load the project admin interface is the user has the quote-admin role
    function check_for_admin_interface_callback(response) {

        var user_info = xmlToJson(response._xml);
        var domain = user_info.session.domain["#text"];
        var user = user_info.session.user["#text"];

        var args = new arc.xml.XmlStringWriter();
        args.add("role",["type","role"],"quota-administrator");

        var command = livearc_execute_handler("actor.self.have", args);
        var role_test = xmlToJson(command._xml);

        if (role_test.role["#text"] === "true"){
            $("#report-tool-listentry").removeClass("hidden");
        }
    }

    function check_for_admin_interface_error(response) {
        console.log('check_for_admin_interface_error`',response);
    }

    $("#report-tool-listentry").addClass("hidden");

    // var command = livearc_execute_handler("system.session.self.describe", null);
    var args = new arc.xml.XmlStringWriter();
    livearc_async_generic_handler("system.session.self.describe", args, check_for_admin_interface_callback, check_for_admin_interface_error);
}


function update_profile_page(username) {
    // this function to be fired after login.
    $("#profile-user-name").text("[ "+ username + " ]");
    $("#profile-user-name").attr("data-name",username);
    $("#profile-menu-content li:eq(0)").addClass("active");
    show_project_list();
    // reporting
    check_for_admin_interface();
    check_for_applications();

    $("#generate_reports_button").click(function(){update_project_report();});
    $("#update_reports_button").click(function(){update_project_report();});
    $("#export_reports_button").click(function(){export_project_list_as_csv();});
}

function profile_check_layout() {

    var offset = $("#top-navbar").outerHeight(true);

    if(!$("#system-alert-container").hasClass("hidden")){
        offset = offset + $("#system-alert-container").outerHeight(true);
    }

    $("#profilepage-container").height($("#fullpage").outerHeight(true)-offset);

    $("#project-detail-view").height($("#profilepage-container").outerHeight(true)-1);
    $("#project-user-content").height($("#profilepage-container").outerHeight(true)-1);

    var top_offset = 0;
    if (!$("#project-pi-alert").hasClass("hidden")){
        top_offset = top_offset + $("#project-pi-alert").outerHeight(true);
    }

    //
    top_offset = top_offset + $("#project-title").outerHeight(true);
    top_offset = top_offset + $(".project-tab-container").outerHeight(true);

    //var project_view_height = $("#project-user-content").height() - top_offset - header_height;
    var project_view_height = $("#profilepage-container").outerHeight(true)  - top_offset;
    //
    $("#project-storage-info-view").height(project_view_height);
    $("#project-settings-view").height(project_view_height);
    $("#project-user-list-view").height(project_view_height);
    $("#project-activity-view").height(project_view_height);
    //
    $("#project-activity-table-view").css("max-height",project_view_height-1);
    //
    $("#application-content-container").css("max-height",project_view_height-1);
}

function setup_profile_tabs() {
    $("#content-profile-reports").load("profile-reports.html");
    $("#content-profile-projects").load("profile-projects.html");
    $("#content-profile-connections").load("profile-connections.html");
    $("#content-profile-tasks").load("profile-tasks.html");
    $("#content-profile-applications").load("profile-applications.html");

    // tab shown events
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        // e.target // newly activated tab
        // e.relatedTarget // previous active tab
        var target = $(e.target).attr("href") // activated tab
        if (target == "#tab-profile-applications"){
            update_new_application_list();
        }
    })
}


function show_tab(name){
    var selected_tab = $("#profile-tab-control>a[data-tabname='"+name+"']");

    $(".project-tab").addClass("hidden");
    $("#profile-tab-control>a").removeClass("active");
    selected_tab.addClass("active");

    if (name === "membership"){
        $("#project-user-list-view").removeClass("hidden");
    }
    if (name === "settings"){
        $("#project-settings-view").removeClass("hidden");
    }
    if (name === "activity"){
        $("#project-activity-view").removeClass("hidden");
    }
    if (name === "info"){
        $("#project-storage-info-view").removeClass("hidden");
    }
}



function show_project_list() {
    // get and display all the projects for a user

    $("#profilepage-projectlist").empty();

    var args = new arc.xml.XmlStringWriter();

    args.add("namespace","projects");

    var command = livearc_execute_handler("asset.namespace.list", args);

    var project_list = xmlToJson(command._xml);

    if (project_list.namespace.namespace.length){
        $("#project-list-column").removeClass("hidden");
        $("#project-detail-column").addClass("col-xs-9");
        $("#project-detail-column").removeClass("col-xs-12");
        // more than 1 project
        $.each(project_list.namespace.namespace, function(index, item){
            $("#profilepage-projectlist").append("<li class='project-list-entry'>"+item["#text"]+"</li>");
        });
    } else {
        // just 1 project
        if (project_list.namespace.namespace["#text"]){
            $("#profilepage-projectlist").append("<li class='project-list-entry'>"+project_list.namespace.namespace["#text"]+"</li>");
            $("#profilepage-projectlist li:eq(0)");
            $("#project-list-column").addClass("hidden");
            $("#project-detail-column").removeClass("col-xs-9");
            $("#project-detail-column").addClass("col-xs-12");

            show_project_detail(project_list.namespace.namespace["#text"]);
        } else {
            console.log("show_project_list: some awful thing has happened");
        }
    }

    $(".project-list-entry").click(function(event){show_project_detail_for_event(event);});

}


function current_user_is_principle(project_name){

    var args = new arc.xml.XmlStringWriter();

    args.add("namespace",project_name);

    var command = livearc_execute_handler("authorization.role.namespace.describe", args);

    var authorisation_info = xmlToJson(command._xml);

    if(authorisation_info.namespace){
        if (authorisation_info.namespace["can-administer"]["#text"] === "true"){
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }

    return false;
}

function check_for_principal_user(project_name) {
    //authorization.role.namespace.describe :namespace "Data Team"
    $("#project-pi-alert").empty();

    if (current_user_is_principle(project_name)){
        $("#public-activation-button").removeClass("disabled");
        $("#add-user-button").removeClass("hidden");
        $("#project-pi-alert").removeClass("hidden");
        $("#project-pi-alert").append("<p>You are a Principal Investigator for this project</p>");
        $(".remove-user-button").removeClass("hidden");
    } else {
        $("#public-activation-button").addClass("disabled");
        $("#project-pi-alert").addClass("hidden");
        $("#add-user-button").addClass("hidden");
        $(".remove-user-button").addClass("hidden");
    }

}


function update_project_list_byname(project_name){
    function update_project_detail_callback(response){
        var project = xmlToJson(response._xml);
        // console.log('detail_package',detail_package);
        // var project = detail_package.project;

        // get user list
        update_user_list(project);

    }

    function update_project_detail_error_callback(response) {
        // project detail cannot be retreived.
        // hide everthing, show an error
        $(".project-view-container").addClass("hidden");
        $(".project-error-container").removeClass("hidden")
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("name",project_name);
    livearc_async_generic_handler("project.describe", args, update_project_detail_callback, update_project_detail_error_callback);
}


function update_user_list(project) {
    $("#project-user-list").empty();
    $("#userfilter-dropdown").empty();
    $("#userfilter-dropdown").append("<li><a href='#'>All Users</a></li>");

    var user_is_admin = current_user_is_principle($("#project-title").text());

    if (project.hasOwnProperty("members")){
        if (project.members.hasOwnProperty("role")){

            if (project.members.role.hasOwnProperty("length")){
                $.each(project.members.role, function(index, role_item){

                    var role = role_item["#text"];
                    var role_bits = role.split(":");
                    var user_role = role_bits[1];

                    if (role_item.hasOwnProperty("actor")){
                        if (role_item.actor.length){
                            $.each(role_item.actor, function(actor_index, actor_item){
                                var username = actor_item["#text"];
                                username = username.split(":")[1];
                                if(!username){
                                    username = 'delegate';
                                }
                                var row_data = "<tr class='project-user-entry' data-username='"+username+"'><td class='username'>"+username+"</td><td class='actualname'>-</td><td class='emailaddress'>-</td><td class='accesstype'><span id='role-badge' class='btn btn-xs btn-primary'>"+user_role+"</span></td>";
                                if (user_role === "administer" || !user_is_admin){
                                    row_data = row_data + "<td></td></tr>";
                                } else {
                                    row_data = row_data + "<td><span class='remove-user-button btn btn-xs btn-danger'>remove</span></td></tr>";
                                }

                                if (username != "public"){
                                    $("#project-user-list").append(row_data);
                                    //for audit filter
                                    $("#userfilter-dropdown").append("<li><a href='#''>"+username+"</a></li>");
                                }
                            });
                        } else {
                            var username = role_item.actor["#text"];
                            username = username.split(":")[1];
                            if(!username){
                                username = 'delegate';
                            }
                            var row_data = "<tr class='project-user-entry' data-username='"+username+"'><td class='username'>"+username+"</td><td class='actualname'>-</td><td class='emailaddress'>-</td><td class='accesstype'><span id='role-badge' class='btn btn-xs btn-primary'>"+user_role+"</span></td>";
                            if (user_role === "administer" || !user_is_admin){
                                row_data = row_data + "<td></td></tr>";
                            } else {
                                row_data = row_data + "<td><span class='remove-user-button btn btn-xs btn-danger'>remove</span></td></tr>";
                            }

                            if (username != "public"){
                                $("#project-user-list").append(row_data);
                                //for audit filter
                                $("#userfilter-dropdown").append("<li><a href='#''>"+username+"</a></li>");
                            }
                        }
                    }
                });
            }
        }
    // $("#userfilter-dropdown").append("<li><a href='#''>All Users</a></li>");
    } else {
        $("#project-user-list").append("<tr><td>No users found</td></tr>")
    }

    $(".remove-user-button").popover({
        placement: 'left',
        html:'true',
        title:'Confirmation required',
        content:function() {
            return $('#confirm-user-remove-view').html();
        }
    }).parent().delegate("#confirm-user-removal-button","click", function(e){
        var target_row = $(e.currentTarget).closest('.project-user-entry');

        remove_user(target_row);

        target_row.find(".remove-user-button").html("<i class='fa fa-cog fa-spin'></i> removing");
        $(".remove-user-button").popover("hide");
    }).parent().delegate("#cancel-user-removal-button","click", function(){
        $(".remove-user-button").popover("hide");
    });
}


function remove_user(user_table_entry){
    var project_name = $("#project-title").text();

    function user_removed_success(response){
        update_project_list_byname(project_name);
    }

    function user_removed_failure(response){

    }

    var username = user_table_entry.attr('data-username');
    var args = new arc.xml.XmlStringWriter();
    args.add("name","ivec:"+username);
    args.add("role",["type", "role"], project_name+":readonly");
    args.add("role",["type", "role"], project_name+":readwrite");
    args.add("type","user");
    livearc_async_generic_handler("actor.revoke", args, user_removed_success, user_removed_failure);
}


function add_new_user(event){
    var type = $(event.currentTarget).attr("data-account-type");
    var username = $(event.currentTarget).attr("data-username");

    var project_name = $("#project-title").text();
    function user_removed_success(response){
        update_project_list_byname(project_name);
        $("#add-user-result").removeClass("hidden");
    }

    function user_removed_failure(response){
        console.log('failed',response);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("name","ivec:"+username);
    if (type === "readwrite"){
        args.add("role",["type", "role"], project_name+":readwrite");
    }
    if (type === "readonly"){
        args.add("role",["type", "role"], project_name+":readonly");
    }
    args.add("type","user");
    livearc_async_generic_handler("actor.grant", args, user_removed_success, user_removed_failure);

}

function search_for_possible_user() {
    function user_search_success(response){
        var add_response = xmlToJson(response._xml);
        if (add_response.exists["#text"] === "true"){
            $("#add-user-default-view").addClass("hidden");
            $("#add-user-found-view").removeClass("hidden");
            $("#add-user-notfound-view").addClass("hidden");
            $("#confirm-add-user").attr("data-username",add_response.exists["@attributes"].user);
            $("#confirm-add-user-rw").attr("data-username",add_response.exists["@attributes"].user);
        } else {
            $("#searched-username").text(add_response.exists["@attributes"].user);
            $("#add-user-default-view").addClass("hidden");
            $("#add-user-found-view").addClass("hidden");
            $("#add-user-notfound-view").removeClass("hidden");
        }
    }
    function user_search_error(response){
        $("#add-user-default-view").addClass("hidden");
        $("#add-user-notfound-view").removeClass("hidden");
        $("#add-user-found-view").addClass("hidden");
    }

    $("#add-user-result").addClass("hidden");

    var search_username = $("#input_username").val();

    var args = new arc.xml.XmlStringWriter();
    args.add("domain","ivec");
    args.add("user",search_username);
    livearc_async_generic_handler("user.exists", args, user_search_success, user_search_error);
}

function update_storage_info(project){
    $("#project-storage-quota-bar").empty();
    $("#project-storage-quota-text").empty();
    console.log('project',project);


 // SDF - replacement for broken use/quota reporting
    if (project.hasOwnProperty("usage") && project.hasOwnProperty("quota")){
            var mount_used = project.usage["#text"]/1000000000.0;
            var mount_available = project.quota["#text"]/1000000000.0;
            var percentage = mount_used / mount_available * 100.0;
            $("#project-storage-quota-bar").append("<div class='progress'><div class='progress-bar' role='progressbar' aria-valuenow='"+percentage+"' aria-valuemin='0' aria-valuemax='100' style='width:"+percentage+"%;'></div>");
            $("#project-storage-quota-text").append("<p>" + mount_used.toFixed(2) + " GB / " + mount_available.toFixed(2) + " GB</p>");
    } else {
        $("#project-storage-info").text("No information at this time");
    }


/*
    if (project.hasOwnProperty("store")){
        if (project.store.hasOwnProperty("mount")){
            if (project.store.mount.hasOwnProperty("size")){
                var mount_used = project.store.mount.size["#text"]/1000000000.0;
                $("#project-storage-info").addClass("project-mounted");

                if (project.store.hasOwnProperty("arg")) {
                    var mount_available = project.store.arg["#text"]/1000000000.0;
                    var percentage = mount_used / mount_available * 100.0;
                    $("#project-storage-quota-bar").append("<div class='progress'><div class='progress-bar' role='progressbar' aria-valuenow='"+percentage+"' aria-valuemin='0' aria-valuemax='100' style='width:"+percentage+"%;'></div>");
                    $("#project-storage-quota-text").append("<p>" + mount_used.toFixed(2) + " GB / " + mount_available.toFixed(2) + " GB</p>");
                } else {
                    $("#project-storage-quota-bar").text("This project has no available quota");
                }
            } else {
                $("#project-storage-info").text("Store is currently unmounted");
                $("#project-storage-info").addClass("project-unmounted");
            }
        } else {
            $("#project-storage-info").text("Store is currently unmounted");
            $("#project-storage-info").addClass("project-unmounted");
        }
    } else {
        $("#project-storage-info").text("Store is currently unmounted");
        $("#project-storage-info").addClass("project-unmounted");
    }
    */

}


function update_actvity_callback(response) {
    var audit_log = xmlToJson(response._xml);

    $.each(audit_log.event, function(index, item){
        $("#project-activity-list").append("<tr><td><span id='role-badge' class='label label-primary'>"+item["@attributes"].type+"</span></td><td>"+item.actor["#text"]+"</td><td>"+item.time["#text"]+"</td><td>"+item.payload.name["#text"]+"</td</tr>");
    });
}

function update_actvity_callback_error(response){
    console.log('update_actvity_callback_error', response);
}


function show_project_update_overlay() {

    $("#project-update-view").height($("#project-storage-info-view").height());
    $("#project-update-view").removeClass("hidden");
}

function dismiss_project_update_overlay() {
    $("#project-update-view").addClass("hidden");
}


function show_project_detail_for_event(event){
    var project_name = $(event.currentTarget).text();
    $(".project-list-entry").removeClass("active");
    $(event.currentTarget).addClass("active");

    show_project_detail(project_name);
}

function show_project_detail(project_name) {
    // var project_name = $(event.currentTarget).text();
    // $(event.currentTarget).addClass("active");

    show_project_update_overlay();

    // get and display detail data for a project
    // note: the use of a project name as a authorative identifier for a project is a Pawsey SC convention/

    // $("#focus-activity-button").click(function(){focus_view("activity");});
    // $("#focus-info-button").click(function(){focus_view("info");});
    // $("#focus-user-list-button").click(function(){focus_view("user");});
    // $("#focus-settings-button").click(function(){focus_view("settings");});

    // $(".project-view-container").removeClass("hidden");

    $("#new-user-search-button").click(function(){search_for_possible_user();});
    $("#public-activation-button").click(function(event) {toggle_public_activation(event);});

    $(".project-error-container").addClass("hidden")
    $("#project-default-view").addClass("hidden")

    $("#project-user-content").removeClass("hidden")

    $("#profile-tab-control>a").click(function(e){profile_tab_selected(e);});

    $("#project-title").text(project_name);
    $("#project-title").removeClass("hidden");

    $('[data-toggle="tooltip"]').tooltip()

    $("#audit-update-view-text").text("Update to show logs");
    $("#audit-update-view").removeClass("hidden");

    // new user modal setup
    $('#add-user-modal').on('shown.bs.modal', function () {
        $('#input_username').focus();
        $("#add-user-default-view").removeClass("hidden");
        $("#add-user-notfound-view").addClass("hidden");
        $("#add-user-found-view").addClass("hidden");
    })

    $("#confirm-add-user").click(function(e){add_new_user(e);});
    $("#confirm-add-user-rw").click(function(e){add_new_user(e);});

    check_for_principal_user(project_name);

    update_project_settings(project_name);

    show_tab("info");

    function show_project_detail_callback(response){
        var project = xmlToJson(response._xml);

        // get store type
        // localisation issue here
        var store_type = "Generic File System"

        // console.log('path',project.store.path["#text"]);


        // if (project.store.path["#text"].search('RDSI_GPFS01') > 0){
        //     store_type = 'RDSI';
        // } else if (project.store.path["#text"].search('livearc01fs')){
        //     store_type = 'General Science';
        // }

        $("#project-description").text("No description");

        if (project["role-namespace"]){
            if (project["role-namespace"].description){
                $("#project-description").text(project["role-namespace"].description["#text"]);
            }
        }

        // get storage info
        update_storage_info(project);

        // get user list
        update_user_list(project);

        $("#userfilter-dropdown").on('click', 'li a', function(){
            $("#user-select-button-group").find('.btn').html($(this).text() + " <span class=\"caret\"></span>");
            $("#user-select-button-group").find('.btn').val($(this).text());
            $("#audit-update-view-text").text("Update to show logs");
            $("#audit-update-view").removeClass("hidden");
        });

        $("#user-select-button-group").find('.btn').val("All Users");

        dismiss_project_update_overlay();
    }

    function project_detail_error_callback(response) {
        // project detail cannot be retreived.
        // hide everthing, show an error
        $(".project-view-container").addClass("hidden");
        $(".project-error-container").removeClass("hidden")
        $("#project-user-list").empty();
        $("#project-storage-quota-bar").empty();
        $("#project-storage-quota-text").empty();

        dismiss_project_update_overlay();

    }

    var args = new arc.xml.XmlStringWriter();
    project_name  = project_name.replace(/'/g, "\'");
    args.add("name",project_name);
    livearc_async_generic_handler("project.describe", args, show_project_detail_callback, project_detail_error_callback);

    // populate audit view
    // audit.query :filter "contains(audit.field.value('namespace'),'/projects/Data Team')"
    $("#project-activity-list").empty();
    $('#audit-list-table tbody').empty();

    setup_audit_interface();

    profile_check_layout();
    // var args1 = new arc.xml.XmlStringWriter();
    // livearc_async_generic_handler("audit.query", args1, update_actvity_callback, update_actvity_callback_error);
}

function profile_tab_selected(e) {
    e.preventDefault();
    show_tab($(e.currentTarget).attr('data-tabname'));
}


// settings controls

function update_project_settings(project_name){
    check_public_activation(project_name);
}


function update_public_status(value){
    // change the public sharing status showin the UI based on BOOL 'value'
    $("#public-access-status").text("OFF");
    $("#public-activation-button").text("Activate");

    if(value){
        // set to true
        $("#public-access-status").text("ON");
        $("#public-activation-button").text("Deactivate");
    }
}

function check_public_activation(project_name) {

    //example query
    //actor.have :role -type role "Data Team:readonly" :name "public" :type domain

    function check_public_activation_callback(response){
        // The response.role should be true/false.  Sometimes this argument is missing.  If the role
        // argument is missing we'll treat that as a false response.

        var public_response  = xmlToJson(response._xml);
        if (public_response.actor.hasOwnProperty("role")){
            if (public_response.actor.role["#text"] === "true"){
                // public access is ON
                update_public_status(true);
            } else {
                update_public_status(false);
            }
        } else {
            update_public_status(false);
        }
    }

    function check_public_activation_error_callback(response){
        update_public_status(false);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("role",["type","role"],project_name+":readonly");
    args.add("name","public");
    args.add("type","domain");


    livearc_async_generic_handler("actor.have", args, check_public_activation_callback, check_public_activation_error_callback);
}


function toggle_public_activation(event) {

    var project_name = $("#project-title").text();

    $("#public-activation-button").button('loading');

    var args = new arc.xml.XmlStringWriter();
    args.add("role",["type","role"],project_name+":readonly");
    args.add("name","public");
    args.add("type","domain");

    if($(event.currentTarget).text() === "Deactivate"){
        livearc_execute_handler("actor.revoke", args, false);
    } else {
        livearc_execute_handler("actor.grant", args, false);
    }

    $("#public-activation-button").button('reset');
    check_public_activation(project_name);
}

// Audit controls
function setup_audit_interface() {

    //current_audit_list();

    // init the button based on current global_audit_filters
    $(".audit-group-item").click(function(e){filter_type_selected(e);});

    $("#audit-update-button").click(function(){get_selected_audit_list();});
}

// audit type filter controls
function clear_all_filters() {
    $(".audit-group-item").removeClass("active");
}

function filter_type_selected(e){
    var selected_type = $(e.currentTarget).text().trim();

    // mutually exclusive filters
    if (selected_type === "everything" || selected_type === "deletion" || selected_type === "uploads"){
        clear_all_filters();
        if($(e.currentTarget).hasClass('active')){
            $(e.currentTarget).removeClass('active');
        } else {
            $(e.currentTarget).addClass('active');
        }
    }

    // combinable filters
    if (selected_type === "files" || selected_type === "users" || selected_type === "folders"){
        $(".audit-exclusive").removeClass('active');
        if($(e.currentTarget).hasClass('active')){
            $(e.currentTarget).removeClass('active');
        } else {
            $(e.currentTarget).addClass('active');
        }
    }

    $("#audit-update-view-text").text("Update to show logs");
    $("#audit-update-view").removeClass("hidden");

    // update ui as needed

}


function get_selected_audit_list(){

    $("#audit-update-view-text").text("Getting your log request...");

    function current_audit_list_callback(request) {
        var audit_list  = xmlToJson(request._xml);
        $('#audit-list-table tbody').empty();

        if (audit_list.event){
            $.each(audit_list.event, function(index,item){
                var payload = '';
                for (prop in item.payload){
                    if (!item.payload.hasOwnProperty(prop)){continue;}
                    payload = payload + item.payload[prop]["#text"] + ' ';
                }

                var type_text  = item["@attributes"].type;
                type_text = type_text.replace(/\./," ");
                type_text = type_text.replace("asset", "file");
                type_text = type_text.replace("namespace", "folder");
                type_text = type_text.replace("role", "user");

                var user_text = item.actor["#text"].split(":")[1];
                if (!user_text){
                    user_text = "delegate";
                }

                var object_id = "-1";
                if (item.hasOwnProperty("object")){
                    object_id = item.object["#text"];
                }

                $('#audit-list-table tbody').append("<tr data-id='"+object_id+"'><td>"+type_text+"</td><td>"+user_text+"</td><td>"+item.time["#text"]+"</td><td>"+payload+"</td></tr>");
            });
        } else {
            $('#audit-list-table tbody').append("<tr data-id='-1'><td>0 entries found</td><td></td><td></td><td></td></tr>");
        }

        $("#audit-update-view").addClass("hidden");
    }

    function current_audit_list_error(request) {
        console.log('audit_list error',request);
    }

    var audit_list = [];
    var project_name = $("#project-title").text();

    // find filter types
    $.each($(".audit-group-item"),function(index, item){
        if ($(item).hasClass('active')){
            audit_list.push($(item).text());
        }
    });

    var args = new arc.xml.XmlStringWriter();
    var username =  $("#user-select-button-group").find('.btn').val();

    args.add("size","10000");

    if (username != "All Users"){
        args.add("actor","ivec:"+username);
    }

    $.each(audit_list,function(index,item){
        if (item === "files"){
            args.add("prefix","asset");
        }

        if (item === "folders"){
            args.add("prefix","namespace");
        }

        if (item === "users"){
            args.add("prefix","role");
        }

        if (item === "deletion"){
            args.add("type","asset.content.deleted");
            args.add("type","asset.deleted");
            args.add("type","namespace.deleted");
        }
        if (item === "everything"){
        }
    });

    args.add("filter","starts-with(audit.field.value('namespace'),'/projects/"+project_name+"') or contains(audit.field.value('name'),'"+project_name+"')");

    livearc_async_generic_handler("audit.query", args, current_audit_list_callback, current_audit_list_error);

}

// reporting
function update_project_report(){

    $("#project_list_table tbody").empty();

    $("#projectreport-default-view").addClass("hidden");
    $("#projectreport-activity-view").removeClass("hidden").addClass("fadeIn");
    $("#projectreport-table-view").addClass("hidden");
    $("#projectreport-error-view").addClass("hidden");

    function project_list_callback(request){
        var report_info = xmlToJson(request._xml);

        if (report_info.hasOwnProperty("store")){
            if (report_info.store.hasOwnProperty("length")){
                $.each(report_info.store,function(index,item){
                    if(item.type["#text"] != "database"){
                        if (item.hasOwnProperty("mount")) {
                            var store_type = "General Science";
                            if (item.mount.hasOwnProperty("path")){
                                var store_type_data = item.mount.path["#text"];

                                if (store_type_data.search('RDSI_GPFS01') > 0){
                                    store_type = 'RDSI';
                                }

                                if (store_type_data.search('/mnt/livearc_db') > 0){
                                    store_type = 'internal';
                                }
                            }

                            var quota_used_bytes = 0;
                            if (item.mount.hasOwnProperty("size")){
                                quota_used_bytes = item.mount.size["#text"];
                            }
                            var mount_used = quota_used_bytes/1000000000.0;

                            var quota_max_bytes = 0;
                            var mount_available = 0;

                            if (item.hasOwnProperty("arg")){
                                quota_max_bytes = item.arg["#text"];
                                mount_available = quota_max_bytes/1000000000.0;
                            }
                            var percentage = mount_used / mount_available * 100.0;

                            html_list_element = "<tr class='online-project'><td class='store-title'>"+item["@attributes"].name+"</td><td><span class='label label-success'>Online</span></td><td>"+store_type+"</td><td class='quota-used' data-quota-used='"+quota_used_bytes+"'>"+mount_used.toFixed(2) +"</td><td class='quota-max' data-quota-max='"+quota_max_bytes+"'> "+mount_available.toFixed(2)+" <button class='btn btn-xs btn-primary quota-change-button'><i class='fa fa-cog'></i></button></td><td class='progress-bar-col'><div class='progress'><div class='progress-bar' role='progressbar' aria-valuenow='"+percentage+"' aria-valuemin='0' aria-valuemax='100' style='width:"+percentage+"%;'></div></div></td><td>"+percentage.toFixed(2)+"% used</td></tr>";
                        } else {
                            html_list_element = "<tr class='offline-project'><td class='store-title'>"+item["@attributes"].name+"</td><td><span class='label label-warning'>Offline</span></td><td>--</td><td> -- </td><td> -- </td><td>--</td></tr>";
                        }
                        $("#project_list_table tbody").append(html_list_element);

                    }
                });
            } else {
                var item = report_info.store;

                if(item.type["#text"] != "database"){
                    if (item.hasOwnProperty("mount")) {
                        var store_type = "General Science";

                        if (item.hasOwnProperty("path")) {
                            if (item.path["#text"].search('RDSI_GPFS01') > 0){
                                store_type = 'RDSI';
                            }
                            if (item.path["#text"].search('/mnt/livearc_db') > 0){
                                store_type = 'internal';
                            }
                        }

                        var quota_used_bytes = 0;
                        if (item.mount.hasOwnProperty("size")){
                            quota_used_bytes = item.mount.size["#text"];
                        }
                        var mount_used = quota_used_bytes/1000000000.0;

                        var quota_max_bytes = 0;
                        var mount_available = 0;

                        if (item.hasOwnProperty("arg")){
                                quota_max_bytes = item.arg["#text"];
                                mount_available = quota_max_bytes/1000000000.0;
                        }

                        var percentage = mount_used / mount_available * 100.0;

                        html_list_element = "<tr class='online-project'><td class='store-title'>"+item["@attributes"].name+"</td><td><span class='label label-success'>Online</span></td><td>"+store_type+"</td><td class='quota-used' data-quota-used='"+quota_used_bytes+"'>"+mount_used.toFixed(2) +"GB </td><td class='quota-max' data-quota-max='"+quota_max_bytes+"'> "+mount_available.toFixed(2)+"GB <button class='btn btn-xs btn-primary quota-change-button'><i class='fa fa-cog'></i></button></td><td class='progress-bar-col'><div class='progress'><div class='progress-bar' role='progressbar' aria-valuenow='"+percentage+"' aria-valuemin='0' aria-valuemax='100' style='width:"+percentage+"%;'></div></div></td><td>"+percentage.toFixed(2)+"% used</td></tr>";
                    } else {
                        html_list_element = "<tr class='offline-project'><td class='store-title'>"+item["@attributes"].name+"</td><td><span class='label label-warning'>Offline</span></td><td>--</td><td> --GB </td><td> -- GB</td><td>--</td></tr>";
                    }
                    $("#project_list_table tbody").append(html_list_element);
                }
            }
        } else {

        }

        $("#projectreport-default-view").addClass("hidden");
        $("#projectreport-activity-view").addClass("hidden");
        $("#projectreport-table-view").removeClass("hidden").addClass("fadeIn");
        $("#projectreport-error-view").addClass("hidden");

        $(".quota-change-button").click(function(e){show_quota_management(e);});

    }

    function project_list_callback_error(request){
        console.log('project_list_callback_error',request);

        $("#projectreport-default-view").addClass("hidden");
        $("#projectreport-activity-view").addClass("hidden");
        $("#projectreport-table-view").addClass("hidden");
        $("#projectreport-error-view").removeClass("hidden");

    }

    var username = $("#profile-user-name").attr("data-name");

    var args = new arc.xml.XmlStringWriter();

    livearc_async_generic_handler("asset.store.describe", args, project_list_callback, project_list_callback_error);

}


function show_quota_management(e){
    var target_row = $(e.currentTarget).closest('.online-project');

    var project_id = $(target_row).find(".store-title").text();
    var project_quota = $(target_row).find(".quota-max").attr('data-quota-max');
    $("#quota-change-confirm-interface").addClass("hidden");
    $("#quota-manage-current").text((project_quota/1000000000000).toFixed(2));
    $("#quota-manage-title").text(project_id);
    $("#quota-change-error").addClass("hidden");

    $("#quota-management-modal").modal('show');

    $("#update-new-quota-button").unbind('click').click(function(){change_project_quota();});
    $("#reset-quota-change-button").unbind('click').click(function(){reset_change_project_quota();})
    $("#confirm-quota-change-button").unbind('click').click(function(){confirm_change_project_quota();})

}


function change_project_quota(){
    var new_quota_tb =  $("#quota-manage-new").val();
    var new_quota_bytes = (new_quota_tb * 1000000000000.).toFixed(0);
    $("#new-quota-terabytes-view").text(new_quota_tb);

    $("#new-quota-bytes-view").text(new_quota_bytes);

    $("#quota-change-confirm-interface").removeClass("hidden");

}


function reset_change_project_quota(){
    $("#quota-change-error").addClass("hidden");
    $("#quota-change-confirm-interface").addClass("hidden");
}


function confirm_change_project_quota() {
    function change_quota_callback_success(request){
        $("#quota-management-modal").modal('hide');
        update_project_report();
    }

    function change_quota_callback_error(request) {
        $("#quota-change-error").removeClass("hidden");
    }

    var new_quota_tb =  $("#quota-manage-new").val();
    var new_quota_bytes = (new_quota_tb * 1000000000000.).toFixed(0);
    var project_id = $("#quota-manage-title").text();

    var args = new arc.xml.XmlStringWriter();
    args.add("store", project_id);
    args.add("arg",["name","file.max.size"], new_quota_bytes);

    livearc_async_generic_handler("asset.store.arg.set", args, change_quota_callback_success, change_quota_callback_error);
}


function export_project_list_as_csv() {
    // function to generate a csv file of projects (asset stores) as a csv file
    // will require HTML5 Download.
    // code: http://stackoverflow.com/questions/16078544/export-to-csv-using-jquery-and-html

    var $rows = $("#project_list_table").find('tr:has(td)'),

        // Temporary delimiter characters unlikely to be typed by keyboard
        // This is to avoid accidentally splitting the actual contents
        tmpColDelim = String.fromCharCode(11), // vertical tab character
        tmpRowDelim = String.fromCharCode(0), // null character

        // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"',

        // Grab text from table into CSV formatted string
        csv = '"' + $rows.map(function (i, row) {
            var $row = $(row),
                $cols = $row.find('td');

            return $cols.map(function (j, col) {
                var $col = $(col),
                    text = $col.text();

                return text.replace('"', '""'); // escape double quotes

            }).get().join(tmpColDelim);

        }).get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim) + '"',

        // Data URI
        csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

    // get date
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd;
    }

    if(mm<10) {
        mm='0'+mm;
    }

    today = dd + '-' + mm + '-' + yyyy;
    blob = new Blob([csv], { type: 'text/csv' });
    var csvUrl = URL.createObjectURL(blob);
    var link = document.createElement('a');
    link.href=csvUrl;

    $(link).attr({
        'download': 'current-storage-report'+today+'.csv',
        'href': csvUrl
    });
    link.click();
}


// applications
function update_new_application_count(){
    //$("#new-applications-badge")
    function count_new_applications_success(response){
        var result = xmlToJson(response._xml);
        $("#new-applications-badge").html(result.total["#text"]);
    }

    function count_new_applications_failure(response){
        console.log('count_new_applications_failure',response);
        $("#new-applications-badge").html("?");
    }
    var args = new arc.xml.XmlStringWriter();
    args.add('where','namespace=/requests/new');
    livearc_async_generic_handler("asset.count", args, count_new_applications_success, count_new_applications_failure);
}

function display_application_data(application_id, application_type){

    function get_content_success(response) {
        var asset_data = xmlToJson(response._xml);
        var application = xmlToJson(response.element("asset/content/xml").xml());

        var collection_name = application.application.collection_name["#text"];
        var firstname = application.application.principal_firstname["#text"];
        var lastname = application.application.principal_lastname["#text"];
        var submission_date = asset_data.asset.ctime["#text"];
        var request_size = application.application.project_request_total["#text"];
        if(application_type==="new"){
            $("#application-list-new tbody").append('<tr data-assetvalue='+asset_data.asset["@attributes"].id +'><td>' + submission_date + '</td><td>' + collection_name + '</td><td>' + firstname +' ' + lastname +'</td><td>'+request_size+'</td></tr>');
            $("#app-detail-backbutton").unbind('click').click(function(e){
                show_application_list_view();
            });
            $("#application-list-new tbody tr").unbind('click').click(function(e){
                show_application_detail_view($(e.currentTarget).attr('data-assetvalue'));
            });
        } else if (application_type==="approved"){
            $("#application-list-approved tbody").append('<tr data-assetvalue='+asset_data.asset["@attributes"].id +'><td>' + submission_date + '</td><td>' + collection_name + '</td><td>' + firstname +' ' + lastname +'</td><td>'+request_size+'</td></tr>');
            $("#app-detail-backbutton").unbind('click').click(function(e){
                show_application_list_view();
            });
            $("#application-list-approved tbody tr").unbind('click').click(function(e){
                show_application_detail_view($(e.currentTarget).attr('data-assetvalue'));
            });
        } else if (application_type==="denied"){
            $("#application-list-denied tbody").append('<tr data-assetvalue='+asset_data.asset["@attributes"].id +'><td>' + submission_date + '</td><td>' + collection_name + '</td><td>' + firstname +' ' + lastname +'</td><td>'+request_size+'</td></tr>');
            $("#app-detail-backbutton").unbind('click').click(function(e){
                show_application_list_view();
            });
            $("#application-list-denied tbody tr").unbind('click').click(function(e){
                show_application_detail_view($(e.currentTarget).attr('data-assetvalue'));
            });
        }
    }

    function  get_content_failure(response){
        console.log('get_content_failure',response);
        if(application_type==="new"){
            $("#application-list-new").append('<tr><td> Could not read application</td></tr>');
        } else if (application_type === "approved"){
            $("#application-list-approved").append('<tr><td> Could not read application</td></tr>');
        } else if (application_type=== "denied"){
            $("#application-list-denied").append('<tr><td> Could not read application</td></tr>');
        }
    }

    var args = new arc.xml.XmlStringWriter();

    args.add("id", application_id);
    args.add("content-as-xml", true);

    livearc_async_generic_handler("asset.get", args, get_content_success,  get_content_failure);
}


function update_new_application_list(){
    //$("#new-applications-badge")
    function list_new_applications_success(response){
        var result = xmlToJson(response._xml);
        $("#application-list-new tbody").empty();
        if (result.namespace.asset != undefined){
            if (result.namespace.asset.hasOwnProperty("length")){
                $.each(result.namespace.asset, function(index, item){
                    display_application_data(item["@attributes"].id,"new");
                });
            } else {
                display_application_data(result.namespace.asset["@attributes"].id,"new");
            }
        }
    }

    function list_new_applications_failure(response){
        $("#new-applications-badge").html("?");
    }

    function list_approved_applications_success(response){
        var result = xmlToJson(response._xml);
        $("#application-list-approved tbody").empty();
        if (result.namespace.asset != undefined){
            if (result.namespace.asset.hasOwnProperty("length")){
                $.each(result.namespace.asset, function(index, item){
                    display_application_data(item["@attributes"].id,"approved");
                });
            } else {
                display_application_data(result.namespace.asset["@attributes"].id,"approved");
            }
        }
    }

    function list_approved_applications_failure(response){
        $("#new-applications-badge").html("?");
    }

    function list_denied_applications_success(response){
        var result = xmlToJson(response._xml);
        $("#application-list-denied tbody").empty();
        if (result.namespace.asset != undefined){
            if (result.namespace.asset.hasOwnProperty("length")){
                $.each(result.namespace.asset, function(index, item){
                    display_application_data(item["@attributes"].id,"denied");
                });
            } else {
                display_application_data(result.namespace.asset["@attributes"].id,"denied");
            }
        }
    }

    function list_denied_applications_failure(response){
        $("#new-applications-badge").html("?");
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("namespace","/requests/new");
    args.add("assets","true");

    livearc_async_generic_handler("asset.namespace.list", args, list_new_applications_success, list_new_applications_failure);

    var args1 = new arc.xml.XmlStringWriter();
    args1.add("namespace","/requests/approved");
    args1.add("assets","true");

    livearc_async_generic_handler("asset.namespace.list", args1, list_approved_applications_success, list_approved_applications_failure);

    var args2 = new arc.xml.XmlStringWriter();
    args2.add("namespace","/requests/denied");
    args2.add("assets","true");

    livearc_async_generic_handler("asset.namespace.list", args2, list_denied_applications_success, list_denied_applications_failure);



}

function show_application_detail_view(asset_id){

    console.log('show_application_detail_view',asset_id);

    function populate_for_code_list(packed_list){
        // takes the picked string from the data model in the form code:percent,code:percent
        // and populates the UI
        $("#project-for-code-display").empty();
        if(packed_list != "" && packed_list != undefined){

            var code_list = packed_list.split(",");

            $.each(code_list, function(index,item){
                values = item.split(":");
                $("#project-for-code-display").append('<li><span>Code: '+values[0]+'</span><span> Percentage: '+values[1]+'<span></li>');
            });
        }
    }
    function get_content_success(response) {
        var asset_data = xmlToJson(response._xml);
        var application = xmlToJson(response.element("asset/content/xml").xml());

        var application_id = asset_data.asset["@attributes"].id;

        var collection_name = application.application.collection_name["#text"];

        var collection_shortname = "not set";
        if (application.application.hasOwnProperty("collection_shortname")){
            collection_shortname = application.application.collection_shortname["#text"];
        }

        var pi_firstname = "not set";
        if (application.application.hasOwnProperty("principal_firstname")){
            pi_firstname = application.application.principal_firstname["#text"];
        }

        var pi_lastname = "not set";
        if (application.application.hasOwnProperty("principal_lastname")){
            pi_lastname = application.application.principal_lastname["#text"];
        }

        var pi_position = "not set";
        if (application.application.hasOwnProperty("principal_position")){
            pi_position = application.application.principal_position["#text"];
        }

        var pi_ident = "not set";
        if (application.application.hasOwnProperty("principal_ident")){
          pi_ident = application.application.principal_ident["#text"];
        }

        var pi_institution = "not set";
        if (application.application.hasOwnProperty("principal_institution")){
            pi_institution = application.application.principal_institution["#text"];
        }

        var pi_phone = "not set";
        if (application.application.hasOwnProperty("principal_phone")){
            pi_phone = application.application.principal_phone["#text"];
        }

        var pi_email = "not set";
        if (application.application.hasOwnProperty("principal_email")){
            pi_email = application.application.principal_email["#text"];
        }

        var dep_firstname = "not set";
        if (application.application.hasOwnProperty("deputy_firstname")){
            dep_firstname = application.application.deputy_firstname["#text"];
        }

        var dep_lastname = "not set";
        if (application.application.hasOwnProperty("deputy_lastname")){
            dep_lastname = application.application.deputy_lastname["#text"];
        }

        var dep_position = "not set";
        if (application.application.hasOwnProperty("deputy_position")){
            dep_position = application.application.deputy_position["#text"];
        }

        var dep_ident = "not set";
        if (application.application.hasOwnProperty("deputy_ident")){
            dep_ident = application.application.deputy_ident["#text"];
        }

        var dep_institution = "not set";
        if (application.application.hasOwnProperty("deputy_institution")){
            dep_institution = application.application.deputy_institution["#text"];
        }

        var dep_phone = "not set";
        if (application.application.hasOwnProperty("deputy_phone")){
            dep_phone = application.application.deputy_phone["#text"];
        }

        var dep_email = "not set";
        if (application.application.hasOwnProperty("deputy_email")){
            dep_email = application.application.deputy_email["#text"];
        }

        var submission_date = asset_data.asset.ctime["#text"];
        var request_size = application.application.project_request_total["#text"];

        var milestone_1 = application.application.project_request_milestone1["#text"];
        var milestone_2 = application.application.project_request_milestone2["#text"];
        var milestone_3 = application.application.project_request_milestone3["#text"];
        var milestone_4 = application.application.project_request_milestone4["#text"];

        var for_list = application.application.project_for_code_list["#text"];
        var latency_flag =  application.application.project_storage_latency["#text"];
        var latency_comment = application.application.project_storage_latency_comment["#text"];

        var description = "not entered";
        if (application.application.hasOwnProperty("project_description")){
            description = application.application.project_description["#text"];
        }

        var community = "not entered";
        if (application.application.hasOwnProperty("project_community")){
            community = application.application.project_community["#text"];
        }

        var funding = "not entered";
        if (application.application.hasOwnProperty("project_funding")){
            funding = application.application.project_funding["#text"];
        }

        var future = "not entered";
        if (application.application.hasOwnProperty("project_future")){
            future = application.application.project_future["#text"];
        }

        var management = "not entered";
        if (application.application.hasOwnProperty("project_management")){
            management = application.application.project_management["#text"];
        }

        var encumberance = "not entered";
        if (application.application.hasOwnProperty("project_encumberance")){
            encumberance = application.application.project_encumberance["#text"];
        }

        var links = "not entered";
        if (application.application.hasOwnProperty("project_links")){
            links = application.application.project_links["#text"];
        }

        $("#application-detail-view").attr("data-application-detail-id",application_id);

        $("#application-detail-title").val(collection_name);
        $("#application-detail-shorttitle").val(collection_shortname);
        $("#application-title-id").text(application_id);

        $("#application-detail-size").text(request_size);
        $("#application-detail-date-submitted").text(submission_date);

        $("#principal-ident .ident-username").val(pi_firstname+" "+pi_lastname);
        $("#principal-ident .ident-position").val(pi_position);
        $("#principal-ident .ident-institution").val(pi_institution);
        $("#principal-ident .ident-ident").val(pi_ident);
        $("#principal-ident .ident-phonenumber").val(pi_phone);
        $("#principal-ident .ident-emailaddress").val(pi_email);


        $("#secondary-ident .ident-username").val(dep_firstname+" "+dep_lastname);
        $("#secondary-ident .ident-position").val(dep_position);
        $("#secondary-ident .ident-institution").val(dep_institution);
        $("#secondary-ident .ident-ident").val(dep_ident);
        $("#secondary-ident .ident-phonenumber").val(dep_phone);
        $("#secondary-ident .ident-emailaddress").val(dep_email);

        $("#application-detail-mile-total").val(request_size);
        $("#application-detail-mile-1").val(milestone_1);
        $("#application-detail-mile-2").val(milestone_2);
        $("#application-detail-mile-3").val(milestone_3);
        $("#application-detail-mile-4").val(milestone_4);
        $("#application-detail-latency-flag").val(latency_flag);
        $("#application-detail-latency-comment").val(latency_comment);

        $("#application-detail-description").val(description);
        $("#application-detail-community").val(community);
        $("#application-detail-funding").val(funding);
        $("#application-detail-future").val(future);
        $("#application-detail-management").val(management);
        $("#application-detail-encumberance").val(encumberance);
        $("#application-detail-links").val(links);

        populate_for_code_list(for_list);

        $("#approve-application-button").unbind("click").click(function(e){
            approve_application($("#application-detail-view").attr("data-application-detail-id"));
        });
        $("#decline-application-button").unbind("click").click(function(e){
            decline_application($("#application-detail-view").attr("data-application-detail-id"));
        });

        $("#app-detail-savebutton").unbind("click").click(function(){
            save_application_changes();
        });
        $("#app-detail-editbutton").unbind("click").click(function(){
            edit_application();
        });
    }

    function  get_content_failure(response){
        console.log('get_content_failure',response);
    }

    $("#application-list-view").addClass("hidden");
    $("#application-detail-view").removeClass("hidden");
    profile_check_layout();

    var args = new arc.xml.XmlStringWriter();

    args.add("id", asset_id);
    args.add("content-as-xml", true);

    livearc_async_generic_handler("asset.get", args, get_content_success,  get_content_failure);
}

function show_application_list_view(asset_id){
    $("#application-list-view").removeClass("hidden");
    $("#application-detail-view").addClass("hidden");
}

function approve_application(application_id){
    function application_approve_success(response) {
        var response_data = xmlToJson(response._xml);
        update_new_application_list();
    }

    function  application_approve_failure(response){
        console.log('application_approve_failure',response);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("id", application_id);
    livearc_async_generic_handler("request.approve", args, application_approve_success,  application_approve_failure);
}

function decline_application(application_id){
    function application_deny_success(response) {
        var response_data = xmlToJson(response._xml);
        update_new_application_list();
    }

    function  application_deny_failure(response){
        console.log('application_deny_failure',response);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("id", application_id);
    livearc_async_generic_handler("request.deny", args, application_deny_success,  application_deny_failure);

}

function edit_application(){
    $("#app-detail-editbutton").addClass("application-editing-active");
    $("#app-detail-savebutton").removeClass("hidden");
    var all_inputs = $( ":input" );
    all_inputs.removeAttr("readonly");
}

function save_application_changes(){
    $("#app-detail-editbutton").removeClass("application-editing-active");
    $("#app-detail-savebutton").addClass("hidden");
    var all_inputs = $( ":input" );
    all_inputs.attr("readonly",true);
}

function save_current_document() {
    var new_xml_document = new arc.xml.XmlStringWriter();

    new_xml_document.add("collection_name",$("application-detail-title").val());
    new_xml_document.add("collection_shortname",$("#application-detail-shorttitle").val());

    new_xml_document.add("backuptype_other_text","");

    new_xml_document.add("principal_firstname",$("#principal-ident .ident-username").val());
    new_xml_document.add("principal_lastname",$("#data-pi-lastname-input").val());
    new_xml_document.add("principal_phone",$("#dprincipal-ident .ident-phonenumber").val());
    new_xml_document.add("principal_email",$("#principal-ident .ident-emailaddress").val());
    new_xml_document.add("principal_ident",$("#principal-ident .ident-ident").val());
    new_xml_document.add("principal_institution",$("#principal-ident .ident-institution").val());
    new_xml_document.add("principal_position", $("#principal-ident .ident-position").val());

    new_xml_document.add("deputy_firstname",$("#data-deputy-firstname-input").val());
    new_xml_document.add("deputy_lastname",$("#data-deputy-lastname-input").val());
    new_xml_document.add("deputy_phone",$("#data-deputy-phone-input").val());
    new_xml_document.add("deputy_email",$("#data-deputy-email-input").val());
    new_xml_document.add("deputy_ident",$("#data-deputy-ident-input").val());
    new_xml_document.add("deputy_institution",$("#data-deputy-institution-input").val());
    new_xml_document.add("deputy_position",  $("#data-deputy-role-selector .btn:first-child").val());


    new_xml_document.add("project_for_code_list",packed_forcode_list());
    new_xml_document.add("project_description",$("#application-detail-description").val());

    new_xml_document.add("project_community",$("#application-detail-community").val());
    new_xml_document.add("project_funding",$("#application-detail-funding").val());
    new_xml_document.add("project_future",$("#application-detail-future").val());
    new_xml_document.add("project_encumberance",$("#application-detail-encumberance").val());
    new_xml_document.add("project_links",$("#application-detail-links").val());
    new_xml_document.add("project_management",$("#application-detail-management").val());

    new_xml_document.add("project_request_total",$("#project-request-total-input").val());
    new_xml_document.add("project_request_milestone1",$("#project-request-milestone1-input").val());
    new_xml_document.add("project_request_milestone2",$("#project-request-milestone2-input").val());
    new_xml_document.add("project_request_milestone3",$("#project-request-milestone3-input").val());
    new_xml_document.add("project_request_milestone4",$("#project-request-milestone4-input").val());

    new_xml_document.add("project_storage_latency",$("#project-access-selector").attr("data-latency"));
    new_xml_document.add("project_storage_latency_comment",$("#project-low-latency-comments").val());

    new_xml_document.add("project_additional_comments",$("#project-add-comments-input").val());

    set_application_xml(_current_document_id,new_xml_document);
}
