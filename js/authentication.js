function initialise_front_page(){
   // login start
    $("#nav-login-button").addClass("disabled");
    $("#nav-login-button").click(function() {frontpage_check_login();});

    $("#dashboard-navbar-username").change(function() {frontpage_validate_inputs();});
    $("#dashboard-navbar-password").change(function() {frontpage_validate_inputs();});
    $("#dashboard-navbar-username").keydown(function(event) {frontpage_check_login_return_hit(event);});
    $("#dashboard-navbar-password").keydown(function(event) {frontpage_check_login_return_hit(event);});

    frontpage_check_for_valid_session();
}

function frontpage_check_login_return_hit(e){
    //rehide error if the user stasrts typing again
//    $("#frontpage-error").addClass("hidden");

    if(e.keyCode == 13){
        frontpage_check_login();
    } else {
        frontpage_validate_inputs();
    }
}

function frontpage_check_login() {
//    var path_to_open = $("#hidden_path_to_open").text();
    var loginIsSuccessful = frontpage_livearc_logon($("#dashboard-navbar-username").val(), $("#dashboard-navbar-password").val());

//    if(!loginIsSuccessful) {
//        $("#frontpage-error").removeClass("hidden");
//        $("#frontpage-error").addClass("alert alert-danger animated fadeInUp").html("Login failed, please check your username and password");
//
//    }
}

function frontpage_validate_inputs() {

    if ($("#dashboard-navbar-username").val().length > 0 && $("#dashboard-navbar-password").val().length > 0){
        $("#nav-login-button").removeClass("disabled");
    } else {
        $("#nav-login-button").addClass("disabled");
    }
}

function frontpage_livearc_logon(username, password) {
    // livearc logon routine.  Does an initial login and load the UI with the
    // root directory. hardwired to iVEC domain users.

    var domain = "ivec";

    global_domain = domain;

    // need to add 1. warn if all caps

    var login_status = true;

    try {
        arc.mf.client.RemoteServer.logon(domain,username,password);
    } catch (e) {
        login_status = false;
    }

    //don't need this anymore
    password = null;

    //save the current session ID for later usage.
    if (login_status) {
        var session_token = arc.mf.client.RemoteServer.sessionId();
        
        // Gene: Non-page specific cookie so as to be shared if/when navigating (path=/)
        document.cookie="sessionid="+session_token+"; path=/";

//        check_for_system_message();
//
//        //from datapage.js
//        var valid_session = check_for_valid_session();
//        if(valid_session) {
//            check_for_system_message();
//            init_interface_for_path("/");
//            $( "#main-tabs a[href='#tab-data']" ).tab('show');
//            initialize_interface_after_login(); //datapage.js
//        }
        return true;
    }

    return false;
}

function frontpage_check_for_valid_session(){
    var valid_session = simple_check_for_valid_session();
    
    if(!valid_session) {
        render_mainview_for_noone();
    }

/*    
    if(valid_session) {
        render_toolbar_for_user(username);
//        $("#frontpage-login-container").addClass("hidden");
//        $("#frontpage-loggedin-container").removeClass("hidden");
    } else {
        render_mainview_for_noone();
//        $("#frontpage-login-container").removeClass("hidden");
//        $("#frontpage-loggedin-container").addClass("hidden");
    }
*/
    
}


function parse_command_xml(asset_xmlelement){
    // grabs all xml elements and attributes into JSON for easy access.
    // Nested elements simply get concatenated names.

    // same as parse_asset without certain asset parsing hacks. so, yeah, not ideal.

    if (typeof asset_xmlelement != "object"){
        return null;
    }

    var asset_metadata = {};

    if(asset_xmlelement.hasAttributes()){
        $.each(asset_xmlelement.attributes(), function(index, attribute){
            asset_metadata[attribute.name()] = attribute.value();
        });
    }

    if(asset_xmlelement.name()){
        var asset_name = '';
        asset_name = asset_xmlelement.name();

        if(asset_xmlelement.hasAttributes()){
            $.each(asset_xmlelement.attributes(), function(index, attribute){
                asset_metadata[attribute.name()] = attribute.value();
            });
        }
        if (asset_xmlelement.value()){
            asset_metadata[asset_name] = asset_xmlelement.value();
        }
    }


    $.each(asset_xmlelement.elements(), function(index, metadata) {

        var id_text = null;
        var local_name = '';

        local_name = metadata.name();

        if(metadata.elements()){
            local_json = {};

            $.each(metadata.elements(), function(index, submetadata) {
                if(submetadata.elements()){
                    //recurse as needed
                    local_json[submetadata.name()] = parse_command_xml(submetadata);
                } else {
                    local_json[submetadata.name()] = submetadata.value();
                }

                if(metadata.hasAttributes()){
                    $.each(metadata.attributes(), function(index, attribute){
                        if(attribute.value()){
                            local_json[attribute.name()] = attribute.value();
                        }
                    });
                }

                asset_metadata[local_name] = local_json;
            });
        } else {
            if(metadata.hasAttributes()){
                $.each(metadata.attributes(), function(index, attribute){
                    if(attribute.value()){
                        asset_metadata[attribute.name()] = attribute.value();
                    }
                });
            }

            asset_metadata[local_name] = metadata.value();
        }
    });
    return asset_metadata;
}


function livearc_execute_handler(command, command_args, suppress_error) {
    // command_args is of the type arc.xml.XmlStringWriter();
    var command_result = '';

    try {
        if(command_args){
            command_result = arc.mf.client.RemoteServer.execute(command, command_args.document());
        } else {
            command_result = arc.mf.client.RemoteServer.execute(command, null);
        }
    } catch(e1) {
        try {
            // try again
            if(command_args){
                command_result = arc.mf.client.RemoteServer.execute(command, command_args.document());
            } else {
                command_result = arc.mf.client.RemoteServer.execute(command, null);
            }
        } catch(e2){
            if (suppress_error){
                // let's just move on like nothing happened
                command_result = "error";
            } else if(typeof e2.message === 'function') {
                $('#alert_error_human_text').text('The server reported a problem. See details below');
                $("#alert_error_detail_text").text('Command: '+command+'<br>Response: '+e2.message());

                var email_text = "mailto:help@pawsey.org.au?&subject=Data%20Website%20Error&body="+e2.message();
                $("#error-email-link").attr('href',email_text);
                $('#myAlertMessageModal').modal('show');
                command_result = e2._type;
            }
        }
    }
    return command_result;
}


function simple_check_for_valid_session() {

    // See if we have a session cookie and if so, test it and update the UI accordingly.

    var status = false;

    var cookie = get_cookie("sessionid");

    if (cookie !== null && cookie.length > 1){
        arc.mf.client.RemoteServer.setSessionId(cookie);

        var command = livearc_execute_handler("system.session.self.describe", null, true);

        if (command === "arc.mf.client.ExNoSession" || command === "arc.mf.server.Services$ExServiceError" || command === "error"){
            //user needs to login, or there's something wrong with the server
            console.log('error - command', command);
            status = false;
        } else {
            var user_data = parse_command_xml(command);
            var username = user_data.session.user;
            console.log('username', username);
//            $("#frontpage-loggedin-container").empty();
//            $("#frontpage-loggedin-container").append("Welcome back,<br><strong>"+username+"</strong>");
            status = true;
        }
    }
    return status;
}

// Gene: Moved here to be generic across Admin and Dashboard pages 
function check_login_and_update(){
    $("#nav-login-button").html('<i class="fa fa-refresh fa-spin fa-1x fa-fw"></i> Checking');
    var username = $("#dashboard-navbar-username").val();
    var password = $("#dashboard-navbar-password").val();
    var status = '';

    if (login_with_name(username,password )){
        render_toolbar_for_user(username);
        render_mainview_for_user(username);
        
        // Gene: Added to write sessionid Cookie
        var session_token = arc.mf.client.RemoteServer.sessionId();
        
        // Gene: Non-page specific cookie so as to be shared if/when navigating (path=/)
        document.cookie="sessionid="+session_token+"; path=/";
        
    } else {
        console.log('no login');
        $("#nav-login-button").html('login');
    }
}


function logout_and_update(){
    var status;

    if (logout()){
        // Gene: Added to remove the client-side cookie on explicit logout
        var session_token = arc.mf.client.RemoteServer.sessionId();
        document.cookie="sessionid="+session_token+"; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;";

        render_mainview_for_noone();
        status = render_toolbar_for_noone();
    }
}

