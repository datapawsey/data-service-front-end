// MediaFlux local interface functions


// General message passing -- sending commands to mediaflux, and getting a response

function check_server(){
    //placeholder:  check for server being up and version or something
}


function livearc_async_handler(command, command_args, response_handler, error_handler){
      // command_args is of the type arc.xml.XmlStringWriter();
    var command_result = '';
    var optional_args = null;
    var output;

    command_result = arc.mf.client.RemoteServer.execute(command, command_args.document(), null, null ,new arc.mf.client.ExecuteResponseHandler(response_handler, error_handler), optional_args);

    return command_result;
}

function livearc_synchronous_handler(command, command_args, suppress_error) {
    // command_args is of the type arc.xml.XmlStringWriter();
    var command_result = '';

    try {
        if(command_args){
            command_result = arc.mf.client.RemoteServer.execute(command, command_args.document());
        } else {
            command_result = arc.mf.client.RemoteServer.execute(command, null);
            console.log('command_result',command_result);
        }
        console.log('command_result',command_result);

    } catch(error) {
        console.log('command_result',error);

        if (suppress_error){
            // let's just move on like nothing happened, but there'll be a hint that all did not go well
            command_result = "error";
        }
    }
    console.log('command_result',command_result);

    return command_result;
}


// Authentication --- logging in and logging output



function check_for_login_status(){
    var command = '';
    var status = '';
    if (command === "arc.mf.client.ExNoSession" || command === "arc.mf.server.Services$ExServiceError" || command === "error" || command === null){
        //user needs to login, or there's something wrong with the server
        status = false;
    } else {
        // we got a user description
        status = true;
    }
    return true;
}


function login_with_admin(username, password){
    var domain = "system";
    var login_status = true;

    try {
        arc.mf.client.RemoteServer.logon(domain,username,password);
    } catch (error) {
        login_status = false;
    }

    return login_status;
}


function login_with_name(username, password){
    var domain = "ivec";
    var login_status = true;

    if (username === 'manager'){
        domain = 'system';
    }

    try {
        arc.mf.client.RemoteServer.logon(domain,username,password);
    } catch (error) {
        console.log('error',error);
        login_status = false;
    }

    return login_status;
}


function login_with_cookie(){
    var status = true;
    var cookie = get_cookie("sessionid");

    if (cookie !== null && cookie.length > 1){
        arc.mf.client.RemoteServer.setSessionId(cookie);
        status = true;
    }
    return status;
}


function login_with_token(application_token){
    var command  = arc.mf.client.RemoteServer.logonWithToken(application_token);
    var sess_id = arc.mf.client.RemoteServer.sessionId();
    document.cookie="sessionid="+sess_id;
}


function logout(){
    var command = arc.mf.client.RemoteServer.logoff();
    return true;
}


function who_am_i(){
    var username = '';
    var command = livearc_synchronous_handler("system.session.self.describe", null, false);
    if (command === "arc.mf.client.ExNoSession" || command === "arc.mf.server.Services$ExServiceError" || command === "error" || command === null){
        //user needs to login, or there's something wrong with the server
        username = false;
    } else {
        // we got a user description
        var response = xmlToJson(command._xml);
        username = response.session.user["#text"];
    }
    return username;
}

function get_cookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}

// some general support routines

function check_for_valid_token(){
    login_with_cookie();
    var status = check_for_login_status();
    if (status){
        console.log('logged in');
    } else {
        console.log('not logged in');
    }

    return status;
}


//project controls


//tools
function xmlToJson(xml) {

    // Create the return object
    var obj = {};

    if (xml.nodeType == 1) { // element
        // do attributes
        if (xml.attributes.length > 0) {
        obj["@attributes"] = {};
            for (var j = 0; j < xml.attributes.length; j++) {
                var attribute = xml.attributes.item(j);
                obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
            }
        }
    } else if (xml.nodeType == 3) { // text
        obj = xml.nodeValue;
    }

    // do children
    if (xml.hasChildNodes()) {
        for(var i = 0; i < xml.childNodes.length; i++) {
            var item = xml.childNodes.item(i);
            var nodeName = item.nodeName;
            if (typeof(obj[nodeName]) == "undefined") {
                obj[nodeName] = xmlToJson(item);
            } else {
                if (typeof(obj[nodeName].push) == "undefined") {
                    var old = obj[nodeName];
                    obj[nodeName] = [];
                    obj[nodeName].push(old);
                }
                obj[nodeName].push(xmlToJson(item));
            }
        }
    }
    return obj;
};
