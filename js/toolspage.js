function initialise_tools_page(){
   // login start
    $("#download-legacytools-button").click(function() {download_legacy_tools();});

}

function download_legacy_tools(){
    download_ashell();
    download_aterm();
}


function download_ashell(){
    var index = 1;
    var public_user = false;

    if (arc.mf.client.RemoteServer.loggedOn()){
        var command = livearc_execute_handler("system.session.self.describe", null);
        var userinfo;

        file_url = "/projects/Data Team/scripts/ashell.py";

        if (command._xml){
            userinfo = xmlToJson(command._xml);
            if (userinfo.session.actor["#text"]){
                if ( userinfo.session.actor["#text"] === "public:public"){
                    file_url = "/Data Team/scripts/ashell.py";
                }
            }
        }
    } else {
        file_url = "/Data Team/scripts/ashell.py";
        quiet_public_logon();
        public_user = true;
    }

    var asset_id = get_asset_id(file_url);

    var filename = 'ashell.py';
    var content_url = arc.mf.client.RemoteServer.attachmentContentUrlDefaultVersion(asset_id);
    var download_command = download_file(content_url,index,filename);

}

function download_aterm(){
    var index = 2;
    var public_user = false;

    if (arc.mf.client.RemoteServer.loggedOn()){
        var command = livearc_execute_handler("system.session.self.describe", null);
        var userinfo;
        file_url = "/projects/Data Team/scripts/aterm.jar";

        if (command._xml){
            userinfo = xmlToJson(command._xml);
            if (userinfo.session.actor["#text"]){
                if ( userinfo.session.actor["#text"] === "public:public"){
                    file_url = "/Data Team/scripts/aterm.jar";
                }
            }
        }
    } else {
        file_url = "/Data Team/scripts/aterm.jar";
        quiet_public_logon();
        public_user = true;
    }

    var asset_id = get_asset_id(file_url);

    var filename = 'aterm.jar';
    var content_url = arc.mf.client.RemoteServer.attachmentContentUrlDefaultVersion(asset_id);
    var download_command = download_file(content_url,index,filename);

}
