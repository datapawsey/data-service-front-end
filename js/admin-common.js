
function initialise_page() {
    // login start
    render_toolbar_for_noone();
    render_mainview_for_noone();
    
    initialise_front_page();
    
    if (simple_check_for_valid_session()) {
//        console.log('valid session');
        var username = who_am_i();
//        console.log('username', username);
        if(username != false){
            render_toolbar_for_user(username);
            render_mainview_for_user(username);
        }
    }
//    else {
//        console.log('invalid session');
//    }

    render_footer();
    
}


// render functions
function render_toolbar_for_noone(){
    $.get('../templates/main-navbar-loggedout.mustache', function(template) {
        var rendered = Mustache.render(template);
        $('#main-navbar').html(rendered);
        $("#nav-login-button").click(function(){check_login_and_update();});
    });
}


function render_toolbar_for_user(username){
    $.get('../templates/main-navbar-loggedin.mustache', function(template) {
        var rendered = Mustache.render(template, {username: username});
        $('#main-navbar').html(rendered);
        $("#nav-logout-button").click(function(){logout_and_update();});
    });
}


function render_mainview_for_noone(){
    $.get('../templates/main-navbar-loggedout.mustache', function(template) {
        var rendered = Mustache.render(template);
        // Gene: Is this the correct ID?
        $('#main-tools-view').html(rendered);
    });
    $('#main-system-view').empty();
    $('#main-project-browser-view').empty();
    $('#main-project-detail-view').empty();
}


function display_error_dialog(human_message, detailed_message, email_addendum){
        $('#alert_error_human_text').text(human_message);
        $("#alert_error_detail_text").text(detailed_message);

        var email_text = "mailto:help@pawsey.org.au?&subject=Data%20Website%20Error&body="+detailed_message+".  Extra detail:"+email_addendum;
        $("#error-email-link").attr('href',email_text);
        $('#myAlertMessageModal').modal('show');
}


function render_error_message(message){
//    console.log(message.message());
    
    // Hide the currently open modal window as only one modal supported
    $('#addProjectModal').modal('hide');
    $('#addMemberModal').modal('hide');
    $('#modifyRoleModal').modal('hide');
    
    var human_message = "Unable to create/update Project";
    var detailed_message = "The Mediaflux error was: "+message;

    display_error_dialog(human_message, detailed_message, null);
}


function display_project_details(linkObject) {
//    console.log('linkObject', linkObject);
    
    var projectName = linkObject.id;
    var showMembers = true;
//    console.log('display_project_details', projectName);

    project_get(projectName, showMembers, render_project_details, render_error_message);
}


function validate_search_text(event) {
    if ($("#input-project-filter-field").val().length > 0 ){
        $("#project-filter-clear-button").show();
        $("#project-filter-execute-button").removeClass("disabled");
//        $("#search-recursive-button").removeClass("disabled");
    } else {
        $("#project-filter-clear-button").hide();
        $("#project-filter-execute-button").addClass("disabled");
//        $("#search-recursive-button").addClass("disabled");
    }

    // if the key is return, do the search
    if(event) {
        if (event.keyCode === 13){
            simple_text_search($("#input-project-filter-field").val(), $("#file-table").attr("data-current-page-size"), 1);
        }
    }

    // Hide the detail panel on search, to trigger repopulation when a project is re-selected
    $('#detailpanel').hide();
    
}

// simple search text and control
function clear_search_field() {
    $("#input-project-filter-field").val("");
    validate_search_text();

    var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
//    complex_query_namespace($("#file-table").attr("data-current-namespace"), current_page_size, 1);
    $("#file-table").attr("data-current-search","");
//    $("#search-recursive-button").prop("checked",false);
}


function simple_text_search(search_text, page_size, page) {
    var username = who_am_i();
    
    $.get('project-browser.mustache', function(template) {
        var rendered = Mustache.render(template, {username: username});
        $('#main-project-browser-view').html(rendered);
        
//        var filter = '*' + search_text + '*';
//        project_list_filter(filter, render_project_browser_for_list, render_error_message);
        
        var projectName = null;
        var showMembers = true;

        project_get(projectName, showMembers, render_project_browser_for_list, render_error_message);
        
        $("#input-project-filter-field").val(search_text);
    });
    
//    $("#file-table").attr("data-current-search",search_text);
//
//    function populate_search_callback(response){
//        var search_contents = xmlToJson(response._xml);
//        $("#file-table").attr("data-current-page-size",parseInt(search_contents.parent.size["#text"]))
//        update_pager_controls(parseInt(search_contents.parent.page["#text"]), parseInt(search_contents.parent.assets["#text"]), parseInt(search_contents.parent.last["#text"]), parseInt(search_contents.parent.size["#text"]));
//        populate_file_search_json(search_contents.asset,search_text);
//        setup_file_pager_controls_search();
//    }
//
//    function populate_search_callback_error(response){
//        console.log('populate_search_callback_error');
//    }
//
//    $("#file-table").attr("data-current-page",page);
//    var search_namespace = $("#file-table").attr("data-current-namespace");
//    var search_recursive = false;
//
//    //recursive?
//    if ($("#search-recursive-button").prop("checked")){
//        search_recursive = true;
//    }
//
    
/*    
    if (search_text.length > 0) {
        // sanitise search string?

        var args = new arc.xml.XmlStringWriter();
//        args.add("filter",search_text);
//        args.add("namespace", search_namespace);
//        args.add("recurse", search_recursive);
//        args.add("size", page_size);
//        args.add("page", page);
//
//        livearc_async_generic_handler("www.list", args, populate_search_callback, populate_search_callback_error);
    } else {
        clear_search_field();
    }
*/    
    
}


function render_common_footer(footerText){
    $.get('../templates/adminpage-footer.mustache', function(template) {
        var rendered = Mustache.render(template, {footerText: footerText});
        $('#main-footer-view').html(rendered);
    });
}

