
var _current_document_id = -1;
var _application_token = -1;

function application_logon_handler() {
    // This gets called when the livearc logon token times out
    var command  = arc.mf.client.RemoteServer.logonWithToken(_application_token);
}

function init_review_display(){

    $.ajaxSetup ({
        // Disable caching of AJAX responses
        cache: false
    });

    arc.mf.client.RemoteServer.setLogonHandler(function (){application_logon_handler();});

    var application_id = "";
    var application_token = "";

    application_id = get_parameter_by_name('id').replace(/\s/g,"");
    application_token = get_parameter_by_name('token').replace(/\s/g,"");

    _application_token = application_token;
    _current_document_id = application_id;

    if (application_id.length < 1 || application_token.length < 10 ){
        //token or is is missing
    } else {
        logon_with_token(application_token);
        show_application_review(application_id);
    }

}


function show_application_review(asset_id){

    function get_content_success(response) {
        var asset_data = xmlToJson(response._xml);
        var application = xmlToJson(response.element("asset/content/xml").xml());

        console.log(application);

        var application_version = 1;

        if (application.application.hasOwnProperty("version")){
          application_version = application.application.version["#text"];
        }

        var application_id = asset_data.asset["@attributes"].id;

        var collection_name = application.application.collection_name["#text"];

        var pi_firstname = application.application.principal_firstname["#text"];
        var pi_lastname = application.application.principal_lastname["#text"];
        var pi_institution = application.application.principal_institution["#text"];
        var pi_position = application.application.principal_position["#text"];
        var pi_phone = application.application.principal_phone["#text"];
        var pi_email = application.application.principal_email["#text"];

        var dep_firstname = application.application.deputy_firstname["#text"];
        var dep_lastname = application.application.deputy_lastname["#text"];
        var dep_institution = application.application.deputy_institution["#text"];
        var dep_position = application.application.deputy_position["#text"];
        var dep_phone = application.application.deputy_phone["#text"];
        var dep_email = application.application.deputy_email["#text"];

        var submission_date = asset_data.asset.ctime["#text"];
        var request_size = application.application.project_request_total["#text"];

        var milestone_1 = application.application.project_request_milestone1["#text"];
        var milestone_2 = application.application.project_request_milestone2["#text"];
        var milestone_3 = application.application.project_request_milestone3["#text"];
        var milestone_4 = application.application.project_request_milestone4["#text"];
        var for_code_list = application.application.project_for_code_list["#text"];

        var latency_flag =  application.application.project_storage_latency["#text"];
        var latency_comment = application.application.project_storage_latency_comment["#text"];

        var description = application.application.project_description["#text"];
        var community = application.application.project_community["#text"];
        var funding = application.application.project_funding["#text"];
        var future = application.application.project_future["#text"];
        var management = "";
        if (application.application.hasOwnProperty("project_management")){
            management = application.application.project_management["#text"];
        }
        var encumberance = application.application.project_encumberance["#text"];
        var links = application.application.project_links["#text"];

        var comments = application.application.project_additional_comments["#text"];

        $("#application-detail-view").attr("data-application-detail-id",application_id);

        $("#application-detail-title").text(collection_name);
        $("#application-detail-size").text(request_size);
        $("#application-detail-date-submitted").text(submission_date);

        $("#principal-ident .ident-username").text(pi_firstname+" "+pi_lastname);
        $("#principal-ident .ident-position").text(pi_position);
        $("#principal-ident .ident-institution").text(pi_institution);
        $("#principal-ident .ident-phonenumber").text(pi_phone);
        $("#principal-ident .ident-emailaddress").text(pi_email);


        $("#secondary-ident .ident-username").text(dep_firstname+" "+dep_lastname);
        $("#secondary-ident .ident-position").text(dep_position);
        $("#principal-ident .ident-institution").text(dep_institution);
        $("#secondary-ident .ident-phonenumber").text(dep_phone);
        $("#secondary-ident .ident-emailaddress").text(dep_email);


        $("#application-detail-mile-total").text(request_size);
        $("#application-detail-mile-1").text(milestone_1);
        $("#application-detail-mile-2").text(milestone_2);
        $("#application-detail-mile-3").text(milestone_3);
        $("#application-detail-mile-4").text(milestone_4);
        $("#application-detail-latency-flag").text("No");

        if (latency_flag === "low"){
            $("#application-detail-latency-flag").text("Yes");
        }
        $("#application-detail-latency-comment").text(latency_comment);

        populate_for_code_list(for_code_list);

        $("#application-detail-description").text(description);
        $("#application-detail-community").text(community);
        $("#application-detail-funding").text(funding);
        $("#application-detail-future").text(future);
        $("#application-detail-encumberance").text(encumberance);
        $("#application-detail-management").text(management);

        $("#application-detail-links").text(links);

        $("#application-additional-comments").text(comments);

        $("#approve-application-button").unbind("click").click(function(e){
            approve_application($("#application-detail-view").attr("data-application-detail-id"));
        });
        $("#decline-application-button").unbind("click").click(function(e){
            decline_application($("#application-detail-view").attr("data-application-detail-id"));
        });
        $("#application-container").removeClass("hidden");
    }

    function  get_content_failure(response){
        console.log('get_content_failure',response);
    }

    $("#application-list-view").addClass("hidden");
    $("#application-detail-view").removeClass("hidden");

    var args = new arc.xml.XmlStringWriter();

    args.add("id", asset_id);
    args.add("content-as-xml", true);

    livearc_async_generic_handler("asset.get", args, get_content_success,  get_content_failure);
}

function populate_for_code_list(packed_list){
    // takes the picked string from the data model in the form code:percent,code:percent
    // and populates the UI

    if(packed_list != "" && packed_list != undefined){

        var code_list = packed_list.split(",");

        $.each(code_list, function(index,item){
            values = item.split(":");
            $("#project-for-code-list").append('<li><div class="">'+values[0]+' ('+values[1]+'%)</div> </li>');
        });


    }
}
