function project_create(description, name, quota, store, update_interface, update_interface_fail){
    function project_create_callback(response){
        var response_contents = xmlToJson(response._xml);
        update_interface();
    }

    function project_create_callback_error(response){
        console.log('project_create bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("description",description);
    args.add("name",name);
    args.add("quota",quota);
    args.add("store",store);

    livearc_async_handler("project.create", args, project_create_callback, project_create_callback_error);
}


function project_modify(description, name, quota, update_interface, update_interface_fail){
    function project_modify_callback(response){
        var response_contents = xmlToJson(response._xml);
        update_interface();
    }

    function project_modify_callback_error(response){
        console.log('project_create bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();
    
    args.add("name",name);
    args.add("quota",quota);
    
    if (description != null) {
        args.add("description",description);
    }
    
    livearc_async_handler("project.modify", args, project_modify_callback, project_modify_callback_error);
}


function project_grant(administer, name, readonly, readwrite, update_interface, update_interface_fail){
    function project_grant_callback(response){
        var response_contents = xmlToJson(response._xml);
        update_interface();
    }

    function project_grant_callback_error(response){
        console.log('project_grant bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();
    
    args.add("name",name);

    if (administer != null) {
        args.add("administer",administer);
    }
    if (readwrite != null) {
        args.add("readwrite",readwrite);
    }
    if (readonly != null) {
        args.add("readonly",readonly);
    }
    
    livearc_async_handler("project.grant", args, project_grant_callback, project_grant_callback_error);
}


function project_revoke(administer, name, readonly, readwrite, update_interface, update_interface_fail){
    function project_revoke_callback(response){
        var response_contents = xmlToJson(response._xml);
        update_interface();
    }

    function project_revoke_callback_error(response){
        console.log('project_revoke bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();

    args.add("name",name);

    if (administer != null) {
        args.add("administer",administer);
    }
    if (readwrite != null) {
        args.add("readwrite",readwrite);
    }
    if (readonly != null) {
        args.add("readonly",readonly);
    }

    livearc_async_handler("project.revoke", args, project_revoke_callback, project_revoke_callback_error);
}


function project_destroy(name, destroy_data, update_interface, update_interface_fail){
    function project_destroy_callback(response){
        var response_contents = xmlToJson(response._xml);
        update_interface();
    }

    function project_destroy_callback_error(response){
        console.log('project_destroy bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("name",name);
    args.add("destroy-data",destroy_data);

    livearc_async_handler("project.destroy", args, project_get_callback, project_get_callback_error);
}


function project_describe(name, show_members, update_interface, update_interface_fail){
    function project_describe_callback(response){
        var response_contents = xmlToJson(response._xml);
        console.log("project_describe response_contents=", response_contents);
        update_interface(response_contents);
    }

    function project_describe_callback_error(response){
        console.log('project_describe bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("name",name);
//    args.add("show-members",show_members);

    livearc_async_handler("project.describe", args, project_describe_callback, project_describe_callback_error);
}


function project_get(name, show_members, update_interface, update_interface_fail){
    function project_get_callback(response){
        var response_contents = xmlToJson(response._xml);
        update_interface(response_contents);
    }

    function project_get_callback_error(response){
        console.log('project_get bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();
    
    // Provide for null being passed as name, as project.get will return the full list of projects if null
    if (name != null) {
        args.add("name",name);
    }

    args.add("show-members",show_members);

    livearc_async_handler("project.get", args, project_get_callback, project_get_callback_error);
}


function project_list(update_interface, update_interface_fail){
    function project_list_callback(response){
        var response_contents = xmlToJson(response._xml);
        console.log('project_list',response_contents);
        update_interface(response_contents);
    }

    function project_list_callback_error(response){
        console.log('project_list bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();

    livearc_async_handler("project.list", args, project_list_callback, project_list_callback_error);
}


function project_list_filter(filter, update_interface, update_interface_fail){
    function project_list_filter_callback(response){
        var response_contents = xmlToJson(response._xml);
        update_interface(response_contents);
    }

    function project_list_filter_callback_error(response){
        console.log('project_list_filter bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("filter", filter);

    livearc_async_handler("project.list", args, project_list_filter_callback, project_list_filter_callback_error);
}


function project_store_list(namespace, update_interface, update_interface_fail){
    function project_store_list_callback(response){
        var response_contents = xmlToJson(response._xml);
        console.log('project_store_list_callback',response_contents);
        update_interface(response_contents);
    }

    function project_store_list_callback_error(response){
        console.log('project_store_list bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("namespace",namespace);

    livearc_async_handler("asset.namespace.application.settings.get", args, project_store_list_callback, project_store_list_callback_error);
}


function project_mount(name, update_interface, update_interface_fail) {
    function project_mount_callback(response){
        var response_contents = xmlToJson(response._xml);
        console.log('project_mount_callback',response_contents);
        update_interface(response_contents);
    }

    function project_mount_callback_error(response){
        console.log('project_mount bad');
        update_interface_fail(response);
    }

    var args = new arc.xml.XmlStringWriter();
    args.add("name",name);

    livearc_async_handler("asset.store.mount", args, project_mount_callback, project_mount_callback_error);
}


function quotaBytes_to_GB(quotaBytes, reverse) {
    var bytesToGB = 1000000000;
    var quotaGB = 0;
    
    if (reverse) {
        quotaGB = quotaBytes * bytesToGB;
    }
    else {
        quotaGB = quotaBytes/bytesToGB;
    }
    
    return quotaGB.toFixed(2);
}

