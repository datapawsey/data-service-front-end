// worker javascript for synchronous file upload without main thread blocking

var sequence = 0;
var file_list = [];

self.addEventListener('message', function(e) {
    var data = e.data;
    if (e.data === 'next'){
        sequence = sequence + 1;
        if (sequence === file_list.length){
            self.postMessage('done');

        } else {
            upload_file(file_list[sequence]);
        }
    } else if (e.data === 'stop'){
        //Stop processing. placeholder in case we want to add any logic
    } else {
        //build file list and start upload
        file_list = data;
        upload_file(file_list[sequence]);
    }
}, false);

function upload_file(file_object){
    var current_path = file_object.path;
    var file = file_object.file;
    var session_id = file_object.session_id;

    var command_result = '';
    var optional_args = '';

    var post_url_head = '/__mflux_svc__'
    var command_xml = '<request><service name="service.execute" session="'+ session_id +'" seq="'+sequence+'"><args><reply>last</reply><service name="asset.create"><name>'+file.name+'</name><namespace>'+current_path+'</namespace></service></args></service></request>';

    var formData = new FormData();
    formData.append('request',command_xml);
    formData.append(file.name, file);

    var request = $.ajax({
        url: post_url_head,
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done( function(data){
            query_namespace(current_path);
    }).fail( function(XMLHttpRequest, textStatus, errorThrown) { 
            command_result = "Error: " + errorThrown; 
    }).always( function( jqXHR, textStatus,errorThrown ) {
            self.postMessage(textStatus+'::'+file.name);
    });

    // var formData = new FormData();
    // formData.append('request',command_xml);
    // formData.append(file.name, file);

    // var request = $.ajax({
    //     url: post_url_head,
    //     type: "POST",
    //     data: formData,
    //     cache: false,
    //     contentType: false,
    //     processData: false,
    //     success: function(data){
    //         self.postMessage('done');
    //     },
    //     error: function(XMLHttpRequest, textStatus, errorThrown) { 
    //         self.postMessage('failed');
    //     } 
    // });
}


// https://gist.github.com/lemonhall/3120320

/*
 * FormData for XMLHttpRequest 2  -  Polyfill for Web Worker  (c) 2012 Rob W
 * License: Creative Commons BY - http://creativecommons.org/licenses/by/3.0/
 * - append(name, value[, filename])
 * - toString: Returns an ArrayBuffer object
 * 
 * Specification: http://www.w3.org/TR/XMLHttpRequest/#formdata
 *                http://www.w3.org/TR/XMLHttpRequest/#the-send-method
 * The .append() implementation also accepts Uint8Array and ArrayBuffer objects
 * Web Workers do not natively support FormData:
 *                http://dev.w3.org/html5/workers/#apis-available-to-workers
 **/
