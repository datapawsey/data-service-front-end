// worker javascript for synchronous file checking

self.addEventListener('message', function(e) {
    check_file_list(e.data);
}, false);

function check_file_list(file_reference_array){
    var file_only_array = [];

    var i = 0;
    var reader = new FileReaderSync();

    for (i = 0; i < file_reference_array.length; i++){

        if (file_reference_array[i].size < 200000){
            try {
                // its a file
                var buffer = reader.readAsArrayBuffer(file_reference_array[i]);
                file_only_array.push(file_reference_array[i]);
            } catch (e){
                // is a dir do nothing, probably
            }
        } else {
            // it's a file
            file_only_array.push(file_reference_array[i]);
        }
    }
    self.postMessage(file_only_array);
}