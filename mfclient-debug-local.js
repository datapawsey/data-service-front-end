// Arcitecta Pty. Ltd.
//
//  Mediaflux Client API for Ajax
//

// Namespaces..
arc           = {version: '0.1'};
arc.xml       = { };
arc.mf        = { };
arc.mf.client = { };


/**
 * AJAX utilities.
 *
 */
arc.mf.client.Ajax = function() {

    function evaluate(xmlHttp,url) {

        var script = xmlHttp.responseText;

        // Defeat caching (for IE)..
        if ( !xmlHttp.getResponseHeader("Date") ) {

            var cached = xmlHttp;

            var ifModifiedSince = cached.getResponseHeader("Last-Modified");
            ifModifiedSince = (ifModifiedSince) ? ifModifiedSince : new Date(0); // January 1, 1970

            xmlHttp = arc.mf.client.Ajax.createXmlHttpRequest();
            xmlHttp.open("GET", url, false);
            xmlHttp.setRequestHeader("If-Modified-Since", ifModifiedSince);
            xmlHttp.send(null);
            if ( xmlHttp.status == 304 ) {
                script = cached.responseText;
            }
        }

        eval(script);
    }

    function load(url,responseHandler,scope) {
        var xmlHttp = arc.mf.client.Ajax.createXmlHttpRequest();

        var async = responseHandler? true : false;

        xmlHttp.open("GET",url,async);

        var req = new arc.mf.client.RemoteServerRequest(xmlHttp);

        if ( async ) {
            xmlHttp.onreadystatechange = function() {
                if ( !req.aborted() ) {
                    if (xmlHttp.readyState == 4 ) {
                        if ( xmlHttp.status === 500 ) {
                            responseHandler.apply(scope,[ "Internal Server Error" ]);
                        } else if ( xmlHttp.status === 200 ) {
                            try {
                                evaluate(xmlHttp,url);
                                responseHandler.apply(scope,[ null ]);
                            } catch ( ex ) {
                                var msg;

                                // IE does not always report error properly with toString function,
                                // so if there is a name (field) then use that.
                                if (ex.name) {
                                    msg = ex.name + ": " + ex.message;
                                } else {
                                    msg = ex.toString();
                                }

                                responseHandler.apply(scope,[ "Script processing error (" + url + "): " + msg]);
                            }
                        } else {
                            responseHandler.apply(scope, [ "Error loading (" + url + "): error code: " + xmlHttp.status]);
                        }
                    }
                }
            }
        }

        xmlHttp.send(null);

        if ( async ) {
            return req;
        }

        evaluate(xmlHttp,url);
        return null;
    }

    return {

        createXmlHttpRequest: function() {
            var xmlHttp;

            // Try to get the right object for different browser
            try {
                //IE10, IE11.
                xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e1) {
                try {
                    // Firefox, Opera 8.0+, Safari, IE7+
                    xmlHttp = new XMLHttpRequest(); // xmlHttp is now a XMLHttpRequest.
                } catch(e2){
                // Internet Explorer
                    try {
                       xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
                    } catch (e3) {
                       xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
                    }
                }
            }

            return xmlHttp;
        },

        /**
         * Load and evaluate a script - synchronously, or asynchronously.
         *
         * @param {Object} url The URL of the script to evaluate.
         * @param {Object} responseHandler If null, or undefined, then synchronous. If set, then will be called upon completion.
         * @param {Object} scope
         */
        loadAndEvaluateScript: function(url, responseHandler, scope) {
            return load(url,responseHandler,scope);
        }
    }
}();


/**
 * Does the browser support XPath? If not we assume it is built
 * into the browser.
 */
var BROWSER_HAS_XPATH_3 = false;
if ( document.implementation.hasFeature("XPath", "3.0") ) {
    BROWSER_HAS_XPATH_3 = true;
}


/*
arc.xml.XPathPrototype = function(xdoc,xml) {

    xdoc.prototype.selectNodes = function(cXPathString, xNode) {
        if ( !xNode ) { xNode = this; }

        // Use our own resolver that simply returns the prefix - we
        // don't care about validating the namespaces - they either
        // exist or they don't..
        var nsResolver = function(prefix) {
            return prefix;
        }

        var oNSResolver = nsResolver; // this.createNSResolver(this.documentElement)
        var aItems = this.evaluate(cXPathString, xNode, oNSResolver, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null)
        var aResult = [];
        for (var i = 0; i < aItems.snapshotLength; i++){
            aResult[i] =  aItems.snapshotItem(i);
        }
        return aResult;
    };

    xdoc.prototype.selectSingleNode = function(cXPathString, xNode) {
        if (!xNode ) { xNode = this; }
        var xItems = this.selectNodes(cXPathString, xNode);
        if ( xItems.length > 0 ) {
            return xItems[0];
        } else {
            return null;
        }
    };

    xml.prototype.selectNodes = function(cXPathString) {
        if (this.ownerDocument.selectNodes) {
            return this.ownerDocument.selectNodes(cXPathString, this);
        } else {
            throw "selectNodes is for XML elements only";
        }
    };

    xml.prototype.selectSingleNode = function(cXPathString){
        if (this.ownerDocument.selectSingleNode) {
            return this.ownerDocument.selectSingleNode(cXPathString, this);
        } else {
            throw "selectNode is for XML elements only";
        }
    };
};
*/

// Add selectSingleNode and selectNodes functions..
// These are not standard parts of Firefox..
/* if ( document.implementation.hasFeature("XPath", "3.0") ) {
    if( typeof XMLDocument == "undefined" ) { XMLDocument = Document; }

    arc.xml.XPathPrototype(XMLDocument,Element);


    XMLDocument.prototype.selectNodes = function(cXPathString, xNode) {
        if ( !xNode ) { xNode = this; }
        var oNSResolver = this.createNSResolver(this.documentElement)
        var aItems = this.evaluate(cXPathString, xNode, oNSResolver, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null)
        var aResult = [];
        for (var i = 0; i < aItems.snapshotLength; i++){
            aResult[i] =  aItems.snapshotItem(i);
        }
        return aResult;
    };

    XMLDocument.prototype.selectSingleNode = function(cXPathString, xNode) {
        if (!xNode ) { xNode = this; }
        var xItems = this.selectNodes(cXPathString, xNode);
        if ( xItems.length > 0 ) {
            return xItems[0];
        } else {
            return null;
        }
    };

    Element.prototype.selectNodes = function(cXPathString) {
        if (this.ownerDocument.selectNodes) {
            return this.ownerDocument.selectNodes(cXPathString, this);
        } else {
            throw "For XML Elements Only";
        }
    };

    Element.prototype.selectSingleNode = function(cXPathString){
        if (this.ownerDocument.selectSingleNode) {
            return this.ownerDocument.selectSingleNode(cXPathString, this);
        } else {
            throw "For XML Elements Only";
        }
    };
    * */
// }


/**
 * <p>A writer is a utility class to create XML documents to send to the server. It represents the document as a stack.</p>
 * <p>Elements are pushed, added and popped from the stack with optional attributes and values.</p>
 * <p>The completed document is returned using the <i>document</i> method.</p>
 *
 * @class A writer for creating XML to send to the server.
 * @constructor
 */
arc.xml.XmlStringWriter = function() {
    // Member variables..
    this._elements = new Array();
    this._document = "";


    this.addNsDecl = function(element,attrs) {
        // We need to include an XML namespace declaration if the element is
        // in a namespace - we can fudge one..
        var idx = element.indexOf(':');
        if ( idx !== -1 ) {
            // attrs = [];

            var ns = element.substring(0,idx);

            var xns = "xmlns:" + ns;

            // Check to see if was defined..
            if ( attrs ) {
                for ( var i=0; i < attrs.length; i+=2 ) {
                    if ( attrs[i] == xns ) {
                        return;
                    }
                }
            }

            this._document += " " + xns + "=\"" + ns + "\"";
        }

    };


   /**
    * Push an element onto the stack of elements.
    *
    * @param {String} element The name of the element.
    * @param {String[]} [attrs] Optional array of key/value pairs. E.g. [ "key", "value" ]
    */
    this.push = function(element,attrs) {
        this._elements.push(element);
        this._document += "<" + element;

        this.addNsDecl(element,attrs);

        if ( attrs !== undefined && attrs !== null ) {
            for ( var i=0; i < attrs.length; i+=2 ) {
                if ( attrs[i+1] !== null ) {
                    this._document += " " + attrs[i] + "=\"" + attrs[i+1] + "\"";
                }
            }
        }

        this._document += ">";
    };

    /**
     * Remove XML nasty characters.
     *
     * @private
     */
    function safeXMLValue(value) {
        var s = value.toString();
        s = s.replace(/[\"]/g, "&quot;");
        s = s.replace(/[\&]/g, "&amp;");
        s = s.replace(/[\<]/g, "&lt;");
        s = s.replace(/[\>]/g, "&gt;");

        return s;
    };


    /**
     * Add an element to the document. The argument may have two or three arguments. If two, then of the
     * form: add(element,value), and if three, then of the form: add(element,attrs,value)
     *
     * @param {String} element The name of the element.
     * @param {String[]} attrs Optional array of key/value pairs. Set to null if there are no attributes.
     * @param {Object} value Value for the element. Will be converted to a string.
     */
    this.add = function(element,attrs,value) {

        if ( arguments.length == 2 ) {
            if ( typeof attrs != 'Array' ) {
                value = attrs;
                attrs = null;
            }
        }

        this._document += "<" + element;

        this.addNsDecl(element,attrs);

        if ( attrs !== null ) {
            for ( var i=0; i < attrs.length; i+=2 ) {
                if ( attrs[i+1] !== null ) {
                    this._document += " " + attrs[i] + "=\"" + attrs[i+1] + "\"";
                }
            }
        }

        this._document += ">";

        if ( value !== undefined && value !== null ) {
            this._document += safeXMLValue(value);
        }

        this._document += "</" + element + ">";
    };

    /**
     * Adds the given (well formed) XML fragment to the document. It is assumed that the
     * fragment is well-formed XML (e.g. does not contain invalid XML characters, etc.).
     *
     * @param {String} f The XML document fragment to add.
     */
    this.addXmlFragment = function(f) {
        this._document += f;
    };

    /**
     * Pops the current element from the stack.
     */
    this.pop = function() {
        var element = this._elements.pop();
        this._document += "</" + element + ">";
    };

    /**
     * Pops all of the elements currently pushed onto the stack.
     */
    this.popAll = function() {
        while ( this._elements.length > 0 ) {
            this.pop();
        }
    };

    /**
     * Automatically pops all elements from the stack and returns the complete XML document.
     *
     * @type String
     */
    this.document = function() {
        this.popAll();
        return this._document;
    };
};

// =============================
// Class: arc.xml.XmlAttribute
// =============================

/**
 * An XML attribute node.
 *
 * @class An XML attribute node.
 * @constructor
 *
 * @param xml The DOM object for this node.
 */
arc.xml.XmlAttribute = function(xml) {
    this._xml = xml;

    /**
     * The name of the attribute.
     *
     * @type String
     * @return The attribute name.
     */
    this.name = function() {
        return this._xml.nodeName;
    };

    /**
     * The value associated with this attribute.
     *
     * @type String
     * @return The value of this attribute.
     */
    this.value = function() {
    if ( this._xml.nodeValue ) {
        return this._xml.nodeValue;
    }

    if ( this._xml.firstChild ) {
            return this._xml.firstChild.nodeValue;
    }

    return null;
    };

};


/**
 * Converts this attribute to a string.
 */
arc.xml.XmlAttribute.prototype.toString = function() {
  return "-" + this.name() + " \"" + this.value() + "\"";
};

// =============================
// Class: arc.xml.XmlElement
// =============================

/**
 * An XML element node.
 *
 * @class An XML element node.
 * @constructor
 * @param xml The DOM object for this node.
 */
arc.xml.XmlElement = function(doc,xml) {
    this._doc = doc;
    this._xml = xml;

    /**
     * The name of the element.
     *
     * @type String
     * @return The name of the element.
     */
    this.name = function() {
        return this._xml.nodeName;
    };

    /**
     * The underlying XML DOM object.
     *
     * @private
     *
     */
    this.xml = function() {
        return this._xml;
    };

    function selectNodes(doc,node,path) {

        if ( !BROWSER_HAS_XPATH_3 ) {
            return node.selectNodes(path);
        }

        // Use our own resolver that simply returns the prefix - we
        // don't care about validating the namespaces - they either
        // exist or they don't..
        var nsResolver = function(prefix) {
            return prefix;
        }


        //var expr = doc.createExpression(path,nsResolver);
        //var aItems = expr.evaluate(node, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);

        var aItems = doc.evaluate(path, node, nsResolver, 7, null); // XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
        var aResult = [];
        for (var i = 0; i < aItems.snapshotLength; i++){
            aResult[i] =  aItems.snapshotItem(i);
        }
        return aResult;

    };

    function selectSingleNode(doc,node,path) {
        if ( !BROWSER_HAS_XPATH_3 ) {
            return node.selectSingleNode(path);
        }

        var xItems = selectNodes(doc,node,path);
        if ( xItems.length > 0 ) {
            return xItems[0];
        } else {
            return null;
        }

    };

    /**
     * Retrieves an element using the given XPath.
     *
     * @param {String} xpath An XPath expression to select the element.
     *
     * @type arc.xml.XmlElement
     * @return An element, if found, or null if no matching element.
     */
    this.element  = function(xpath) {
        var node = selectSingleNode(this._doc,this._xml,xpath);
        if ( !node ) {
            return null;
        }

        if ( node.nodeType != 1 ) {
          return null;
        }

        return new arc.xml.XmlElement(this._doc,node);
    };

    /**
     * Indicates whether this element contains other nodes
     * with the optional given path.
     *
     * @param {String} [xpath] An XPath expression to select an element. If not specified, then indicates whether this element contains any other elements.
     *
     * @type Boolean
     * @return true if there are matching elements and false if not.
     */
    this.hasElement = function(xpath) {
        if ( xpath !== undefined ) {
            var nodes = selectNodes(this._doc,this._xml,xpath);
            if ( nodes === null ) {
                return false;
            }

            if ( nodes.length === 0 ) {
                return false;
            }

            return true;
        }

        var n = this._xml.firstChild;
        while ( n ) {
            if ( n.nodeType === 1 ) {
                return true;
            }

            n = n.nextSibling;
        }

        return false;
    };

    /**
     * Retrieves an array of elements matching the given XPath.
     *
     * @param {String} xpath An XPath expression to select elements.
     *
     * @type arc.xml.XmlElement[]
     * @return The matched elements, or null if none found.
     *
     */
    this.elements = function(xpath) {
        var nodes;
        if ( xpath ) {
            nodes = selectNodes(this._doc,this._xml,xpath);
        } else {
            var n = this._xml.firstChild;
            nodes = null;
            while ( n ) {
                if ( n.nodeType === 1 ) {
                    if ( !nodes ) {
                        nodes = new Array();
                    }

                    nodes.push(n);
                }

                n = n.nextSibling;
            }
        }

        if ( !nodes ) {
            return null;
        }

        if ( nodes.length === 0 ) {
            return null;
        }

        var xes = new Array(nodes.length);
        for ( i=0; i < nodes.length; i++ ) {
            if ( nodes[i].nodeType === 1 ) {
                xes[i] = new arc.xml.XmlElement(this._doc,nodes[i]);
            }
        }

        if ( xes.length === 0 ) {
            return null;
        }

        return xes;
    };

    /**
     * Indicates whether this element has any attributes or not.
     *
     * @type Boolean
     * @return true if the element has attributes, and false if not.
     */
    this.hasAttributes = function() {
        var xas = this._xml.attributes;
        if ( !xas || xas.length === 0 ) {
            return false;
        }

        return true;
    };

    /**
     * Retrieves all of the attributes associated with this element.
     *
     * @type arc.xml.XmlAttribute[]
     * @return The attributes, if any, or null.
     */
    this.attributes = function() {
        var xas = this._xml.attributes;
        if ( !xas || xas.length === 0 ) {
            return null;
        }

        var attributes = new Array();
        for ( i=0; i < xas.length; i++ ) {
            var xa = xas.item(i);
            if ( xa.nodeType === 2 ) {
                attributes.push(new arc.xml.XmlAttribute(xa));
            }
        }

        return attributes;
    };

    function retrieveValue(node) {
        // OK, as of FF 14, the node may have the value directly, rather than within sub
        // nodes.
        if ( node.nodeValue ) {
            return node.nodeValue;
        }

        if ( !node.childNodes ) {
            return null;
        }

        if ( node.childNodes.length === 0 ) {
            return null;
        }

        var v = null;
        for ( var i=0; i < node.childNodes.length; i++ ) {

            var n = node.childNodes[i];
            if ( n.nodeValue ) {
                if ( i == 0 ) {
                    v = n.nodeValue;
                } else {
                    v += n.nodeValue;
                }
            }
        }

        return v;
    };

    /**
     * Retrieve the value of the node given a the specified XPath. If no XPath is
     * specified, then returns the value of this element. If there are multiple
     * elements matching the given path, then the value of the first matching
     * element will be retrieved.
     *
     * @param {String} [xpath] An XPath to select a specified element or attribute.
     * @type String
     * @return The value of the node, if found, or null.
     */
    this.value = function(xpath) {
            if ( xpath ) {
            var node = selectSingleNode(this._doc,this._xml,xpath);
            if ( !node ) {
                return null;
            }

        return retrieveValue(node);
            } else {
        return retrieveValue(this._xml);
            }
    };

    /**
     * Retrieves the values for all elements matching the given path.
     *
     * @param {String} xpath An XPath expression to select an attribute or element.
     *
     * @type String[]
     * @return An array of strings containing the values, or null if there were no matching nodes.
     */
    this.values = function(xpath) {
            var nodes = selectNodes(this._doc,this._xml,xpath);
        if ( !nodes ) {
            return null;
            }

            var vals = new Array(nodes.length);
            for ( i=0; i < nodes.length; i++ ) {
            vals[i] = retrieveValue(nodes[i]);
            }

            return vals;
    };

    /**
     * <p>Converts the value for the given XPath (if specified) to a boolean value. A default value can be
     * supplied and used if there is no value found for the given path.</p>
     *
     * <p>The following values will be converted to <i>true</i>: [1,true,yes]</p>
     * <p>The following values will be converted to <i>false</i>: [0,false,no]</p>
     *
     * @param {String} [xpath] An XPath expression to select a sub-node. If not specified, or null, then selects this element.
     * @param {Boolean} [defaultValue] A value to use if no value is otherwise found.
     *
     * @throws {Exception} If the value cannot be converted.
     * @type Boolean
     * @return A boolean value.
     */
    this.booleanValue = function(xpath,defaultValue) {
        var v = this.value(xpath);
        if ( v === null ) {
            if ( defaultValue !== undefined ) {
                return defaultValue;
            }

            throw "Cannot find boolean value at xpath: " + xpath;
        }

        v = v.toLowerCase();
        if ( v == "true" ) {
            return true;
        }

        if ( v == "false" ) {
            return false;
        }

        if ( v == "1" ) {
            return true;
        }

        if ( v == "0" ) {
            return false;
        }

        if ( v == "yes" ) {
            return true;
        }

        if ( v == "no" ) {
            return false;
        }

        throw "Cannot convert to boolean: " + xpath + "=" + v;
    };

    /**
     * Returns a string value for the given path.
     *
     * @param {String} [xpath] An XPath expression to select a sub-node. If not specified, or null, then selects this element.
     * @param {String} [defaultValue] A value to use if no value is otherwise found.
     *
     * @throws {Exception} If no value can be found.
     * @type String
     * @return The value as a string.
     */
    this.stringValue = function(xpath,defaultValue) {
        var v = this.value(xpath);
        if ( v === null ) {
            if ( defaultValue !== undefined ) {
                return defaultValue;
            }

            throw "Cannot find string value at xpath: " + xpath;
        }

        return v;
    };

    /**
     * Returns the given xpath as an integer.
     * @param {String} [xpath] An XPath expression to select a sub-node. If not specified, or null, then selects this element.
     * @param {Integer} [defaultValue] A value to use if no value is otherwise found.
     * @param {Integer} [radix] The radix of the integer
     *
     * @throws {Exception} If the value cannot be converted.
     * @type Integer
     * @return The value as an integer.
     */
    this.intValue = function(xpath,defaultValue,radix) {
        var v = this.value(xpath);
        if ( v === null ) {
            if ( defaultValue !== undefined && defaultValue !== null ) {
                return defaultValue;
            }

            throw "Cannot find integer value at xpath: " + xpath;
        }

        if ( v == 'infinity' ) {
            return Number.POSITIVE_INFINITY;
        }

        if ( v == '-infinity' ) {
            return Number.NEGATIVE_INFINITY;
        }

        return parseInt(v,radix);
    };

   /**
     * Returns the given xpath as a long integer.
     * @param {String} [xpath] An XPath expression to select a sub-node. If not specified, or null, then selects this element.
     * @param {Integer} [defaultValue] A value to use if no value is otherwise found.
     * @param {Integer} [radix] The radix of the integer
     *
     * @throws {Exception} If the value cannot be converted.
     * @type Integer
     * @return The value as an integer.
     */
    this.longValue = function(xpath,defaultValue,radix) {
        var v = this.value(xpath);
        if ( v === null ) {
            if ( defaultValue !== undefined && defaultValue !== null ) {
                return defaultValue;
            }

            throw "Cannot find long integer value at xpath: " + xpath;
        }

        if ( v == 'infinity' ) {
            return Number.POSITIVE_INFINITY;
        }

        if ( v == '-infinity' ) {
            return Number.NEGATIVE_INFINITY;
        }

        return parseInt(v,radix);
    };

   /**
     * Returns the given xpath as a floating point number.
     * @param {String} [xpath] An XPath expression to select a sub-node. If not specified, or null, then selects this element.
     * @param {Integer} [defaultValue] A value to use if no value is otherwise found.
     *
     * @throws {Exception} If the value cannot be converted.
     * @type Float
     * @return The value as a floating point number.
     */
    this.floatValue = function(xpath,defaultValue) {
        var v = this.value(xpath);
        if ( v === null ) {
            if ( defaultValue !== undefined ) {
                return defaultValue;
            }

            throw "Cannot find float value at xpath: " + xpath;
        }

        if ( v == 'infinity' ) {
            return Number.POSITIVE_INFINITY;
        }

        if ( v == '-infinity' ) {
            return Number.NEGATIVE_INFINITY;
        }

        return parseFloat(v);
    };

    /**
     * Returns the specified path as a double
     */
    this.doubleValue = function(xpath,defaultValue) {
        var v = this.value(xpath);
        if ( v === null ) {
            if ( defaultValue !== undefined ) {
                return defaultValue;
            }

            throw "Cannot find double value at xpath: " + xpath;
        }

        if ( v == 'infinity' ) {
            return Number.POSITIVE_INFINITY;
        }

        if ( v == '-infinity' ) {
            return Number.NEGATIVE_INFINITY;
        }

        return parseFloat(v);
    };

};

/**
 * Converts the element to a string.
 */
arc.xml.XmlElement.prototype.toString = function() {
  var s = ":" + this.name();
  var attributes = this.attributes();

  if ( attributes !== null ) {
    var i;
    for ( i=0; i < attributes.length; i++ ) {
      s += " -" + attributes[i].name() + "=" + attributes[i].value();
    }
  }

  var v = this.value();
  if ( v ) {
    s += "=\"" + v + "\"";
  }

  var es = this.elements();
  if ( es !== null ) {
    s += " < ";
    for ( i=0; i < es.length; i++ ) {
      s += " " + es[i];
    }

    s += " > ";
  }

  return s;
};


BROWSER_TYPE = 0;
function detectBrowserType() {
    var ua = navigator.userAgent.toLowerCase();
    if ( ua.indexOf('msie') != -1 ) {
        BROWSER_TYPE = 1;
    }
};

detectBrowserType();


/**
 * XML (string) parser.
 */
arc.xml.XmlDoc = function() {
    return {

        /**
         * Parse an XML document in a string.
         *
         * @param txt String containing the XML document.
         * @return arc.xml.XmlElement
         */
        parse: function(txt) {

            var xmlDoc;
            try { //Internet Explorer
                if ( BROWSER_TYPE === 1 ) {
                    xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                    xmlDoc.async="false";
                    xmlDoc.loadXML(txt);
                } else {
                    var parser=new DOMParser();
                    xmlDoc=parser.parseFromString(txt,"text/xml");
                }
            } catch (e) {
                var parser=new DOMParser();
                xmlDoc=parser.parseFromString(txt,"text/xml");
            }

            return new arc.xml.XmlElement(xmlDoc,xmlDoc.documentElement);
        }

    }

}();




/**
 * <p>Remote server requests are returned by the service <i>arc.mf.client.RemoteServer.execute</i> when
 * executing a service asynchronously. This request object provides a handle that allows the request to be
 * aborted by the calling appliciation.</p>
 *
 * @class Returned when asynchronously executing services.
 * @constructor
 * @param {XmlHttpRequest} xmlHttp The underlying XMLHttpRequest object.
 *
 * @see arc.mf.client.RemoteServer.execute
 */
arc.mf.client.RemoteServerRequest = function(req) {
    this._aborted = false;
    this._req = req;

    /**
     * Aborts this request.
     */
    this.abort = function() {
        if ( this._aborted ) {
            return;
        }

        this._aborted = true;
        this._req.abort();
    };

    /**
     * Has this request been aborted?
     *
     * @type Boolean
     * @return true if aborted and false if not.
     */
    this.aborted = function() {
        return this._aborted;
    };
}


/**
 * @private
 */
arc.mf.client.RemoteAjaxRequest = function(xmlHttp) {
    this._xmlHttp = xmlHttp;

    /**
     * Aborts this request.
     */
    this.abort = function() {
        this._xmlHttp.abort();
    };

};

/**
 * @private
 */
arc.mf.client.RemoteFormRequest = function(frame) {
    this._frame = frame;

    /**
     * Aborts this request.
     */
    this.frame = function() {
    };

};

/**
 * Progress information when uploading content to the server.
 *
 * @class Progress information when uploading content to the server.
 * @constructor
 *
 * @param total The total number of bytes to be sent.
 * @param progress The total number of bytes read.
 * @param normalizedProgress The ratio of progress to the total.
 * @param eta The estimated time (of arrival) in seconds before the content is fully uploaded.
 */
arc.mf.client.Progress = function(total,progress,normalizedProgress,eta) {
    this._total = total;
    this._progress = progress;
    this._normalizedProgress = normalizedProgress;
    this._eta = eta;

    /**
     * The total number of bytes to transfer.
     *
     * @return The number of bytes to transfer
     * @type Long
     */
    this.total = function() {
        return this._total;
    };

    /**
     * The total number of bytes transferred.
     *
     * @return The number of bytes transferred.
     * @type Long
     */
    this.progress = function() {
        return this._progress;
    };

    /**
     * The ratio of progress to total.
     *
     * @return The ratio progress/total
     * @type Double
     */
    this.normalizedProgress = function() {
        return this._normalizedProgress;
    };

    /**
     * The number of seconds estimated to complete the transfer.
     *
     * @return The number of seconds to completion.
     * @type Double
     */
    this.estimatedTimeToComplete = function() {
        return this._eta;
    };

};


// =============================
// Class: arc.mf.client.LogonHandler
// =============================

 /*
arc.mf.client.LogonHandler = function(logonHandler) {
  this.logon = logonHandler;
}
*/

// =============================
// Class: arc.mf.client.ExecuteResponseHandler
// =============================

/**
 * <p>An execute response handler is a pair of functions for handling an asynchronous normal or error response
 * from the server when using the service <i>arc.mf.client.RemoteServer.execute</i>.</p>
 *
 * @class A pair of handler functions for asynchronous service execution.
 * @constructor
 * @param replyHandler A function to process the normal response. The function will be called with arguments (<i>re</i>,<i>args</i>), where <i>re</i> is the response element and <i>args</i> are arguments that may have been supplied to <i>arc.mf.client.RemoteServer.execute</i>
 * @param errorHandler A function to process an error reponse. The function will be called with the arguments (<i>ex</i>,<i>args</i>), where <i>ex</i> is the exception (of type <i>arc.mf.client.ExServer</i>) and <i>args</i> are arguments that may have been supplied to <i>arc.mf.client.RemoteServer.execute</i>
 * @param [progressHandler] A function to receive notification of upload progress (if sending data). If supplied, the function will be called with the arguments (<i>progress</i>,<i>args</i>), where <i>progress</i> is of type <i>arc.mf.client.Progress</i> and <i>args</i> are arguments that may have been supplied to <i>arc.mf.client.RemoteServer.execute</i>.
 *
 * @see arc.mf.client.RemoteServer
 * @see arc.mf.client.ExServer
 */
arc.mf.client.ExecuteResponseHandler = function(replyHandler,errorHandler,progressHandler) {
  this.replyHandler = replyHandler;
  this.errorHandler = errorHandler;
  this.progressHandler = progressHandler;
};

/**
 * <p>An output handler is used to deal with output data that is returned when executing a service.
 * A callback function is executed with an argument specifying the download URL for the data. For
 * example:
 * </p>
 * <p>callback(url)</p>
 *
 * @class A output handler for processing the output returned by a service call.
 * @param outputCallback A function that will be called with the output. Will be called with the URL to download the data.
 * @param [scope] The execution scope of the function.
 */
arc.mf.client.OutputHandler = function(outputCallback,scope) {
    this._outputCallback = outputCallback;
    this._scope = scope || this;

    /**
     * Calls the output handler with the given URL.
     *
     * @private
     */
    this.execute = function(url) {
        this._outputCallback.apply(this._scope,[ url ]);
    };
};


/**
 * <p>The input to a service execute function can be a TypedStringInput. This allows the
 * MIME type of the input to be associated with the string input data.</p>
 *
 * @class A MIME types input string to send as the input data to <i>arc.mf.client.RemoteServer.execute</i>
 * @constructor
 * @param string The input string to send as attached data to the service request.
 * @param mimeType The MIME type of the string.
 */
arc.mf.client.TypedStringInput = function(string,mimeType) {
    this._string = string;
    this._mimeType = mimeType;

    /**
     * @private
     */
    this.string = function() {
        return this._string;
    };

    /**
     * @private
     */
    this.mimeType = function() {
        return this._mimeType;
    };
};

// =============================
// Class: arc.mf.client.RemoteServer
// =============================

/**
 * <p>An interface to the server which allows a client application to logon, execute services and logoff.</p>
 * <p>In order to use the server, the calling application must first set a logon handler to prompt the user to log on when they
 * first use the system, or when their session times out. The graphical user interface for entering credentials is
 * up to each application.<p/>
 *
 * @class An interface to the server.
 */
arc.mf.client.RemoteServer = function() {

    // Members
    var _logonHandler = null;
    var _sid          = null;
    var _fid          = 1;
    var _loadedClientCSS = false;
    var _isIE = false;
    var _downloadFrame = null;
    var _seqId        = 1;

    var SECURE_IFRAME_SRC = "javascript:false";
    var FORM_ENCODING = "multipart/form-data";

    /**
     * Load some CSS that enables us to make some things hidden.
     *
     * @private
     */
    function loadClientCSS() {
        if ( _loadedClientCSS ) {
            return;
        }

        var headID = document.getElementsByTagName("head")[0];
        var cssNode = document.createElement('link');
        cssNode.type = 'text/css';
        cssNode.rel = 'stylesheet';
        cssNode.href = '/mflux/client/mfclient.css';
        cssNode.media = 'screen';
        headID.appendChild(cssNode);

        _loadedClientCSS = true;
    };

    function detectBrowserType() {
        var ua = navigator.userAgent.toLowerCase();
        if ( ua.indexOf('msie') != -1 ) {
            _isIE = true;
        }
    };

    detectBrowserType();

    /**
     * @private
     */
    function processResponse(xe,service,args,input,output,responseHandler,scope) {
        var type = xe.value("/response/reply/@type");

        // Error handling..
        if ( type == "error" ) {
            var reply = xe.element("/response/reply");

            var sversion = reply.value("server/version");

            var type     = reply.value("error");

            var expired = false;

            // Standard types..
            var exception;
            if ( type == "arc.mf.authentication.Authentication$ExFailure" ) {
                exception = new arc.mf.client.ExAuthenticationFailure();
            } else if ( type == "arc.mf.server.Session$ExSessionInvalid" ) {
                exception = new arc.mf.client.ExSessionInvalid();
                expired = true;
            } else {
                var scls     = reply.values("sclass");
                var message  = reply.value("message");
                var stack    = reply.value("stack");

                // Filter out any context, which makes the error harder to
                // read.
                var idx = message.indexOf("\nContext:\n");
                if ( idx != -1 ) {
                    message = message.substring(0,idx);
                }

                exception = new arc.mf.client.ExServer(sversion,service,type,scls,message,stack);
            }

            // Session expired? Then retry, logging on again..
            if ( expired ) {
                _sid = null;
                _seqId = 1;
                return arc.mf.client.RemoteServer.execute(service,args,input,output,responseHandler,scope);
            }

            if ( responseHandler ) {
                responseHandler.errorHandler(exception,scope);
            } else {
                throw exception;
            }

        } else {

            var re = xe.element("/response/reply/result");

            // Any outputs?
            if ( output !== undefined && output !== null ) {
                var oid = re.value("outputs/id");
                if ( oid === null ) {

                    var ex = "Expected output data, but no data generated.";
                    if ( responseHandler ) {
                        var args = scope;
                        scope = scope || this;
                        responseHandler.errorHandler.apply(scope,[ex,args]);
                    } else {
                        throw ex;
                    }
                }

                // Pass the URL for the output to the output handler..
                var url = "/mflux/output.mfjp?_skey=" + _sid + "&id=" + oid;
                output.execute(url);
            }

            // The actual response is embedded under the response element, since
            // we used 'service.execute' to execute the service on our behalf.
            var rre = re.element("response");
            if ( rre !== null ) {
                re = rre;
            }

            if ( responseHandler ) {
                var args = scope;
                scope = scope || this;
                responseHandler.replyHandler.apply(scope,[re,args]);
                return null;
            } else {
                return re;
            }
        }
    };

    /**
     * Execute the request at the server. If there is a response handler, then the
     * request will execute asynchronously.
     *
     * @private
     */
    function executeAtServer(service,args,input,output,responseHandler,scope) {
        if ( !service ) {
            throw "No service specified.";
        }

        var executing = true;
        while ( executing ) {
            // Need to logon before executing?
            if ( !_sid ) {
                if ( !_logonHandler ) {
                    var ex = new arc.mf.client.ExNoSession();
                    if ( responseHandler ) {
                        responseHandler.errorHandler(ex,scope);
                        return;
                    }

                    throw ex;
                }

                // If the request is ansynchronous, then it can execute after
                // logging on, which is an asynchronous activity..
                if ( responseHandler ) {
                    _logonHandler(function() { arc.mf.client.RemoteServer.execute(service,args,input,output,responseHandler,scope); });
                    return;
                }

                // Otherwise..logon, and indicate that the caller needs to retry..
                _logonHandler(null);

                // The logon must be synchronous..
                if ( !_sid ) {
                  throw new arc.mf.client.ExNoSession();
                }
            }

            try {
                return sendRequestToServer(_sid,_seqId++,service,args,input,output,responseHandler,scope);
            } catch ( ex ) {
                // This applies for synchronous requests..
                if ( ex.type != "arc.mf.client.ExSessionInvalid" ) {
                    throw ex;
                }

                _sid = null;
            }
        }

        // Never gets here..
        return null;
    };

    /**
     * @private
     */
    function sendRequestToServer(sid,seq,service,args,input,output,responseHandler,scope) {
        if ( input === undefined || input === null ) {
            return sendAJAXRequestToServer(sid,seq,service,args,output,responseHandler,scope);
        }

        if ( responseHandler === undefined || responseHandler === null ) {
            throw "arc.mf.client.RemoteServer.execute: If supplying input data to a service request, then the request must be asynchronous. Response handler required.";
        }

        return sendFormRequestToServer(sid,seq,service,args,input,output,responseHandler,scope);
    }

    /**
     * Creates the request packet..
     */
    function createRequestXML(sid,seq,service,args,outputs) {
        var request = "<request>";

        // Wrap the service request in the service "server.execute" - this
        // provides a common way to deal with services that do and do
        // not provide data outputs. We cannot wrap "system.logon" - which
        // won't have a sid - as the server is looking for that service
        // name for one that does not need a session id.
        if ( sid ) {
            request += '<service name="service.execute"';
            request += ' session="' + sid + '" seq="' + seq + '"';
            request += '>';
            request += '<args>';
            request += '<reply>last</reply>';
        }

        request += '<service name="' + service + '"';

        if ( outputs !== undefined && outputs !== null ) {
            request += ' outputs="1"';
        }

        request += '>';

        if ( args ) {
            if ( sid ) {
                request += args;
            } else {
                request += '<args>' + args + '</args>';
            }
        }

        request += '</service>';

        if ( outputs !== undefined && outputs !== null ) {
            request += '<outputs-via>session</outputs-via>';
        }

        if ( sid ) {
            request += '</args>';
            request += '</service>';
        }

        request += '</request>';

        return request;
    };

    /**
     * Send the request to the server using AJAX.
     *
     * @private
     */
    function sendAJAXRequestToServer(sid,seq,service,args,output,responseHandler,scope) {
        var xmlHttp = arc.mf.client.Ajax.createXmlHttpRequest();

        var async = (responseHandler !== undefined && responseHandler !== null)? true : false;

        var request = createRequestXML(sid,seq,service,args,output);

        xmlHttp.open("POST","/__mflux_svc__",async);
        xmlHttp.setRequestHeader("Content-Type","text/xml");

        var req = new arc.mf.client.RemoteServerRequest(xmlHttp);

        if ( async ) {
            xmlHttp.onreadystatechange = function() {
                if ( !req.aborted() ) {
                    if (xmlHttp.readyState == 4 ) {
                        if ( xmlHttp.status === 500 ) {
                            throw "Internal Server Error";
                        } else if ( xmlHttp.status === 200 ) {
                            if (xmlHttp.responseXML) {
                                var xdoc = xmlHttp.responseXML;
                                var xml = xdoc.documentElement;

                                processResponse(new arc.xml.XmlElement(xdoc,xml),service,args,null,output,responseHandler,scope);
                            }
                        }
                    }
                }
            }
        }

        xmlHttp.send(request);

        if ( async ) {
            return req;
        }

        // Must be synchronous..
        if ( xmlHttp.responseXML ) {

            var xdoc = xmlHttp.responseXML;
            var xml = xdoc.documentElement;

            return processResponse(new arc.xml.XmlElement(xdoc,xml),null,null,null,output,null,scope);
        }

        return null;
    };

    function checkIfInternalError(xe,service,doc) {
        // May be an error, such that not XML:
        if ( xe === null || xe.element("/response") === null ) {
            throw new arc.mf.client.ExServer(null,service,"InternalServerError",null,"An internal server error has occurred while executing the request.",doc.innerHTML);
        }
    };

    function startProgressMonitor(progressHandler,scope,seq) {
        var w = new arc.xml.XmlStringWriter();
        w.add("seq",seq);

        var adoc = w.document();

        var iid = null;
        function updateProgress() {
            var re = arc.mf.client.RemoteServer.execute("system.session.progress.describe",adoc);
            var pe = re.element("io/packet");
            if ( pe !== null ) {
                var total = pe.longValue("total");
                var progress = pe.longValue("progress");
                var np = pe.doubleValue("progress/@normalized");
                var eta = pe.longValue("eta");

                progressHandler(new arc.mf.client.Progress(total,progress,np,eta),scope);
            }
        };

        // Run twice a second..
        iid = setInterval(updateProgress,500);
        return iid;
    };

    function stopProgressMonitor(iid) {
        clearInterval(iid);
    };

    /**
     * Send the request to the server using a form POST.
     *
     * @private
     */
    function sendFormRequestToServer(sid,seq,service,args,input,output,responseHandler,scope) {

        // Dynamically load the CSS that contains the hidden class..
        // loadClientCSS();

        // Create a hidden IFRAME to receive the response.
        var formId = "mf-client-hidden-form-" + _fid;
        var frameId = "mf-client-hidden-iframe-" + _fid;

        _fid++;

        var frame = document.createElement('iframe');
        frame.id = frameId;
        frame.name = frameId;
        frame.style.width = 0;
        frame.style.height = 0;
        frame.style.visibility = 'hidden';
        frame.style.top = -10000;
        frame.style.left = -10000;

        // frame.className = 'mf-client-hidden';

        // In order to stop IE popping up a message about insecure content when
        // using HTTPS, we need to set the frame source.
        if ( _isIE ) {
            frame.src = SECURE_IFRAME_SRC;
        }

        document.body.appendChild(frame);

        // IE also requires the frame to have a name.
        if ( _isIE ){
           document.frames[frameId].name = frameId;
        }

        var form = document.createElement('form');

        form.id = formId;
        form.target = frameId;
        form.method = 'POST';
        form.enctype = FORM_ENCODING;
        form.encoding = FORM_ENCODING;
        form.action = '/__mflux_svc__';

        form.style.width = 0;
        form.style.height = 0;
        form.style.visibility = 'hidden';
        form.style.top = -10000;
        form.style.left = -10000;

        // Add the XML request to the form.
        var ri = document.createElement('input');
        ri.type = 'hidden';
        ri.name = 'request';
        ri.value = createRequestXML(sid,seq,service,args,output);
        form.appendChild(ri);

        if ( input !== undefined && input !== null ) {
            var type = typeof(input);
            if ( type == 'object' ) {
                if ( input.mimeType === undefined ) {

                    if ( input.type === undefined ) {
                        throw "Remote server inputs must be an 'input' DOM object, a 'string' or an arc.mf.client.TypedStringInput. Found " + type;
                    }

                    type = input.type;
                    if ( type == 'file' || type == 'hidden' || type == 'text' ) {
                        form.appendChild(input);
                    } else {
                        throw "Remote server inputs must be an 'input' DOM object of type [file,text,hidden]. Found " + type;
                    }

                } else {
                    var i = document.createElement('input');
                    i.type = 'hidden';
                    i.name = 'attachment-type';
                    i.value = input.mimeType();
                    form.appendChild(i);

                    i = document.createElement('input');
                    i.type = 'hidden';
                    i.name = 'attachment';
                    i.value = input.string();
                    form.appendChild(i);
                }

            } else if ( type == 'string' ) {
                var i = document.createElement('input');
                i.type = 'hidden';
                i.name = 'attachment';
                i.value = input;
                form.appendChild(i);
            } else {
                throw "Remote server inputs must be an 'input' DOM object, a 'string' or an arc.mf.client.TypedStringInput. Found " + type;
            }
        }

        document.body.appendChild(form);

        var req = new arc.mf.client.RemoteServerRequest(new arc.mf.client.RemoteFormRequest(frame));

        // Progress monitor..
        var pmid = null;

        // The following callback will process the IFRAME's response
        function responseCallback() {
            try {

                if ( pmid !== null ) {
                    stopProgressMonitor(pmid);
                }

                // The document object depends on the browser type.
                var rdoc;
                if ( _isIE ) {
                    // IE8 supports contentDocument
                    rdoc = (frame.contentDocument || frame.contentWindow.document);
                } else {
                    rdoc = (frame.contentDocument || window.frames[id].document);
                }

                var xdoc;
                var xml;
                if ( rdoc.XMLDocument ) {
                    // This is the IE path.
                    xdoc = rdoc.XMLDocument;
                    xml = xdoc; // .documentElement;
                } else {
                    // This path is followed by FireFox and Safari..
                    xdoc = rdoc;
                    xml = xdoc.documentElement;
                }

                if ( !req.aborted() ) {
                    // Its possible (e.g. FireFox) that the XPath selection is
                    // not available for a frame document..we need to explicitly
                    // add.
                    var xe = new arc.xml.XmlElement(xdoc,xml);

                    checkIfInternalError(xe,service,xml);

                    // The response is always XML..unless there is an error.
                    processResponse(xe,service,args,input,output,responseHandler,scope);
                }

            } catch ( e ) {
                responseHandler.errorHandler(e,scope);
            }

            if ( frame.addEventListener ) {
                frame.removeEventListener('load',responseCallback,false);
            } else {
                frame.detachEvent('onload',responseCallback);
            }

            removeItem(form);
            removeItem(frame);
        }

        if ( frame.addEventListener ) {
            frame.addEventListener('load',responseCallback,false);
        } else if ( frame.attachEvent ) {
            frame.attachEvent('onload',responseCallback);
        }

        if ( responseHandler.progressHandler ) {
            pmid = startProgressMonitor(responseHandler.progressHandler,scope,seq);
        }

        try {
            form.submit();
        } catch ( ex ) {
            stopProgressMonitor(pmid);
            throw ex;
        }

        return req;
    };

    function downloadData(url) {
                // Dynamically load the CSS that contains the hidden class..
        // loadClientCSS();

        // Create a hidden IFRAME to receive the response. We'll create one of
        // these only..
        var frame;
        if ( _downloadFrame === null ) {
            var frameId = "mf-client-hidden-iframe-" + _fid;

            _fid++;

            frame = document.createElement('iframe');
            frame.id = frameId;
            frame.name = frameId;
            frame.style.width = 0;
            frame.style.height = 0;
            frame.style.visibility = 'hidden';
            frame.style.top = -10000;
            frame.style.left = -10000;

            document.body.appendChild(frame);

            // IE also requires the frame to have a name.
            if ( _isIE ){
                document.frames[frameId].name = frameId;
            }

            _downloadFrame = frame;
        } else {
            frame = _downloadFrame;
        }

        frame.src = url;
    };


    /**
     * @private
     */
    function removeItem(item) {
        if ( _isIE ) {
            var d = document.createElement('div');
            d.appendChild(item);
            d.innerHTML = '';
        } else if ( item.parentNode ) {
            item.parentNode.removeChild(item);
        }
    };

    // Public interface...
    return {

        /**
         * <p>Sets a handler that is called when the user should be prompted to log into the server.
         * The user will need to logon before executing services, and if/when their current session
         * times out.</p>
         *
         * <p>The function should prompt the user, call <i>arc.mf.client.RemoteServer.logon</i> and if successful
         * call the post logon callback if one has been set.</p>
         *
         * <p>The logon handler must be set before executing any services.</p>
         *
         * @param {function(post)} lh The handler function. The function may be passed a post login function (will be non-null if set) that should be called after successful logon.
         * @see arc.mf.client.RemoteServer.logon
         */
        setLogonHandler : function(lh) {
            _logonHandler = lh;
        },

        /**
         * Is the session logged on? This does not mean the session will be valid,
         * but simply that they have logged on (and not logged off).
         */
        loggedOn: function() {
            if ( _sid === null ) {
                return false;
            }

            return true;
        },
        /**
         * Log onto the server using a token. An application must log on before
         * execute will send requests to the server.
         *
         * @param {String} token Token generated
         *
         * @throws {arc.mf.client.ExAuthenticationFailure} The supplied credentials are invalid.
         */
        logonWithToken: function(token){
            var w = new arc.xml.XmlStringWriter();
            w.add("token",null, token);

            _sid = null;

            var xe = sendRequestToServer(null,null,"system.logon",w.document());

            _sid = xe.value("session");
        },
        /**
         * Log onto the server using the triplet domain/user/password. An application must log on before
         * execute will send requests to the server.
         *
         * @param {String} domain The domain name.
         * @param {String} user The user name.
         * @param {String} password The user's password.
         *
         * @throws {arc.mf.client.ExAuthenticationFailure} The supplied credentials are invalid.
         */
        logon : function(domain,user,password) {
            var w = new arc.xml.XmlStringWriter();
            w.add("app",null,"WEB");
            w.add("domain",null,domain);
            w.add("user",null,user);
            w.add("password",null,password);

            _sid = null;

            var xe = sendRequestToServer(null,null,"system.logon",w.document());

            _sid = xe.value("session");
        },

        // Method: httpAttemptAuthentication
        httpAttemptAuthentication: function (appName) {
            var w = new arc.xml.XmlStringWriter();
            w.add("app", null, appName);

            _sid = null;

            var xe = sendRequestToServer(null, null, "http.authentication.attempt", w.document());
            var authenticated = (xe.value("authenticated") == "true");
            if (authenticated) {
                var domain = xe.value("domain");
                var user = xe.value("user");
                var session = xe.value("session");

                _sid = session;

                return {authenticated: authenticated, domain: domain, user: user, session: session}
            } else {
                return {authenticated: false}
            }
        },

        /**
         * Returns the session identifier, or null if no session.
         *
         * @return The unique session identifier for this client, or null.
         */
        sessionId: function() {
            return _sid;
        },
        setSessionId: function(session_id) {
            _sid = session_id;
        },

        /**
         * <p>Executes the specified named service at the server. The service may have arguments. A typical
         * way to create arguments is to use an <i>arc.xml.XmlStringWriter</i> and pass the output of the
         * method <i>document</i> to as the arguments.</p>
         * <p>For example:</p>
         * <div style="margin-left: 8px">
         * <p>var w = new arc.xml.XmlStringWriter();</p>
         * <p>w.add("id",null,"27");</p>
         * <p>var r = arc.mf.client.RemoteServer.execute("asset.get",w.document());
         * </div>
         * <p>Service execution may be synchronous or asynchronous. If synchronous, then execute will return an
         * <i>arc.xml.XmlElement</i> containing the result of the service. If there is an error, then an exception
         * will be thrown. If a responseHandler is supplied, then the service will execute asynchronously. The
         * response handler has two functions to handle a normal reponse or error response.</p>
         * <p>If the session has not logged on, or the current session has expired, then this method will automatically
         * call the registered logon handler.</p>
         *
         * @param {String} service The name of the service to execute.
         * @param {String} [args] The arguments, if any, for the service.
         * @param {String|Object} [input] An optional DOM input element, a string, or an arc.mf.client.TypedStringInput. If the input is an input DOM element then it must be of type [<i>file,hidden,text</i>] and it will be moved/reparented within a hidden form so no future references should be made to that input by the caller. If specified, then the request is asynchronous and <b>must</b> have a response handler.
         * @param {arc.mf.client.OutputHandler} [output] An optional output handler. If specified, the data is requested.
         * @param {arc.mf.client.ResponseHandler} [responseHandler] If the request should execute asynchronously, then supply a response handler. If no response handler then the method will execute synchronously.
         * @param {Object} [appArgs] Additional application defined arguments that will be passed to a response handler.
         *
         * @return If executing synchronously, then returns an <i>arc.xml.XmlElement</i> containing the result. If executing asynchronously, then returns an <i>arc.mf.client.RemoteServerRequest</i>
         * @type arc.xml.XmlElement | arc.mf.client.RemoteServerRequest
         *
         * @throws {arc.mf.client.ExNoSession} If not logged on and there is no logon handler.
         * @throws {arc.mf.client.ExServer} If an exception occurs while executing the service in the server.
         *
         * @see arc.mf.client.RemoteServer.setLogonHandler
         * @see arc.xml.XmlElement
         * @see arc.mf.client.RemoteServerRequest
         */
        execute : function(service,args,input,output,responseHandler,appArgs) {
            return executeAtServer(service,args,input,output,responseHandler,appArgs);
        },

        /**
         * Downloads data from the given URL. This service might be called (for example) by
         * an output handler passed to the <i>execute</i> service.
         *
         * @param {String} url A URL for data to be downloaded.
         * @param {String} [filename] An optional file name for the downloaded data. If not supplied, then a generic one will be generated by the server.
         */
        download : function(url,filename) {
            filename = filename || null;

            if ( url.indexOf('&disposition=') == -1 ) {
                url += "&disposition=attachment";
            }

            if ( filename !== null ) {
                url += "&filename=" + filename;
            }

            return downloadData(url);
        },

        /**
         * Explicitly logs off the session. Doing so is good practice, but not mandatory since any session will
         * time out in the server after a period of inactivity.
         */
        logoff : function() {
            if ( !_sid ) {
                return;
            }

            try {
                sendRequestToServer(_sid,_seqId++,"system.logoff");
            } catch ( ex ) {
                // Ignore expired sessions when logging off..
                if ( ex.type != "arc.mf.client.ExSessionInvalid" ) {
                    throw ex;
                }
            }

            _sid = null;
            _seqId = 1;
        },

        /**
         * Constructs a URL to retrieve the icon for a specified version of an asset from the server.
         *
         * @param {Integer} id The asset identifier.
         * @param {Integer} version The version of the asset.
         * @param {Integer} size The dimensions in pixels of a bounding box for the icon.
         *
         * @type String
         * @return A URL that can be used for the <i>src</i> attribute of an <i>img</i> HTML element.
         */
        iconUrl : function(id,version,size) {
            return "/mflux/icon.mfjp?_skey=" + _sid + "&id=" + id + "&version=" + version + "&size=" + size;
        },

        /**
         * Constructs a URL to retrieve the icon for a file (based on the file extension).
         *
         * @param {String} filename The name of the file.
         * @param {Integer} size The dimensions in pixels of a bounding box for the icon.
         *
         * @type String
         * @return A URL that can be used for the <i>src</i> attribute of an <i>img</i> HTML element.
         */
        iconUrlForFile : function(filename,size) {
            var idx = filename.lastIndexOf('.');
            if ( idx == -1 ) {
                return "/mflux/icon.mfjp?_skey=" + _sid + "&type=content/unknown&size=" + size;
            }

            var ext = filename.substr(idx+1);
            return "/mflux/icon.mfjp?_skey=" + _sid + "&ext=" + ext + "&size=" + size;
        },

        /**
         * Constructs a URL to retrieve the content for the specified asset and version with an <i>inline</i> disposition.
         *
         * @param {Integer} id The asset identifier.
         * @param {Integer} version The version of the asset.
         *
         * @type String
         * @return A URL that will retrieve the content for the asset.
         */
        inlineContentUrl : function(id,version) {
            return "/mflux/content.mfjp?_skey=" + _sid + "&id=" + id + "&version=" + version + "&disposition=inline";
        },

        /**
         * Constructs a URL to retrieve the content for the specified asset and version with an <i>attachment</i> disposition.
         *
         * @param {Integer} id The asset identifier.
         * @param {Integer} version The version of the asset.
         *
         * @type String
         * @return A URL that will retrieve the content for the asset.
         */
        attachmentContentUrl : function(id,version) {
            return "/mflux/content.mfjp?_skey=" + _sid + "&id=" + id + "&version=" + version + "&disposition=attachment";
            return "/mflux/content.mfjp?_skey=" + _sid + "&id=" + id + "&disposition=attachment";
        },
        /**
         * Constructs a URL to retrieve the content for the specified asset (default version) an <i>attachment</i> disposition.
         *
         * @param {Integer} version The version of the asset.
         *
         * @type String
         * @return A URL that will retrieve the content for the asset.
         */
        attachmentContentUrlDefaultVersion : function(id,version) {
            return "/mflux/content.mfjp?_skey=" + _sid + "&id=" + id + "&disposition=attachment";
        },
        /**
         ** Returns a URL to an asset image pyramid tile.
         **
         ** @param {Integer} id The asset identifier.
         ** @param {Integer} version The version of the asset.
         ** @param {Integer} zoom The zoom level.
         ** @param {Integer} x The position of the tile from the left of the image.
         ** @param {Integer} y The position of the tile from the top of the image.
         */
         imagePyramidTileUrl: function(id,version,zoom,x,y) {
            return "/mflux/iptile.mfjp?_skey=" + _sid + "&id=" + id + "&version=" + version + "&zoom=" + zoom + "&x=" + x + "&y=" + y;
         }

    }
}();


// =============================
// Class: arc.mf.client.ExServer
// =============================

/**
 * Represents an exception that occured in the server.
 *
 * @class A remote server exception.
 * @constructor
 *
 * @param {String} serverVersion The version of the remote server.
 * @param {String} type The type of the exception (as reported by the server).
 * @param {String} service The service that was being executed.
 * @param {String[]} stypes The super type exceptions (as reported by the server).
 * @param {String} message Any message associated with the exception.
 * @param {String} stack The stack trace (in the server) of the exception.
 *
 */
arc.mf.client.ExServer = function(sversion,service,type,stypes,message,stack) {
    this._serverVersion = sversion;
    this._type    = type;
    this._service = service;
    this._stypes  = stypes;
    this._message = message;
    this._stack   = stack;

    /**
     * Returns the version of the server.
     *
     * @type String
     * @return The server version.
     */
    this.serverVersion = function() {
        return this._serverVersion;
    };

   /**
    * The type of the exception within the server.
    *
    * @type String
    * @return The remote exception type.
    */
    this.type = function() {
        return this._type;
    };

    /**
     * Returns the service that was being executed.
     *
     * @type String
     * @return The name of the service.
     */
    this.service = function() {
        return this._service;
    };

    /**
     * If the exception had super types, then returns the names of those super-types.
     *
     * @type String[]
     * @return The super-type names, or null if the exception has no super-types.
     */
    this.superTypes = function() {
        return this._stypes;
    };

    /**
     * The message associated with the exception, if any.
     *
     * @type String
     * @return The message, if any, or null if no message.
     */
    this.message = function() {
        return this._message;
    };

    /**
     * Returns the stack trace (generated in the server) for the exception.
     *
     * @type String
     * @return The stack trace for the exception.
     */
    this.stack = function() {
        return this._stack;
    };
};

/**
 * Returns a string representation of the exception.
 */
arc.mf.client.ExServer.prototype.toString = function() {
  return this.type() + ": " + this.message();
}


// =============================
// Class: arc.mf.client.ExNoSession
// =============================

/**
 * Generated when there is no specified logon handler and the client has not logged onto
 * the server, or the existing session has expired.
 *
 * @class No session can be established.
 */
arc.mf.client.ExNoSession = function() {
    this._type = "arc.mf.client.ExNoSession";

    /**
     * Returns the type of the exception.
     *
     * @type String
     * @return arc.mf.client.ExNoSession
     */
    this.type = function() {
        return this._type;
    };
};


/**
 * Converts the exception to a string.
 */
arc.mf.client.ExNoSession.prototype.toString = function() {
  return this.type() + ": Not logged into the server.";
}

// =============================
// Class: arc.mf.client.ExSessionInvalid
// =============================

/**
 * Session is invalid and caller needs to logon again.
 *
 * @class The session is invalid and caller needs to logon again.
 */
arc.mf.client.ExSessionInvalid = function() {
    this._type = "arc.mf.client.ExSessionInvalid";

    /**
     * Returns the type of the exception.
     *
     * @type String
     * @return arc.mf.client.ExSessionInvalid
     */
    this.type = function() {
        return this._type;
    };
}

/**
 * Converts the exception to a string.
 */
arc.mf.client.ExSessionInvalid.prototype.toString = function() {
    return this.type() + ": session is invalid.";
};


/**
 * Logon credentials are invalid.
 *
 * @class Generated when calling <i>arc.mf.client.RemoteServer.logon</i> and the credentials are invalid.
 *
 * @see arc.mf.client.RemoteServer.logon
 */
arc.mf.client.ExAuthenticationFailure = function() {
    this._type = "arc.mf.client.ExAuthenticationFailure";

    /**
     * Returns the type of the exception.
     *
     * @type String
     * @return arc.mf.client.ExAuthenticationFailure
     */
    this.type = function() {
        return this._type;
    };
};

/**
 * Converts the exception to a string.
 */
arc.mf.client.ExAuthenticationFailure.prototype.toString = function() {
  return this.type() + ": Invalid user credentials.";
};


// =============================
// Class: arc.mf.Namespace
// =============================

arc.mf.Namespace = { };

// Method: Namespace.create
arc.mf.Namespace.create = function() {
  for ( var i=0; i < arguments.length; i++ ) {
    var nps = arguments[i].split(".");
    var rp = nps[0];
    var o;
    eval('if (typeof ' + rp + ' == "undefined"){' + rp + ' = {};} o = ' + rp + ';');
    for (j=1; j<nps.length; ++j) {
      o[nps[j]]=o[nps[j]] || {};
      o=o[nps[j]];
    }
  }
}

// =============================
// Class: arc.mf.LibraryLoader
// =============================

arc.mf.LibraryLoader = function() {
    var _libs = [];

    return {

        /**
         * Loads a library.
         *
         * @param path The URL path to the library
         * @param library The name of the library (.js will be appended)
         * @param {Function} responseHandler Optional function to be notified. If set, then will be asynchronous.
         * @param {Object} scope Optional scope for response handler.
         */
        load: function(path,library,responseHandler,scope) {
            if ( _libs[library] ) {

                if ( responseHandler ) {
                    responseHandler.apply(scope,[ null ]);
                }

                return;
            }

            var src;
            if ( path ) {
                src = path + "/" + library + ".js";
            } else {
                src = library + ".js";
            }

            try {
                arc.mf.client.Ajax.loadAndEvaluateScript(src,function(ex){
                    if ( ex ) {
                        this.responseHandler.apply(this.scope,[ex]);
                        return;
                    }

                    // The script might register the library - if not, then
                    // add an entry..
                    if ( !this.libs[this.library] ) {
                        this.libs[this.library] = { };
                    }

                    this.responseHandler.apply(this.scope, [ null ]);

                },{ responseHandler: responseHandler, scope: scope, libs: _libs, library: library });
            } catch ( ex ) {
                if ( _libs[library] ) {
                    _libs[library] = null;
                }

                throw "Error loading: " + src + ": " + ex.toString();
            }

        },

        /**
         * Loads a set of libraries.
         *
         * @param {Object} path The base path for the library.
         * @param {Array} libraries The library names to load.
         * @param {Object} responseHandler
         * @param {Object} scope
         */
        loadAll: function(path,libraries,responseHandler,scope) {
            var fn = function(ex) {
                if ( ex ) {
                    this.responseHandler.apply(this.scope,[ ex ]);
                    return;
                }

                // At the end?
                if ( this.idx == this.libraries.length ) {
                    this.responseHandler.apply(this.scope,[ null ]);
                    return;
                }

                // Go to the next one?
                arc.mf.LibraryLoader.load(this.path,this.libraries[this.idx++],this.fn,this);
            };

            var ctx = { path: path, responseHandler: responseHandler, scope: scope, libraries: libraries, idx: 1, fn: fn };

            // Load the first library, and when loaded, that will cause the load
            // of the next.
            arc.mf.LibraryLoader.load(path,libraries[0],fn,ctx);
        },

        /**
         * Registers the given library.
         *
         * @param name The name of the library.
         * @param lib The library configuration.
         */
        register: function(name,lib) {
            if ( _libs[name] ) {
                throw "Library '" + name + "' already registered";
            }

            _libs[name] = { library: lib };
        },


        /**
         * Returns the list of libraries that have been loaded. Will return an
         * array of library configurations.
         */
        loaded: function() {
            var loaded = new Array(_libs.length);
            for ( var i=0; i < _libs.length; i++ ) {
                loaded[i] = _libs[i];
            }

            return loaded;
        }
    }
}();


// =============================
// Class: LibraryWrapper
// =============================

arc.mf.LibraryWrapper = function(cfg) {
  _cfg = cfg;

  // Libray might require other libraries.
  if ( cfg.require ) {
    var rs = cfg.require;
    for ( var i=0; i < rs.length; i++ ) {
      var r = rs[i];

      arc.mf.LibraryLoader.load(r.path,r.name,function() {

      });
    }
  }

  return {
    version: function() {
      return _cfg.version;
    },

    require: function() {
      return _cfg.require;
    }
  };

};

// =============================
// Class: Library
// =============================

arc.mf.Library = function() {
  return {
    register: function(name,cfg) {
        arc.mf.LibraryLoader.register(name,new arc.mf.LibraryWrapper(cfg));
        }
  };
}();


// =============================
// Class: PluginManager
// =============================

/**
 * @class The plugin manager is a singleton that loads plugin components.
 */
arc.mf.PluginManager = function() {
    var _loaded = false;
    var _plugins = [];
    var _namespace = null;
    var _baseUrl = null;

    /**
     * Loads the plugins.
     *
     * @private
     */
    function loadPlugins(responseHandler,scope) {

        var w = new arc.xml.XmlStringWriter();
        w.add("view","www-public");
        w.add("namespace",null,_namespace + "/registry");
        w.add("assets",null,"true");
        w.add("action",null,"get-name");

        var xe =  arc.mf.client.RemoteServer.execute("asset.namespace.list",w.document());
        var pis = xe.values("namespace/asset");
        if ( pis ) {
            var libs = [];

            for ( var i=0; i < pis.length; i++ ) {
                var name = pis[i];
                var idx = name.lastIndexOf(".js");
                if ( idx != -1 ) {
                    var library = name.substring(0,idx);
                    libs[libs.length] = library;
                }
            }

            arc.mf.LibraryLoader.loadAll(_baseUrl + "/registry",libs,function(ex) {
                if (!ex) {
                    _loaded = true;
                }

                this.responseHandler.apply(this.scope,[ _plugins, ex ]);
            }, { responseHandler: responseHandler, scope: scope, pis: pis });
        }

    }

    return {

        /**
         * The base namespace on the server which contains the registered plugins.
         */
        setNamespace: function(ns) {
            _namespace = ns;
        },

        /**
         * Sets the base URL for retrieving plugins from the server.
         */
        setUrl: function(baseUrl) {
            _baseUrl = baseUrl;
        },

        /**
         * Returns the base URL for retrieving plugins.
         */
        url: function() {
            return _baseUrl;
        },

        /**
         * Called by a plugin script on evaluation to register a handler object
         * for the plugin. The form of the handler (object/class) is application
         * defined.
         */
        register: function(handler) {
            _plugins.push(handler);
        },

        /**
         * Returns the available plugins.
         */
        plugins: function(responseHandler,scope) {
            if ( !_loaded ) {
                loadPlugins(responseHandler,scope);
                return;
            }

            responseHandler.apply(scope,[ _plugins, null ]);
        }
    };

}();


// Class: EventManager
//
//  An event manager is used to add listeners for events and to
//  fire events as required.
//
arc.mf.EventManager = function() {
    this._handlers = null;

    // Method: addListener
    this.addListener = function(name,handler,scope) {
        if ( this._handlers === null ) {
            this._handlers = [];
        }

        var hs = this._handlers[name];
        if ( hs === undefined ) {
            hs = [];
            this._handlers[name] = hs;
        }

        if ( scope === undefined ) {
            scope = null;
        }

        hs[hs.length] = { handler: handler, scope: scope };
    };

    // Method: fireEvent
    this.fireEvent = function(name,event) {
        if ( this._handers === null ) {
            return;
        }

        var hs = this._handlers[name];
        if ( hs === undefined ) {
            return;
        }

        for ( var i=0; i < hs.length; i++ ) {
            var h = hs[i];
            h.handler(event,h.scope);
        }
    };
};
