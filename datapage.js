// js middleware for pawsey data front end. Requires the mediaflux mf_client to be loaded.

var _global_domain = "";
var current_version = 1.2;

var _global_login_check_timer = 0;
var _global_staging_check_timer = 0;

// global holders
var move_file_array = [];
var move_file_descriptor_array = [];

var _global_upload_list = [];
var _global_upload_size = 0;
var _global_uploaded_size = 0;
var _global_upload_start_time = 0;
var _global_download_size = 0;
var _global_upload_monitor = [];

var _global_current_download_size = 0;
var _global_download_list= [];

var file_type_obj = {
    audio: ["audio/mpeg", "audio/mp4"],
    compressed: ["application/x-zip", "application/x-tar"],
    excel: ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel"],
    flash: ["application/x-shockwave-flash"],
    html: ["text/html"],
    image: ["image/png", "image/jpeg"],
    javascript: ["application/x-javascript"],
    pdf: ["application/pdf"],
    powerpoint: ["application/vnd.openxmlformats-officedocument.presentationml.presentation", "application/vnd.ms-powerpoint"],
    text: ["text/plain"],
    video: ["video/quicktime", "video/mp4"],
    word: ["application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"],
    xml: ["text/xml"],
    unknown: ["content/unknown"]
};

// check layout routines
function datapage_check_layout(){
    //set the size of the file table based on the height of a full screen view
    var offset = 170;

    if(!$("#system-alert-container").hasClass("hidden")){
            offset = offset + $("#system-alert-container").outerHeight(true);
    }

    if(!($("#task-view-container").hasClass("hidden"))){
        offset = offset + $("#task-view-container").outerHeight(true);
    }

    $("#div-file-table").height($("#fullpage").outerHeight(true)-offset);
}


// init routines

function initialise_data_browser(){
    // Sets the various actions we need for this tool.  This must be run once
    // in the page where the code is included.
    $.ajaxSetup ({
        // Disable caching of AJAX responses
        cache: false
    });

    // check for something going very wrong. If so, we warn the user
    check_for_version_consistency();

    // set the default domain
    $("#ivec_domain_selected").addClass("active");
    $("#nav-userlogin-button").click(function() {show_login();});
    // $("#nav-userlogin-control").hide();

    $("#public-access-button").click(function() {show_public_access();});

    // login start
    $("#process-login-button").addClass("disabled");
    $("#process-login-button").click(function() {checkLogin();});
    $("#public-login-button").click(function(){public_logon();});

    $("#inputUsername").change(function() {validateInputs();});
    $("#inputPassword").change(function() {validateInputs();});
    $("#inputUsername").keydown(function(event) {check_login_return_hit(event);});
    $("#inputPassword").keydown(function(event) {check_login_return_hit(event);});

    $("#group-selection").click(function() {toggle_group_selection();});

    // init action buttons
    $("#search-clear-button").click(function(){
        clear_search_field();
        // show_hide_refine_search(false);
        show_hide_bootstrap_alert_message(false, null);
    });
    $("#search-clear-button").hide();
    $("#search-execute-button").click(function(){
        simple_text_search($("#input-search-field").val(), $("#file-table").attr("data-current-page-size"),1);
    });

    // populate file type drop down list
    for (var key in file_type_obj){
        if (file_type_obj.hasOwnProperty(key)) {
            $("#dropdown_filetype").append('<option value="'+ key +'"> '+ key +' </option>');
        }
    }


    $("#input-search-field").keydown(function(event) {validate_search_text(event);});

    $("#task-underway-view").hide();
    $("#file-upload-view").hide();

    // whole page drop method

    //disabling dragstart on body prevents dragging html elements.
    $(document.body).on('dragstart', function(){return false;});

    // $(document.body).on('dragover',function(){return false;});

    $(document.body).on('dragenter dragover',function(event){
        event.preventDefault();
        event.stopPropagation();
        handle_drag_start(event);
    });

    $(document.body).on('drop',function(event) {
        event.preventDefault();
        event.stopPropagation();
        var valid_session = check_for_valid_session();
        // only accept drops if the user is logged in.
        if (valid_session){
            handle_drop_event(event);
        }
    });

    $(document.body).on('dragleave dragexit',function(event){
        if( event.originalEvent.pageX <= 0 || event.originalEvent.pageX >= $(window).width() ||
            event.originalEvent.pageY <= 0 ||  event.originalEvent.pageY >= $(window).height()){
            handle_drag_end(event);
        }
    });

//  timeout login
    $("#overlay-login-button").click(function(){login_after_timeout();});
    $("#overlay-public-login-button").click(function(){public_login_after_timeout();});
    $("#reset-timeout-login-view").click(function(){reset_login_after_error();});

//  tasks UI
    $("#visible-upload-button").click(function(e){
        e.stopPropagation();
        $("#hidden-upload-input").trigger("click");
    });
    $("#show-display-uploads").click(function(){toggle_task_view();});
    $("#show-display-downloads").click(function(){toggle_task_view();});

//  public accessible interface
    $("#login-from-public-button").click(function(){show_login();});
    $("#public-clear-delete-button").click(function(){clear_all_selected_items();});
    $("#public-download-files-button").click(function(){download_selected_assets();});
    $("#public-download-archive-button").click(function(){download_archived_assets();});


//  everything else
    $("#dismiss-system-alert").click(function(){dismiss_system_alert();});
    $("#parent-namespace-button").click(function(){open_parent_namespace();});
    $("#move-items-interface").hide();
    $("#move-items-button").click(function(){toggle_move_items_interface();});
    $("#cancel-move-button").click(function(e){cancel_current_move(e);});
    $("#move-here-button").click(function(){activate_move_selected_files();});
    $("#show-moving-items-button").click(function(e){show_move_help(e);});


    $("#share-files-button").click(function(){share_selected_assets();});
    $("#unshare-files-button").click(function(){unshare_selected_assets();});
    $("#download-files-button").click(function(){download_selected_assets();});
    $("#download-archive-button").click(function(){download_archived_assets();});


    $("#clear-delete-button").hide();
    $("#clear-delete-button").click(function(){clear_all_selected_items();});

    $("#export-text-list-button").click(function(){get_filelist_for_current_state();});

//  breadcrumb control
    $("#refresh-filelist-button").click(function(e) {
        $("#refresh-filelist-button a i").addClass("fa-spin");
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        var current_path = $("#file-table").attr("data-current-namespace");
        var current_page =  parseInt($("#file-table").attr("data-current-page"),10);
        complex_query_namespace(current_path, current_page_size, current_page);
    });


//  file actions
    $("#selected-items-button").click(function(){toggle_selected_items_menu();});
    $("#clear-selection-button").click(function(){clear_all_selected_items();});
    $("#select-all-files-button").click(function(){select_all_files();});
    $("#select-all-folders-button").click(function(){select_all_folders();});

    $("#dismiss-tasks-button").click(function(){toggle_task_view();});
    $("#toggle-upload-log-view").click(function(){toggle_progress_view_type();});
    $("#dismiss-upload-view").click(function(){hide_upload_progress_interface();});
    $("#upload-bottom-tab-button").click(function(){toggle_upload_progress_view_visibility();});
    $("#download-bottom-tab-button").click(function(){toggle_download_progress_view_visibility();});
    $("#clear-current-upload-button").click(function(){clear_upload_interface();});

    // upload/download control
    $("#reset-current-upload-button").click(function(event){reset_upload_interface(event);});
    $("#reset-current-download-button").click(function(event){reset_download_interface(event);});
    $("#cancel-current-upload-button").click(function(event){cancel_current_upload_dummy(event);});
    $("#hidden-upload-input").change(function(e){files_manually_selected_for_upload(e);});

    $("#dismiss-upload-overlay").click(function(){dismiss_upload_overlay();});
    $("#upload-dialogue-click").click(function(){$("#upload-dialogue-input").click();});
    $("#upload-dialogue-input").change(function(e){
        console.log('upload-dialogue-input', e);
    });

    $("#generate-download-list").click(function(){
        check_for_list_type();
    });

    $("#transfer-mode-button").click(function(){toggle_transfer_mode();});
    $("#remote-data-view").hide();
    // $("#div-refine-search").hide();
    $("#btnModal_Confirm").on("click", function(){delete_selected_items();});

    $("#myDeleteModal").on('show.bs.modal', function (e) {
        $("#btnModal_Confirm").show();
        $("#btnModal_Cancel").html('Cancel');
        var filenames_selected = get_selected_file_or_directory_names();
        $("#tblModalText tbody").empty();
        $.each(filenames_selected, function(index, filename){
            $("#tblModalText tbody").append('<tr><td>'+ filename +'</td><td></td></tr>');
        });
    });

    $("#public-file-toggle-button").click(function(){toggle_detail_public_button();});

    $("#show-new-namespace-button").popover({
        placement: 'left',
        html:'true',
        content:function() {
            return $('#new-namespace-popover-content').html();
        }
    }).parent().delegate("#new-namespace-input","keypress", function(e){
        validate_input_namespace(e);
    }).parent().delegate("#new-namespace-button", "click", function() {
        make_new_namespace();
    }).parent().delegate("#cancel-namespace-button","click", function(){
        $("#show-new-namespace-button").popover("hide");
    }).on('shown.bs.popover', function() {
        $("#new-namespace-input").focus();
    });

    //initial setup file view pager
    setup_file_pager_controls_browse();

    //set livearc logon handler
    arc.mf.client.RemoteServer.setLogonHandler(function (e){authent_timeout_handler(e);});

//  main file table init

//  check for the page requested.  If the request is a path, we try to get that path, dealing
//  with authent as needed.  If nothing specific is requested we try to initiate a session
//  based on a previous seesion ID (see check_for_valid_session())

    var valid_session = false;
    var init_session_is_done = false;

    var urlstring = window.location.href;
    var path = null;


    // is there a login token?
    var login_token = get_parameter_by_name('token').replace(/\s/g,"");

    if (login_token.hasOwnProperty(length)){

        logon_with_token(login_token);
        path = "/projects";
        init_interface_for_path(path);

        $( "#main-tabs a[href='#tab-data']" ).tab('show');

        init_session_is_done = true;
    }

    // check for public access
    if (!init_session_is_done){
        if (window.location.pathname.indexOf("/public") === 0) {

            path = get_parameter_by_name('path');

            if (_global_domain === "ivec"){
                path = "/projects" + path;
            }

            show_public_access(path);

            init_session_is_done = true;
            $( "#main-tabs a[href='#tab-data']" ).tab('show');
        }
    }

    // check for normal authenticated access
    if (!init_session_is_done){
        if (window.location.pathname.indexOf("/projects") === 0) {
            valid_session = check_for_valid_session();

            path = get_parameter_by_name('path');

            if (valid_session){
                                initialize_interface_after_login();
                                check_for_system_message();
                init_interface_for_path(path);
            } else {
                show_project_access(path);
            }
            init_session_is_done = true;
            $( "#main-tabs a[href='#tab-data']" ).tab('show');
        }
    }

    // no login requested somehow, time to fall back on something
    if (!init_session_is_done){
        valid_session = check_for_valid_session();

        if(valid_session) {
            check_for_system_message();
                initialize_interface_after_login();
            if (_global_domain === "ivec"){
                init_interface_for_path("/projects");
            } else {
                init_interface_for_path("/");
            }
            $( "#main-tabs a[href='#tab-data']" ).tab('show');
            datapage_check_layout();
        }
    }


    if(valid_session){
        $("#bottom-upload-tab").removeClass("hidden");
    }

    if (urlstring.indexOf("/tools/") > -1) {
        $( "#main-tabs a[href='#tab-tools']" ).tab('show');
    }

    if (urlstring.indexOf("/news/") > -1) {
        $( "#main-tabs a[href='#tab-news']" ).tab('show');
    }

    if (urlstring.indexOf("/help/") > -1) {
        $( "#main-tabs a[href='#tab-help']" ).tab('show');
    }

    datapage_check_layout();
}

// file upload and DnD

function update_upload_progress_meter(percentage){
    if (percentage < 100){
        if (!$("#upload-indicator-text i").hasClass*"fa-spin"){
            $("#upload-indicator-text").html('<i class="fa fa-refresh fa-spin"></i>');
        }
    }
    var percentage_label = "p"+percentage;
    $("#upload-indicator").removeClass().addClass("c100"+" "+percentage_label+" small");
}


function toggle_progress_view_type() {
    if($("#upload-progress-log-view").hasClass("hidden")){
        datapage_check_layout();
        $("#upload-progress-log-view").removeClass("hidden");
    } else {
        $("#upload-progress-log-view").addClass("hidden");
    }
}


function toggle_download_progress_view_visibility() {

    $("#upload-progress-view").addClass('hidden');
    $("upload-control").removeClass("faded");
    $("download-control").removeClass("faded");

    if ($("#download-progress-view").hasClass('hidden')){
        $("#show-current-downloadlog-button").addClass("active");
        $("#show-current-uploadlog-button").removeClass("active");
        $("#download-progress-view").removeClass('hidden');
        $("#file-display").addClass("hidden");
        $("#download-display-caret").addClass("fa fa-caret-down");
        $("upload-control").addClass("faded");
    } else {
        $("#show-current-downloadlog-button").removeClass("active");
        $("#download-progress-view").addClass('hidden');
        $("#file-display").removeClass("hidden");
        $("#download-display-caret").addClass("fa fa-caret-right");
    }
}

function toggle_upload_progress_view_visibility() {
    $("#download-progress-view").addClass('hidden');
    $("upload-control").removeClass("faded");
    $("download-control").removeClass("faded");

    if ($("#upload-progress-view").hasClass('hidden')){
        $("#show-current-uploadlog-button").addClass("active");
        $("#show-current-downloadlog-button").removeClass("active");
        $("#upload-progress-view").removeClass('hidden');
        $("#file-display").addClass("hidden");
        $("download-control").addClass("faded");
    } else {
        $("#upload-progress-view").addClass('hidden');
        $("#show-current-uploadlog-button").removeClass("active");
        $("#file-display").removeClass("hidden");
    }
}

function handle_drag_over(event){

}


function handle_drag_start(event) {
    var table_height = $("#file-table").height();
    $("#file-upload-view").css({height:table_height});
    $("#file-upload-view").show();
    var current_path = get_current_path();
    $("#upload-directory-target").text(current_path);
}


function handle_drag_end(event) {
    dismiss_upload_overlay();
}

function dismiss_upload_overlay() {
    $("#file-upload-view").hide();
}

var parse_starts = 0;
var parse_ends = 0;

function clear_upload_interface(){
    //clear upload UI elements to default state
    $("#upload-count-badge").html("idle");
    $("#upload-count-badge").removeClass("upload-badge-error");
    $("#upload-count-badge").removeClass("upload-badge-done");

    $("#upload-message-holder").html('<div class="alert alert-warning" role="alert">No uploads underway.</div>');
    $("#file-count-progress").empty();

    $("#table-upload-log tbody").empty();
    $("#table-download-log tbody").empty();

    $("#current-file-eta-text").text("---");
    $("#urrent-file-eta-units").text("seconds");

    $("#clear-current-upload-button").addClass("hidden");
}


function parse_directory(directory_entry){
    var directory_reader = directory_entry.createReader();
    var files = [];
    var new_files = directory_reader.readEntries(function(entries) {
        if (entries.length) {
            for (var i =0 ; i < entries.length; i++){
                if (entries[i].isFile){
                    var full_path = entries[i].fullPath;
                    parse_starts = parse_starts + 1;

                    entries[i].file(function(file){
                        _global_upload_list.push({file:file, full_path:full_path});
                        parse_ends = parse_ends + 1;
                        check_all_parse_completed();
                    });

                } else if (entries[i].isDirectory) {
                    parse_starts = parse_starts + 1;
                    parse_directory(entries[i]);
                }
            }
        }
        parse_ends = parse_ends + 1;
        check_all_parse_completed();
    });
}


function check_all_parse_completed() {
    // parse_starts is incremented everytime a directory OR file access is started.  Each
    // one starts a async call in a recursively called function.  We want to know when
    // all these aycn calls are done, so we check this function at the end of each async call back

    if (parse_starts === parse_ends && parse_starts > 0){
        livearc_upload_filelist_start();
    }
}


function handle_directory_parsable_list(items){

    var length = items.length;
    var local_file_list = [];

    for (var i = 0; i < length; i++) {
        var entry = items[i].webkitGetAsEntry();
        var command_args = new arc.xml.XmlStringWriter();
        command_args.add("name",entry.name);

        if (entry.isFile) {

            var full_path = entry.fullPath;
            parse_starts = parse_starts + 1;
            entry.file(function(file){
                 _global_upload_list.push({file:file, full_path:full_path});
                parse_ends = parse_ends + 1;
                check_all_parse_completed();
            });
        } else if (entry.isDirectory) {
            parse_starts = parse_starts + 1;
            parse_directory(entry);
        }
    }
    check_all_parse_completed();
}


function handle_drop_event(event) {

    // page-footer is on the main html (index.html)
    // show_upload_progress_interface();
    reset_upload_interface(null);

    _global_upload_list = [];
    _global_upload_size = 0;
    _global_uploaded_size = 0;

    if (event.originalEvent.dataTransfer.items){
        // ie: chrome
        handle_directory_parsable_list(event.originalEvent.dataTransfer.items);

    } else {
        // effectively 'not chrome'
        var length = event.originalEvent.dataTransfer.files.length;

        // sync upload via async methods
        for (var i = 0; i < length; i++) {
            var full_path = '';
            _global_upload_list.push({file:event.originalEvent.dataTransfer.files[i], full_path:full_path, uploaded:0, done:false});
        }

        livearc_upload_filelist_start();
    }

    // upload is all done
    $("#file-upload-view").hide();
    event.preventDefault();
}


function check_for_system_message() {

    // check for system error message.

    var system_message_asset = "path=/www/system-alert";

    $("#system-alert-container").addClass("hidden");

    var args_01 = new arc.xml.XmlStringWriter();
    args_01.add("id",system_message_asset);

    var command = "";
    try {
        command = livearc_execute_handler("asset.exists", args_01, true);
    } catch (e){
        command = "";
    }

    var command_result = "";
    try {
        command_result = xmlToJson(command._xml);
    } catch(e) {
        console.log(e);
    }

    if (command_result.hasOwnProperty("exists")){
        if (command_result.exists["#text"] === "true"){
            var args = new arc.xml.XmlStringWriter();
            args.add("id",system_message_asset);
            args.add("label","PUBLISHED");

            try {
                command = livearc_execute_handler("asset.label.exists", args, true);
            } catch (e){
                command = "";
            }

            var exists_command = xmlToJson(command._xml);

            if (exists_command.exists){
                if (exists_command.exists["#text"] === "true"){
                    var args_02 = new arc.xml.XmlStringWriter();
                    args_02.add("id",system_message_asset);
                    try {
                        command = livearc_execute_handler("asset.get", args_02, true);
                    } catch (e){
                        command = "";
                    }

                    if (command === ""){
                        $("#system-alert").html("");
                    } else {
                        var result = xmlToJson(command._xml);
                        $("#system-alert").html("<strong>Attention:</strong> "+result.asset.meta["mf-note"].note["#text"]);
                        $("#system-alert-container").removeClass("hidden");
                    }
                }
            }
        }
    }

    datapage_check_layout();
}

function dismiss_system_alert(){
    console.log('dismiss_system_alert');
    $("#system-alert-container").addClass("hidden");
    datapage_check_layout();
}


function toggle_task_view(){
    $( "#main-tabs a[href='#tab-data']" ).tab('show');

    if($("#task-view-container").hasClass("hidden")){
    // $("#task-miniview-container").addClass("hidden");
    $("#task-view-container").removeClass("hidden");
    } else {
        // $("#task-miniview-container").removeClass("hidden");
        $("#task-view-container").addClass("hidden");
    }

    if(!$("#upload-progress-view").hasClass("hidden")){
        $("#upload-progress-view").addClass("hidden");
    }
    if(!$("#download-progress-view").hasClass("hidden")){
        $("#download-progress-view").addClass("hidden");
    }
    $("#file-display").removeClass("hidden");
    datapage_check_layout();
}


function check_for_version_consistency() {
    $.getJSON( "/current-version.json", function( data ) {
        if(current_version < data.current_version){
            $("#system-alert").append("<div class='row'><div class='center alert alert-danger'><p><strong>Code is out of date</strong> I expected to see version "+data.current_version+" but found "+current_version+". Please force-reload your browser and get the current version of this web tool.</p></div></div>");
        }
    });
}


function get_parameter_by_name(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function show_project_access(path_to_open){
    $( "#main-tabs a[href='#tab-data']" ).tab('show');
    $("#login-panel").append("<p class='hidden' id='hidden_path_to_open'>"+path_to_open+"</p>");
}

function show_public_access(path){
    public_logon(path);
    $( "#main-tabs a[href='#tab-data']" ).tab('show');
}


function get_selected_file_or_directory_names(){
    var selected_rows = index_selected_assets();
    var names = [];

    $.each(selected_rows, function(index, row){
        if($(row).hasClass("folder-row")){
            names.push($(row).data('pathinfo'));
        } else if($(row).hasClass("file-row")){
            names.push($(row).data('filename'));
        }
    });
    return names;

}

// login activities
function check_login_timer_function(){
    //recheck login

    var valid_session = test_for_valid_session();
    if(!valid_session){
        $("#login-panel-overlay").removeClass("hidden");
    }
}

function check_login_function(){
    //recheck login

    var valid_session = test_for_valid_session();
    if(!valid_session){
        $("#login-panel-overlay").removeClass("hidden");
    }
}

function logon_with_token(application_token){

    var command  = arc.mf.client.RemoteServer.logonWithToken(application_token);

    hide_login();

    $("#nav-login-control").removeClass("hidden");
    $("#nav-login-control").show();
    $("#nav-login-name").html("<strong>token user</strong>");
    $("#nav-logout-button").click(function() {livearc_logout();});

    // $("#task-miniview-container").removeClass("hidden");

    $(".public-interface").addClass("hidden");
    $("#selected-items-box").removeClass("hidden");
    $("#general-controls-box").removeClass("hidden");
    // $("#user-tasks-box").removeClass("hidden");

    $("#bottom-tab-container").removeClass("hidden");

    var sess_id = arc.mf.client.RemoteServer.sessionId();
    document.cookie="sessionid="+sess_id;
}

function livearc_logout() {
    // logout, cleanup and show the login window
    $( "#main-tabs a[href='#tab-data']" ).tab('show');

    var command = arc.mf.client.RemoteServer.logoff();
    show_login();

    //stop login check timer
    //window.clearInterval(_global_login_check_timer)
}

function quiet_public_logon() {
    var result = arc.mf.client.RemoteServer.logon('public','public','public');
    return true;
}

function quiet_public_logoff() {
    arc.mf.client.RemoteServer.logoff();
}

function quiet_logoff() {
    $( "#main-tabs a[href='#tab-data']" ).tab('show');
    arc.mf.client.RemoteServer.logoff();
}

function public_logon(local_path) {

    try {
        arc.mf.client.RemoteServer.logon('public','public','public');
        _global_domain = 'public';
    } catch (e) {
        $("#login-error").show();
        $("#login-nav-control").hide();
    }

    hide_login();

    $(".public-interface").removeClass("hidden");
    $("#selected-items-box").addClass("hidden");
    $("#general-controls-box").addClass("hidden");
    // $("#user-tasks-box").addClass("hidden");
        // no upload for you
        $("#show-display-uploads").addClass("hidden");
        $("#upload-control").addClass("hidden");
        $("#upload-info-control").addClass("hidden");


    if (local_path) {
        init_interface_for_path(local_path);
    } else {
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        complex_query_namespace("/", current_page_size, 1);
    }

    check_for_system_message();
}


function toggle_group_selection() {
    if($("#group-selection").hasClass("ivec-user")){
        $("#group-selection").removeClass("ivec-user").addClass("system-user");
        $("#group-selection").html("I'm an admin user");
    } else {
        $("#group-selection").removeClass("system-user").addClass("ivec-user");
        $("#group-selection").html("I'm a Pawsey user");
    }
}


function test_for_valid_session() {
    var status = false;

    var cookie = get_cookie("sessionid");

    if (cookie !== null && cookie.length > 1){
        arc.mf.client.RemoteServer.setSessionId(cookie);

        var command = livearc_execute_handler("system.session.self.describe", null);

        if (command === "arc.mf.client.ExNoSession" || command === "arc.mf.server.Services$ExServiceError" || command === "error" || command === null){
            //user needs to login, or there's something wrong with the server
            status = false;
        } else {
            // we got a user description
            status = true;
        }
    }

    return status;
}


function check_for_valid_session() {

    // See if we have a session cookie and if so, test it and update the UI accordingly.
    var status = false;

    var cookie = get_cookie("sessionid");

    if (cookie !== null && cookie.length > 1){
        arc.mf.client.RemoteServer.setSessionId(cookie);

        var command = livearc_execute_handler("system.session.self.describe", null);

        if (command === "arc.mf.client.ExNoSession" || command === "arc.mf.server.Services$ExServiceError" || command === "error" || command === null){
            //user needs to login, or there's something wrong with the server
            $("#output").addClass("alert alert-danger animated fadeInUp").html("Your session is expired. Please login.");
        } else {
            // we got a user description when asked. The user is logged in.
            // Govern yourselves accordingly.

            // note this can fail so we check that command actually has an xml package
            var user_data = null;
            if(command.hasOwnProperty("_xml")){
                user_data = xmlToJson(command._xml);
            }

            if (user_data === null) {
                status = false;
            } else {
                status = true;
            }
        }
    }
    return status;
}


function initialize_interface_after_login(){

    var command = livearc_execute_handler("system.session.self.describe", null);

    if(command.hasOwnProperty("_xml")){
        var user_data = xmlToJson(command._xml);

        var domain = user_data.session.domain["#text"];
        _global_domain = domain;
        var username = user_data.session.user["#text"];

        if (user_data.session.actor['@attributes'].type == "identity"){
            username = "token user";
        }

        hide_login();

        $("#nav-login-control").removeClass("hidden");
        $("#nav-login-control").show();
        $("#nav-login-name").html("<strong>"+username+"</strong>");
        $("#nav-logout-button").click(function() {livearc_logout();});

        $(".public-interface").addClass("hidden");
        $("#selected-items-box").removeClass("hidden");
        $("#general-controls-box").removeClass("hidden");

        $("#show-display-uploads").removeClass("hidden");
        $("#upload-control").removeClass("hidden");
        $("#upload-info-control").removeClass("hidden");
        $("#bottom-tab-container").removeClass("hidden");
    }
}


function mediaflux_authenticate(username, password){
    var domain = "ivec";
    var login_status = true;

    try {
        arc.mf.client.RemoteServer.logon(domain,username,password);
    } catch (e) {
        login_status = false;
    }

    return login_status;
}

function public_login_after_timeout(){
    quiet_public_logoff(); //just in case
    var login_status = quiet_public_logon();

    if (login_status){
        $("#timeout-public-login-message").addClass("hidden");
        $("#timeout-public-login-success").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function() {
                $("#login-public-panel-overlay").addClass("hidden");
                refresh_display_after_timeout();
            });
        $("#timeout-public-login-success").removeClass("hidden");
    }
}


function login_after_timeout(){
    $("#timeout-control-view").addClass("hidden");
    $("#timeout-login-message").removeClass("hidden");

    var login_status = mediaflux_authenticate($("#input-username-overlay").val(), $("#input-password-overlay").val());
    if (login_status){
        $("#timeout-login-message").addClass("hidden");
        $("#timeout-login-success").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
            function() {
                $("#login-panel-overlay").addClass("hidden");
                refresh_display_after_timeout();
            });
        $("#timeout-login-success").removeClass("hidden");
    } else {
        $("#timeout-login-error").removeClass("hidden");
    }
}


function reset_login_after_error(){
    $("#timeout-login-error").addClass("hidden");
    $("#timeout-control-view").removeClass("hidden");

}


function refresh_display_after_timeout(){

}


function livearc_logon(username, password, path_to_open) {
    // livearc logon routine.  Does an initial login and load the UI with the
    // root directory.

    var domain = "ivec";

    if ($("#group-selection").hasClass("system-user")){
        domain = "system";
    }

    _global_domain = domain;
    var login_status = true;

    try {
        arc.mf.client.RemoteServer.logon(domain,username,password);
    } catch (e) {
        if(domain === "system"){
            $("#output").addClass("alert alert-danger animated fadeInUp").html("Login failed, are you sure you're an admin user?");
        } else {
            $("#output").addClass("alert alert-danger animated fadeInUp").html("Login failed, please check your password");
        }
        $("#login-nav-control").hide();

        login_status = false;
    }

    // 1. login worked : update UI
    if (login_status){
        initialize_interface_after_login();
    }
    // 2. either way: don't need this anymore
    password = null;

    // 3. save the current session ID for later usage.
    if (login_status) {
        var sess_id = arc.mf.client.RemoteServer.sessionId();
        document.cookie="sessionid="+sess_id;

        check_for_system_message();

        $("#bottom-tab-container").removeClass("hidden");
        init_interface_for_path(path_to_open);

         _global_login_check_timer = setInterval(function () {check_login_timer_function();}, 600000);

        return true;
    }

    return false;
}


function init_interface_for_path(path_to_open){

    path_to_open = path_to_open.trim();
    var current_page_size = 10;

    var path_resolved = false;

    if(!path_to_open){path_to_open="";}

    if(path_to_open != "") {
        // is the path a namespace?
        var command_args = new arc.xml.XmlStringWriter();
        command_args.add("namespace",path_to_open);
        var command_result = livearc_execute_handler("asset.namespace.exists", command_args, true);

        command_result = parse_command_xml(command_result);

        if (command_result.exists === "true"){
            //simple_query_namespace(null,path_to_open);
            current_page_size = 10; // default
            complex_query_namespace(path_to_open, current_page_size, 1);
            //query_namespace(path_to_open, "");
            path_resolved = true;
        }

        // is the the path an asset?
        if (!path_resolved) {
            var command_args2 = new arc.xml.XmlStringWriter();
            command_args2.add("id","name="+path_to_open);

            var command_result2 = livearc_execute_handler("asset.exists", command_args2, true);

            //var result2 = xmlToJson(command_result2._xml);
            command_result2 = parse_command_xml(command_result2);

            if(command_result2.exists === "true"){
                query_asset(path_to_open);
                path_resolved = true;
            }
        }

        // None of the above? time to post an error
        if(!path_resolved){
            show_path_not_found(path_to_open,'File not found');
        }
    }

    // get root list
    if (!path_resolved && path_to_open === ""){
        var current_path = "/";

        var w2 = new arc.xml.XmlStringWriter();
        w2.add("assets","true");

        if (_global_domain === "ivec"){
            current_path = current_path + "projects";
            w2.add("namespace",current_path);
        }

        current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        complex_query_namespace(current_path, current_page_size, 1);
    }

}


function check_login_return_hit(e){
    if(e.keyCode == 13){
        checkLogin();
    }
}


function checkLogin() {
    var path_to_open = $("#hidden_path_to_open").text();
    var loginIsSuccessful = livearc_logon($("#inputUsername").val(), $("#inputPassword").val(), path_to_open.replace(/^\s+|\s+$/g,''));

    if(loginIsSuccessful) {
        hide_login();
    } else {
        toggleLoginError();
    }
}


function show_login() {
    $("#nav-login-control").hide();
    $("#login-panel").removeClass("hidden");
    $("#file-display").hide();
        $("#task-view-container").addClass("hidden");
    $("#bottom-tab-container").addClass("hidden");
}


function hide_login() {
    $( "#login-panel").addClass("hidden");
    $( "#file-display").show();
}


function toggleLoginError() {
    $( "#login-error").show("fast");
}


function validateInputs() {
    $( "#login-error").hide();

    if ($("#inputUsername").val().length > 0 && $("#inputPassword").val().length > 0){
        $("#process-login-button").removeClass("disabled");
        $("#inputPassword").focus();
    } else {
        $("#process-login-button").addClass("disabled");
    }
}


function livearc_logon_done(data) {
    $(data).find("name").each( function(person_name) {
        console.log(person_name);
    });
}


function authent_timeout_handler(e) {
    // This gets called when the livearc logon token times out
    // only show it, if the normal logon view is hidden.

    if (_global_domain === "public"){
        //dont show the login timout view for the public user

        if ($("#login-panel").hasClass("hidden")){
            $("#timeout-login-error").addClass("hidden");
            $("#timeout-login-success").addClass("hidden");
            $("#timeout-control-view").removeClass("hidden");
            $("#timeout-login-message").addClass("hidden");
            $("#login-public-panel-overlay").removeClass("hidden");
        }
    } else {

        if ($("#login-panel").hasClass("hidden")){
            $("#timeout-login-error").addClass("hidden");
            $("#timeout-login-success").addClass("hidden");
            $("#timeout-control-view").removeClass("hidden");
            $("#timeout-login-message").addClass("hidden");
            $("#login-panel-overlay").removeClass("hidden");
        }
    }
}


function localResponseHandler(responseData) {
    console.log(responseData);
}


// main ui action
function update_status () {

    var account_name = 'jperson';

    $("#account_name").empty();
    $("#account_name").append(account_name);

    $("#update_date").empty();
    $("#update_date").append("Last updated: <br>"+Date());

    // dummy data
    var project_list = [{project_name:'ABCD',project_usage:98.3, project_allocation:'150Tb'},{project_name:'DEFG',project_usage:43.8,project_allocation:'10 Tb'}];
    var resource_list = [{resource_name:'Pawsey Storage', resource_type:'Storage', resource_status:0},{resource_name:'RDSI Storage', resource_type:'Storage', resource_status:1}];

    // populate resource/server list
    $("#resource-list").empty();
    jQuery.each(resource_list,function(i,resource){
        if (resource.resource_status === 0){
            $("#resource-list").append('<li class="list-group-item list-group-item-success"><span>'+resource.resource_name+' : NORMAL</span></li>');
        } else {
            $("#resource-list").append('<li class="list-group-item list-group-item-danger"><span>'+resource.resource_name+' : NORMAL</span></li>');
        }
    });

    // populate project storage info
    $("#project-status-table").empty();
    jQuery.each(project_list,function(i,project){
        if (project.project_usage > 90){
            $("#project-status-table").append('<tr><td><h5>'+project.project_name+'</h5></td><td><div class="progress"><div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="'+project.project_usage+'" aria-valuemin="0" aria-valuemax="100" style="width: '+project.project_usage+'%;">'+project.project_usage+' Used</div></div></td><td><h5>'+project.project_allocation +'</h5></td></tr>');
        } else if(project.project_usage > 80){
            $("#project-status-table").append('<tr><td><h5>'+project.project_name+'</h5></td><td><div class="progress"><div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="'+project.project_usage+'" aria-valuemin="0" aria-valuemax="100" style="width: '+project.project_usage+'%;">'+project.project_usage+' Used</div></div></td><td><h5>'+project.project_allocation+'</h5></td></tr>');
        } else {
            $("#project-status-table").append('<tr><td><h5>'+project.project_name+'</h5></td><td><div class="progress"><div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="'+project.project_usage+'" aria-valuemin="0" aria-valuemax="100" style="width: '+project.project_usage+'%;">'+project.project_usage+' Used</div></td><td><h5>'+project.project_allocation+'</div>'+project.project_allocation+'</h5></td></tr>');
        }
    });
}

function setup_help_tabs() {
    $('#help-container').load('https://portal.ivec.org/docs/Data_Services/Data_Portal #content');
}

function load_tabs() {
    $( "#content-tab-intro" ).load( "intro.html" );
    $( "#content-tab-tools" ).load( "tools.html" );
    $( "#content-tab-landing" ).load( "home.html" );
    $( "#content-tab-data" ).load( "data.html" );
    $( "#content-tab-news" ).load( "news.html" );
    $( "#content-tab-documentation" ).load( "documentation.html" );
    $( "#content-tab-help" ).load( "need-help.html" ,function(){
        setup_help_tabs();
    });
    $( "#content-tab-account" ).load( "account.html" );

    $('a[data-toggle="tab"]').on('show.bs.tab', function() {clear_active_tab();});
    $('a[href="#tab-account"]').on('shown.bs.tab', function() {check_current_user();});
}

function clear_active_tab() {
    $("nav li").removeClass("active");
}

function fullscreen_view() {

    var docElm = document.getElementById("livearc_frame");

    if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
    }
    else if (docElm.msRequestFullscreen) {
        docElm.msRequestFullscreen();
    }
    else if (docElm.mozRequestFullScreen) {
        docElm.mozRequestFullScreen();
    }
    else if (docElm.webkitRequestFullScreen) {
        docElm.webkitRequestFullScreen();
    }
    else {
        console.log('nope');
    }
}


// simple search text and control
function clear_search_field() {
    $("#input-search-field").val("");
    validate_search_text();

    var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
    complex_query_namespace($("#file-table").attr("data-current-namespace"), current_page_size, 1);
    $("#file-table").attr("data-current-search","");
    $("#search-recursive-button").prop("checked",false);
}

function simple_text_search(search_text, page_size, page) {

    $("#file-table").attr("data-current-search",search_text);

    function populate_search_callback(response){
        var search_contents = xmlToJson(response._xml);
        $("#file-table").attr("data-current-page-size",parseInt(search_contents.parent.size["#text"]));
        update_pager_controls(parseInt(search_contents.parent.page["#text"]), parseInt(search_contents.parent.assets["#text"]), parseInt(search_contents.parent.last["#text"]), parseInt(search_contents.parent.size["#text"]));
        populate_file_search_json(search_contents.asset,search_text);
        setup_file_pager_controls_search();
    }

    function populate_search_callback_error(response){
        console.log('populate_search_callback_error');
    }

    $("#file-table").attr("data-current-page",page);
    var search_namespace = $("#file-table").attr("data-current-namespace");
    var search_recursive = false;

    //recursive?
    if ($("#search-recursive-button").prop("checked")){
        search_recursive = true;
    }

    if (search_text.length > 0) {
        // sanitise search string?

        var args = new arc.xml.XmlStringWriter();
        args.add("filter",search_text);
        args.add("namespace", search_namespace);
        args.add("recurse", search_recursive);
        args.add("size", page_size);
        args.add("page", page);

        livearc_async_generic_handler("www.list", args, populate_search_callback, populate_search_callback_error);
    } else {
        clear_search_field();
    }
}

function validate_search_text(event) {
    if ($("#input-search-field").val().length > 0 ){
        $("#search-clear-button").show();
        $("#search-recursive-button").removeClass("disabled");
        $("#search-execute-button").removeClass("disabled");
    } else {
        $("#search-clear-button").hide();
        $("#search-execute-button").addClass("disabled");
        $("#search-recursive-button").addClass("disabled");
    }

    // if the key is return, do the search
    if(event) {
        if (event.keyCode === 13){
            simple_text_search($("#input-search-field").val(), $("#file-table").attr("data-current-page-size"), 1);
        }
    }
}

// livearc interaction

function  files_manually_selected_for_upload(event){
    var length = event.currentTarget.files.length;
    _global_upload_list = [];

    // sync upload via async methods
    for (var i = 0; i < length; i++) {
        var full_path = '';
        _global_upload_list.push({file:event.currentTarget.files[i], full_path:full_path, uploaded:0, done:false});
    }

    livearc_upload_filelist_start();
}


function upload_progress_handler(response, args){
    console.log('upload_progress_handler',response);
}


function upload_response_handler(response,args){
    console.log('upload_response_handler',response);

}


function upload_error_handler(response,args){
    console.log('upload_error_handler',response.message());
}

// set interval timer
var _global_timer_id = 0;
var _global_first_progress_measure = 0;
var _global_first_progress_time = 0;
var _global_initial_eta = 0;

function time_remaining_estimate(){

    var time_remaining = 0;
    var current_upload_size = 0;
    var upload_size = _global_upload_list.length;
    _global_current_upload_size = upload_size;

    update_current_task_total();

    for (var i=0; i < upload_size; i++){
        current_upload_size = current_upload_size + _global_upload_list[i].file.size;
    }

    if (upload_size === _global_upload_size){
        time_remaining = "---";
    } else {
        var completion = (_global_upload_size-current_upload_size)/_global_upload_size;

        var rate = (_global_upload_size-current_upload_size)/(jQuery.now() - _global_upload_start_time)*1000;

        time_remaining = current_upload_size/rate;

        if (completion < 0.04){
            //wait until at least 4% is uploaded before we report ETA
            time_remaining = -1;
        }
    }
    return time_remaining;
}

function update_visible_upload_eta(time){
    var timetext;
    //time in seconds.

    if (time > 24*60*60){
        timetext = Math.round(time/(24*60*60))+" days";
    } else if (time > 60*60 ){
        timetext = Math.round(time/(60*60)) + "hours";
    } else if (time > 60){
        timetext = Math.round(time/60) + " mins";
    } else if (time > 0){
        timetext = Math.round(time) + " secs";
    } else {
        timetext = '---';
    }

    return timetext;
}

function reset_upload_interface(e){
    if(e){e.stopPropagation();}
    $("#upload-indicator-text").html('idle');
    $("#upload-indicator").removeClass().addClass("c100 p00 small");
    $("#table-upload-log tbody").empty();
    $("#upload-task-count").html('0');
}


function reset_download_interface(e){
    if(e){e.stopPropagation();}
    $("#download-indicator-text").html('idle');
    $("#download-indicator").removeClass().addClass("c100 p00 small");
    $("#table-download-log tbody").empty();
    $("#download-task-count").html('0');
}


function update_current_upload_info(sequence) {

    var args = new arc.xml.XmlStringWriter();
    args.add("seq",sequence);
    var total_file_count = $("#file-count-progress").data('total');

    $("#upload-task-count").text(total_file_count);

    function progress_update_callback(response) {
        var packet_info = response.element("io/packet");
        if ( packet_info !== null ) {
            var total = packet_info.longValue("total");
            var progress = packet_info.longValue("progress");
            var normalised_progress = packet_info.doubleValue("progress/@normalized")*100;
            var eta = packet_info.longValue("eta");

            var elapsed_time  = (jQuery.now() - _global_upload_start_time)/1000;

            if (_global_first_progress_measure === 0) {
                _global_first_progress_measure = 1;
                _global_initial_eta = eta;
            }

            var net_time_remaining = 0;

            if (_global_first_progress_time === 0){
                net_time_remaining = eta/(total* (1 - normalised_progress/100)) * _global_upload_size;
            } else {
                net_time_remaining = _global_first_progress_time - _global_initial_eta + eta;
            }

            var percentage = 100 - Math.round((_global_upload_size - (_global_uploaded_size + progress))/_global_upload_size*100);

            //update_upload_progress_meter(percentage);
            var time_remaining_text = update_visible_upload_eta(net_time_remaining);

        }
    }

    function progress_error_callback(response) {
        //eek
    }
    livearc_async_generic_handler("system.session.progress.describe", args, progress_update_callback, progress_error_callback);
}


function abort_current_upload_info_timer() { // to be called when you want to stop the timer
    clearInterval(_global_timer_id);
}


function check_upload_viability(){
    // can the current upload really go ahead?  False should result in a process halt and useful error
    var check_upload = {};
    check_upload.status = true;
    check_upload.message = "Nothing to report";

    // do we have permission?
    var current_path = get_current_path();
    var argument = new arc.xml.XmlStringWriter();
    argument.add("namespace",current_path);

    var namespace_check = livearc_execute_handler("asset.namespace.describe", argument);
    namespace_check = xmlToJson(namespace_check._xml);

    if (namespace_check.namespace.access.create["#text"] === "false" || namespace_check.namespace.access["create-assets"]["#text"] === "false"){
        //user does not have permission
        check_upload.status = false;
        check_upload.message = "You don't have permission to add files and/or folders to the location: "+current_path;
    }

// SDF - cap refresh => had to move asset stores around => this check is broken
// SDF - shouldn't be an issue as mediaflux will report a "quota exceeded" error anyway
 
//    var argument2 = new arc.xml.XmlStringWriter();
//    argument2.add("namespace",current_path);

//    var project_check = livearc_execute_handler("asset.namespace.store.name", argument);
//    project_check = xmlToJson(project_check._xml);
//    var project_name = project_check.store["#text"];

    // does the account have enough free space?
//    if (check_upload.status === true){

//        var argument3 = new arc.xml.XmlStringWriter();
//        argument3.add("name",project_name);

//        var store_check = livearc_execute_handler("project.describe", argument3);
//            store_check = xmlToJson(store_check._xml);

//            var quota =  store_check.store.arg["#text"];
//            var used = store_check.store.mount.size["#text"];
//            var available = quota - used;

//            if (_global_upload_size > available){
                // Not enough space on project
//                var uploadsize_text = _global_upload_size/(1000*1000)+"MB";
//                var available_text = available/(1000*1000)+"MB";

//                check_upload.status = false;
//                check_upload.message = "You don't have enough free space in your project. You need " + uploadsize_text + ", but you have "+available_text+".  If this seems incorrect, please get in touch.";
//            }
//    }
    return check_upload;
}

function livearc_upload_filelist_start() {
        $("#upload-indicator-text").html('<i class="fa fa-refresh fa-spin"></i>');
        $("#upload-indicator").removeClass().addClass("c100 p00 small");

    var sequence = 0;
    var upload_size = _global_upload_list.length;

    $("#upload-task-count").html(upload_size);

    _global_upload_size = 0;
    _global_current_upload_size = 0;
    _global_upload_start_time = 0;

    for (var i=0; i < upload_size; i++){
        _global_upload_size = _global_upload_size + _global_upload_list[i].file.size;
    }

    var uploadcheck = check_upload_viability();

    if(uploadcheck.status){
        _global_upload_start_time = jQuery.now(); //window.performance.now();

        $("#upload-indicator").attr("data-total-count",upload_size);
            $("#upload-indicator").attr("data-done-count",0);

        $("#table-upload-log tbody").empty();

        var current_count = sequence + 1;

        for (var i=0; i < upload_size; i++){
            var local_file = _global_upload_list[i];
            $("#table-upload-log tbody").append("<tr class='progress-log-name-style'><td>"+local_file.file.name+"</td><td>---</td><td class='progress-status-item'>waiting...</td></tr>");
        }

        var file = _global_upload_list[0];
        if (file !== undefined){
            livearc_upload_status_processing(file.file.name, sequence);
            livearc_upload_handler(file, sequence);
        } else {
            // how did we get here? 0 files to try and upload?
        }
    } else {
        // show error
        display_error_dialog("I can't process that upload.",uploadcheck.message,null);
    }
}

function livearc_upload_status_processing(filename, sequence) {

    //update UI for upload start and start timer
    _global_timer_id = setInterval(function() {update_current_upload_info(sequence);}, 2000);

    var table_row = $('#table-upload-log tbody tr:contains("'+filename+'")');
    table_row.find("td:last").html("<i class='fa fa-refresh fa-spin'></i> uploading...");

    //insert row in current file table view
    $("#file-table tbody").prepend('<tr class="file-row" data-filetype="incoming"><td class="icon-column"><i class="fa fa-repeat fa-spin" aria-hidden="true"></i></td><td class="name-column"><p class="file-name-input-field">'+filename+'</p></td><td class="size-column hidden-xs">---</td><td class="hidden-xs hidden-sm time-column">---</td><td class="migrate-column"><span class="dmf-indicator hidden">new</span></td><td class="public-share-indicator control-column"></td><td class="highlight-cell control-column"></td><td></td></tr>');
}

function livearc_upload_status_complete(filename, sequence, status, detailed_message) {

    for (var i = 0; i < _global_upload_list.length; i++){
        if (_global_upload_list[i].file.name === filename){
            _global_upload_list[i].done = true;
            _global_uploaded_size = _global_uploaded_size + _global_upload_list[i].file.size;
        }
    }

    //update UI for upload done and quit timer
    abort_current_upload_info_timer();

    var table_row = $('#table-upload-log tbody tr:contains("'+filename+'")');

    if (status != "success"){
        table_row.addClass("bad-upload");
        table_row.after("<tr class='message'><td class='detailed-error-style' colspan='3'>" + detailed_message + "</td></tr>");
    }

    table_row.find("td:last").text(status);
    table_row.find("td:nth-child(2)").text(new Date().toUTCString());

    //update progress bar
    var actual_index = sequence+1;

    var total_file_count = $("#upload-indicator").attr("data-total-count");

    var percentage = actual_index/total_file_count * 100;
}


function livearc_upload_filelist_next(sequence) {

    sequence = sequence + 1;
    var time_remaining = time_remaining_estimate();

    update_visible_upload_eta(time_remaining);

    _global_first_progress_measure = 0;
    _global_first_progress_time = time_remaining_estimate();

    var length = _global_upload_list.length;
    var file = _global_upload_list[0];
    var alldone = true;

    for (var i = 0; i < length; i++){
        file = _global_upload_list[i];
        if (!file.done){
            alldone = false;
            break;
        }
    }

    if(alldone){
            file= undefined;
    }

    if (file !== undefined){
        livearc_upload_status_processing(file.file.name, sequence);
        livearc_upload_handler(file, sequence);
    } else {
        //file upload is all done. Or so we sincerely hope.
        update_upload_progress_meter(100);

        $("#upload-indicator-text").text("done");

        var bad_count = $('#table-upload-log tbody tr.bad-upload').length;

        var file_count = $('#table-upload-log tbody tr').length - $('#table-upload-log tbody tr.message').length ;

        $("#upload-message-holder").empty();
        if (bad_count > 0){
            $("#upload-count-badge").addClass("upload-badge-error");
            $("#upload-count-badge").text(bad_count+" errors");
            $("#upload-message-holder").html('<div class="alert alert-danger" role="alert"><strong>Finished with '+bad_count+' failed uploads (of '+file_count+')</strong>  Please note you need Google Chrome to upload directories/folders</div>');
        } else {
            $("#upload-count-badge").text(file_count+" done");
            $("#upload-count-badge").addClass("upload-badge-done");
            $("#upload-message-holder").html('<div class="alert alert-success" role="alert"><strong>All Done</strong>. '+file_count+' files uploaded.</div>');
        }

        $("#clear-current-upload-button").removeClass("hidden");

    }
}


function livearc_upload_file_list(file_list){
    var plain_file_list = [];

    var file_upload_worker = new Worker('/file_upload_worker.js');

    file_upload_worker.addEventListener('message', function(e) {
        //is typeof string and contains success
        if ( typeof e.data === 'string'){
            //throw alert for directory/ies
            if (e.data != "done"){
               file_upload_worker.postMessage("next");
            }
        }
    }, false);

        var current_path = get_current_path();
    var session_id = arc.mf.client.RemoteServer.sessionId();

    // the file_upload_worker needs a prepared js array to send.
    for (var i = 0; i < file_list.length; i++){
        plain_file_list.push({file:file_list[i],path:current_path,session_id:session_id});
    }

    file_upload_worker.postMessage(plain_file_list);
}


function cancel_current_upload_dummy(e){
    console.log('stand in cancel_current_upload');
    e.stopPropagation();
}

function livearc_upload_handler(file_object, sequence){
    // This function actually does the upload AJAX
    var request = '';

    function cancel_current_upload(e){
        e.stopPropagation();
        request.abort();
        abort_current_upload_info_timer();
        update_interface_for_cancel();
    }

    $("#cancel-current-upload-button").unbind();
    $("#cancel-current-upload-button").click(function(event){cancel_current_upload(event);});

    var current_path = get_current_path();
    var create_path = '';
    var command_result = '';
    var optional_args = '';

    var session_id = arc.mf.client.RemoteServer.sessionId();

    var post_url_head = '/__mflux_svc__';

    var command_xml = '';
    var sanitised_name = '';

    if (file_object.full_path === ''){
        sanitised_name = sanitise_string_for_xml(file_object.file.name);
        command_xml = '<request><service name="service.execute" session="'+ session_id +'" seq="'+sequence+'"><args><reply>last</reply><service name="asset.create"><name>'+sanitised_name+'</name><namespace>'+current_path+'</namespace></service></args></service></request>';
    } else {
        var split_path = file_object.full_path.split('/');
        create_path = get_current_path() + "/" + split_path.slice(0,split_path.length-1).join("/");
        sanitised_name = sanitise_string_for_xml(file_object.file.name);
        command_xml = '<request><service name="service.execute" session="'+ session_id +'" seq="'+sequence+'"><args><reply>last</reply><service name="asset.create"><name>'+sanitised_name+'</name><namespace create="true">'+create_path+'</namespace></service></args></service></request>';
    }

    var formData = new FormData();
    formData.append('request',command_xml);
    formData.append('nb-data-attachments','1');
    formData.append(file_object.file.name, file_object.file);

    request = $.ajax({
        xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                    var percentage = 100 - Math.round((_global_upload_size - (_global_uploaded_size + evt.loaded))/_global_upload_size*100);
                    update_upload_progress_meter(percentage);
                }
            }, false);
        // fallback
        xhr.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
                var percentage = 100 - Math.round((_global_upload_size - (_global_uploaded_size + evt.loaded))/_global_upload_size*100);
                update_upload_progress_meter(percentage);
            }
        }, false);

        return xhr;
    },
    url: post_url_head,
    type: "POST",
    data: formData,
    cache: false,
    timeout: 10000000000,
    contentType: false,
    processData: false
    }).done( function(data){
        // succeeded (placeholder)
    }).fail( function(XMLHttpRequest, text_status, error) {
        // mediaflux error state IS NOT set here after mediaflux upload error (placeholder)
        // this is just for AJAX errors.  a mediaflux error is ajax success.  #zen
    }).always( function( xhr, text_status, error ) {
        // mediaflux error state IS set here after livearc upload error
        // look for type = error on response @attributes
        // log result and update UI

        var response_data = "";
        var detailed_message = "";

        var data_is_xml = true;

        try {
            response_data = xmlToJson(xhr);
        } catch(xml_error){
            data_is_xml = false;
        }

        if (data_is_xml){
            if (response_data.hasOwnProperty('response')){
                if (response_data.response.reply["@attributes"].type === "error"){
                    text_status = "failed";
                    detailed_message = response_data.response.reply.message["#text"];
                    display_error_dialog("Sorry, your upload could not be processed.", detailed_message, "");
                }
            }

            // update UI and file table
            livearc_upload_status_complete(file_object.file.name, sequence, text_status,detailed_message);

            var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);

            complex_query_namespace(current_path, current_page_size, 1);

            // do the next upload
            livearc_upload_filelist_next(sequence);
        } else {
            // non xml response. something abnornal occured.
            detailed_message = "Something very unexpected has happened.  The technical error message that follows might help us figure this out:  ";
            var extra_detail = "Nothing, sadly.";
            if (xhr.hasOwnProperty('statusText')){
                extra_detail = xhr.statusText;
            }
            detailed_message = detailed_message + extra_detail;
            display_error_dialog("Sorry, your upload could not be processed.", detailed_message, "");
        }
    });

    return command_result;
}

function update_interface_for_cancel(){
    // user has cancelled upload. Currently active upload is noted as a user cancelled item and highlighted
    $("#upload-indicator-text i").removeClass("fa-spin");
    $.each($('#table-upload-log tbody tr'), function(index, item){
        if($(item).find("td:last i").hasClass("fa-spin")){
            $(item).find("td:last").html('<i class="fa fa-exclamation-triangle"></i> user cancelled');
            $(item).find("td:nth-child(2)").text(new Date().toUTCString());
            $(item).addClass('bad-upload');
        }

        if($(item).find("td:last").text() === "waiting"){
            $(item).find("td:last").text("user cancelled");
        }
    });
}


function livearc_async_execute_with_output_handler(command, command_args, local_output_handler){
    var command_result = '';

    try {
        command_result = arc.mf.client.RemoteServer.execute(command, command_args.document(), null, new arc.mf.client.OutputHandler(local_output_handler) ,null, null);
    } catch(e) {
        $('#alert_error_human_text').text('The server reported a problem. See details below');
        $("#alert_error_detail_text").text('Command: '+command+'<br>Response: '+e.message());

        var email_text = "mailto:help@pawsey.org.au?&subject=Data%20Website%20Error&body="+e.message();
        $("#error-email-link").attr('href',email_text);
        $('#myAlertMessageModal').modal('show');
    }
    return command_result;
}

function livearc_execute_with_output_handler(command, command_args){
    var command_result = '';

    try {
        command_result = arc.mf.client.RemoteServer.execute(command, command_args.document(), null, new arc.mf.client.OutputHandler(output_handler) ,null, null);
    } catch(e) {
        $('#alert_error_human_text').text('The server reported a problem. See details below');
        $("#alert_error_detail_text").text('Command: '+command+'<br>Response: '+e.message());

        var email_text = "mailto:help@pawsey.org.au?&subject=Data%20Website%20Error&body="+e.message();
        $("#error-email-link").attr('href',email_text);
        $('#myAlertMessageModal').modal('show');
    }
    return command_result;
}

function livearc_execute_handler(command, command_args, suppress_error) {
    // command_args is of the type arc.xml.XmlStringWriter();
    var command_result = '';

    try {
        if(command_args){
            command_result = arc.mf.client.RemoteServer.execute(command, command_args.document());
        } else {
            command_result = arc.mf.client.RemoteServer.execute(command, null);
        }
    } catch(e1) {
        try {
            // try again
            if(command_args){
                command_result = arc.mf.client.RemoteServer.execute(command, command_args.document());
            } else {
                command_result = arc.mf.client.RemoteServer.execute(command, null);
            }
        } catch(e2){
            if (suppress_error){
                // let's just move on like nothing happened
                command_result = "error";
            } else if(typeof e2.message === 'function') {
                $('#alert_error_human_text').text('The server reported a problem. See details below');
                $("#alert_error_detail_text").text('Command: '+command+'<br>Response: '+e2.message());

                var email_text = "mailto:help@pawsey.org.au?&subject=Data%20Website%20Error&body="+e2.message();
                $("#error-email-link").attr('href',email_text);
                $('#myAlertMessageModal').modal('show');
                command_result = e2._type;
            }
        }
    }
    return command_result;
}

function livearc_async_generic_handler(command, command_args, response_handler, error_handler){
      // command_args is of the type arc.xml.XmlStringWriter();
    var command_result = '';
    var optional_args = null;
    var output;

    command_result = arc.mf.client.RemoteServer.execute(command, command_args.document(), null, null ,new arc.mf.client.ExecuteResponseHandler(response_handler, error_handler), optional_args);

    return command_result;
}


function output_handler(output_response){
    $("#detail-preview").html("<img width='200px' src='"+output_response+"'>");
}


function show_hide_bootstrap_alert_message(isShow, text){
    if(isShow){
        $("#div-data-alert-message").show();
        $("#data-alert-message-text").text(text);
    } else {
        $("#div-data-alert-message").hide();
        $("#data-alert-message-text").text("");
    }
}

function livearc_generic_async_execute_handler(command, command_args, response_handler, error_handler){
    // command_args is of the type arc.xml.XmlStringWriter();
    var command_result = '';
    var optional_args = null;

    command_result = arc.mf.client.RemoteServer.execute(command, command_args.document(), null, null,new arc.mf.client.ExecuteResponseHandler(response_handler,error_handler), optional_args);

    return command_result;
}

// file browser functions

function get_current_path() {
    var path = $("#path-breadcrumbs li").eq(-1).data("path");
    if (path === ""){path = "/";}
    return path;
}

function delete_selected_items() {
    var selected_items = index_selected_assets();
    var root_path = $("#path-breadcrumbs li").eq(-1).data("path");
    if(! (root_path)) { root_path = "/"; }
    var remove_path = "";
    var command_result = "";

    $('#pModalText').html("");
    $("#tblModalText tbody").empty();
    $("#btnModal_Confirm").hide();
    $("#btnModal_Cancel").html('Ok');
    var error_delete = false;

    if(selected_items.length > 0){

        $.each(selected_items, function(index, asset){

            var w2 = new arc.xml.XmlStringWriter();

            if($(asset).hasClass("folder-row")){

                if (root_path === "/"){
                    remove_path = root_path + $(asset).data("pathinfo");
                } else {
                    remove_path = root_path+"/" + $(asset).data("pathinfo");
                }
                w2.add("namespace",remove_path);
                var foldername = $(asset).data("pathinfo");
                try {
                    var command_result = arc.mf.client.RemoteServer.execute("asset.namespace.destroy", w2.document());
                    $("#tblModalText tbody").append('<tr><td style="text-decoration:line-through;">'+ foldername +'</td><td> successfully deleted </td></tr>');
                } catch(e1) {
                    try { // try two times to handle authentication error
                        var command_result_inner = arc.mf.client.RemoteServer.execute("asset.namespace.destroy", w2.document());
                        $("#tblModalText tbody").append('<tr><td style="text-decoration:line-through;">'+ foldername +'</td><td> successfully deleted </td></tr>');
                    } catch (e2) {
                        error_delete = true;
                        if (e.type() === "arc.mf.server.Services$ExServiceError"){
                            $("#tblModalText tbody").append('<tr><td>'+ foldername +'</td><td style="color:red;">Error: permission denied</td></tr>');
                        } else {
                            $("#tblModalText tbody").append('<tr><td>'+ foldername +'</td><td style="color:red;">Error: failed to delete</td></tr>');
                        }
                    }
               }

            } else {
                w2.add("id",$(asset).data("assetid"));
                var filename = $(asset).data('filename');

                try {
                    var command_result_asset = arc.mf.client.RemoteServer.execute("asset.destroy", w2.document());
                    $("#tblModalText tbody").append('<tr><td style="text-decoration:line-through;">'+ filename +'</td><td> successfully deleted </td></tr>');
                } catch(e1) {
                    try {
                        var command_result_asset_inner = arc.mf.client.RemoteServer.execute("asset.destroy", w2.document());
                        $("#tblModalText tbody").append('<tr><td style="text-decoration:line-through;">'+ filename +'</td><td> successfully deleted </td></tr>');
                    } catch(e2) {
                        error_delete = true;
                        if(e.type() === "arc.mf.server.Services$ExServiceError"){
                            $("#tblModalText tbody").append('<tr><td>'+ filename +'</td><td style="color:red;">Error: permission denied</td></tr>');
                        } else {
                            $("#tblModalText tbody").append('<tr><td>'+ filename +'</td><td style="color:red;">Error: failed to delete</td></tr>');
                        }
                    }
                }
            }
        });
    }

    if(error_delete === false){
        $('#myDeleteModal').modal('hide');
    }

    var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
    complex_query_namespace(root_path, current_page_size, 1);
}


function validate_input_namespace(e){

    if($("#new-namespace-input").val().length > 0) {
        $("#new-namespace-button").removeClass("disabled");
    }
    //invoke action on return
    if(e.keyCode == 13){
        make_new_namespace();
    }
}


function make_new_namespace() {
    var root_path = $("#path-breadcrumbs li").eq(-1).data("path");
    var new_path = "";

    if (root_path === ""){
        new_path = "/"+$("#new-namespace-input").val();
    } else {
        new_path = root_path+"/"+$("#new-namespace-input").val();
    }
    $("#show-new-namespace-button").popover("hide");

    var w2 = new arc.xml.XmlStringWriter();
    w2.add("namespace",new_path);

    livearc_execute_handler("asset.namespace.create", w2);

    //query_namespace(root_path,"");
    //simple_query_namespace(null, root_path);

    var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
    complex_query_namespace(root_path, current_page_size, 1);
}


function find_total_file_size_of_selected_files(selected_assets) {

    var bytes = 0;
    $.each(selected_assets, function(index, row){
        bytes = bytes + parseInt($(row).data('size'),10);
    });
    return bytes;
}


function update_selected_assets_info(){
    //  Updated the UI for table row selections.
    //  This is called after every selection
    var current_path = get_current_path();

    var selected_assets = index_selected_assets();

    if (selected_assets.length > 0 && current_path != '/' && current_path != '/projects') {

        // auto show menu
        show_selected_items_menu();

        // public
        $("#public-selected-items-group").find(".command-item").removeClass("disabled");

        // users
        $("#selected-items-count-view").html(selected_assets.length);
        $("#selected-items-group").find(".command-item").removeClass("disabled");
        $("#selected-items-group").find(".command-item-warning").removeClass("disabled");

        $("#clear-selection-button").show();
        // $("#share-files-button").removeClass("disabled");
        // $("#unshare-files-button").removeClass("disabled");

        if (selected_assets.length > 5){
            $("#download-files-button").addClass("disabled");
        } else {
            $("#download-files-button").removeClass("disabled");
        }

        var bytes = find_total_file_size_of_selected_files(selected_assets);
        $("#selected-file-status").html(selected_assets.length+' files selected ('+ bytes +' Bytes)');
        if (selected_assets.length > 1){
            $("#download-archive-button").removeClass("disabled");
        } else {
            $("#download-archive-button").addClass("disabled");
        }

        // if a folder is selected, can only download as zip.
        if (count_number_of_directories_selected() > 0) {
            $("#download-archive-button").removeClass("disabled");
            $("#download-files-button").addClass("disabled");
        }
    } else {
        // there's nothing selected, act accordingly

        // public
        $("#public-selected-items-group").find(".command-item").addClass("disabled");

        // users
        $("#selected-items-count-view").html(selected_assets.length);
        $("#selected-items-group").find(".command-item-warning").addClass("disabled");
        $("#selected-items-group").find(".command-item").addClass("disabled");
    }

    $("#show-new-namespace-button").removeClass("disabled");
    if (current_path == '/' || current_path == '/projects') {
        $("#show-new-namespace-button").addClass("disabled");
    }

}


function update_breadcrumb_for_search(search_text){
    $("#path-breadcrumbs").empty();
    $("#path-breadcrumbs").append('<li class="path-breadcrumb">Search results for <strong>'+search_text+'</strong></li>');
}


function update_path_breadcrumbs(current_path, isMetaFile){
    // sets the breadcrumb view at the top for the current path
    // We specifically catch the '/' path for display purposes.

    current_path = current_path.replace("//", "/");
    var path_sections = current_path.split("/");

    $("#path-breadcrumbs").empty();
    $("#path-breadcrumbs").unbind();

    //var domain = $("#domain_button_group").find("label.active").data("domain");

    if((_global_domain != "ivec")){
        if (current_path === "/"){
            $("#path-breadcrumbs").append('<li class="path-breadcrumb" data-path="/"><strong><i class="fa fa-home"></i> Home</strong></li>');
        } else {
            $("#path-breadcrumbs").append('<li class="path-breadcrumb" data-path="/"><i class="fa fa-home"></i> Home</li>');
        }
    }

    var cumulative_path = "";
    for (var index=1; index < path_sections.length; index++ ){
        cumulative_path = cumulative_path+"/"+path_sections[index];
        if (index === path_sections.length - 1 ){
            //if its a file, don't make it clickable
            if(isMetaFile === true) {
                 $("#path-breadcrumbs").append('<li data-path='+cumulative_path+'><strong>'+path_sections[index] +'</strong></li>');
            } else {
                $("#path-breadcrumbs").append('<li class="path-breadcrumb" data-path="'+cumulative_path+'"><strong><i class="fa fa-folder-open-o"></i> '+path_sections[index] +'</strong></li>');
            }
        } else {
            $("#path-breadcrumbs").append('<li class="path-breadcrumb" data-path="'+cumulative_path+'"><i class="fa fa-folder-o"></i> '+path_sections[index] +'</li>');
        }
    }

    var netsize = 0;
    var endsize = 0;
    var count = 0;
    $.each($("#path-breadcrumbs li"),function(index,element){
        netsize = netsize + $(this).outerWidth();
        count = index;
    });

    var size_reduction = 0;
    endsize = $("#path-breadcrumb-container").outerWidth(true);

    if( netsize > endsize){
        //overflow in the breadcrumb view
        size_reduction = (netsize - endsize)/(count-1);
    }

    if (size_reduction > 0){
        $.each($("#path-breadcrumbs li"),function(index,element){
            if ((index) < count){
                $(this).addClass("crumb-overflow");
                $(this).width(($(this).outerWidth()-size_reduction));
            }
        });
    }

    $( ".path-breadcrumb").click(function(e) {
        //query_namespace($(e.currentTarget).data('path'),"");
        //simple_query_namespace(null, $(e.currentTarget).data('path'));
        $(e.currentTarget).append(" <span class='bubble-label bubble-small'><i class='fa fa-cog fa-spin'></i> Loading </span>");
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        complex_query_namespace($(e.currentTarget).data('path'), current_page_size, 1);
    });

    return path_sections;
}


function number_of_files_in_a_directory(current_path) {

    var w2 = new arc.xml.XmlStringWriter();
    w2.add("action","count");
    var argWhere = "namespace >= '" + current_path +"'";
    w2.add("where", argWhere);

    var command = livearc_execute_handler("asset.query", w2);
    var data = parse_asset(command);
    return data.value;
}



function populate_namespace_size(path, view_object){
    function populate_namespace_callback(response,args){
        // call back for populate_namespace_size()
        var data = parse_command_xml(response);
        view_object.html(Math.round(data.value/(1024*1024) *100)/100+" MB");
    }
    function populate_namespace_callback_error(response){
        // call back for populate_namespace_size()
    }

    var w2 = new arc.xml.XmlStringWriter();
    path  = path.replace(/'/g, "&apos;");
    w2.add("where","namespace>='"+path+"'");
    w2.add("action", "sum");
    w2.add("xpath", "content/size");
    livearc_async_generic_handler("asset.query", w2, populate_namespace_callback, populate_namespace_callback_error);
}


function get_namespace_size(current_path){
    //ask livearc for computation of namespace size (bytes)
    var w2 = new arc.xml.XmlStringWriter();
    w2.add("where","namespace>='"+current_path+"'");
    w2.add("action", "sum");
    w2.add("xpath", "content/size");

    var command = livearc_execute_handler("asset.query", w2);
    var data = parse_command_xml(command);
    return data.value;
}


function async_thumbnail_response(response,args){
    console.log('async_thumbnail_response',response);
}

function async_thumbnail_error(response,args){

}

function toggle_detail_public_button(){

    var asset_id = $("#public-file-toggle-button").data('asset-id');

    var published = check_published_status(asset_id);
    var result = 0;

    if(!published){
        result = share_asset(asset_id);
    } else {
        result = unshare_asset(asset_id);
    }

    published = check_published_status(asset_id);

    if(published){
        $("#public-file-toggle-button").html("<i class='fa fa-ban text-danger'> Don't share file");
        $("#detail-public-path").removeClass("see-through");
    } else {
        $("#public-file-toggle-button").html("<i class='fa fa-share'></i> Share this file");
        $("#detail-public-path").addClass("see-through");
    }
}

function show_namespace_detail(event) {

    $("#modal-namespace-detail-view").modal('show');

    var namespace_path = $(event.currentTarget).parent().data('fullpath');

    namespace_path = namespace_path.replace("//", "/");

    var public_namespace_path = "";
    var split_path = namespace_path.split('/');
    var parent_path = "/"+split_path.slice(0,split_path.length-1).join("/");


    if (split_path[1] === 'projects'){
        public_namespace_path =  "/"+split_path.slice(2,split_path.length).join("/");
    } else {
        public_namespace_path = namespace_path;
    }

    $("#namespace-detail-name").html(namespace_path);

    $("#namespace-detail-project-path").html("https://data.pawsey.org.au/projects/?path=" + namespace_path);
    if(_global_domain === "public"){
        $("#private-namespace-holder").addClass("hidden");
    }

    $("#namespace-detail-public-path").html("https://data.pawsey.org.au/public/?path=" + public_namespace_path);

    populate_namespace_detail(namespace_path);
}


function populate_namespace_detail(current_path) {

    $("#namespace-busy-spinner").removeClass("hidden");

    function update_namespace_detail(response, command){
        var details = xmlToJson(response._xml);

        // a new async task is called.  This one can be quite slow for large projects.
        populate_namespace_size(current_path, $("#namespace-net-size"));

        if (details.namespace.hasOwnProperty("acount")){
            $("#namespace-net-count").html(details.namespace.acount["#text"]);
        }

        $("#namespace-net-size").html("<i class='fa-li fa fa-gear fa-spin'></i>  Processing...");

        if (details.namespace.hasOwnProperty("nscount")){
            $("#namespace-subfolder-count").html(details.namespace.nscount["#text"]);
        }

        if (details.namespace.hasOwnProperty("store")){
            $("#namespace-store-name").html(details.namespace.store["#text"]);
        }

        if (details.namespace.hasOwnProperty("creator")){
            if (details.namespace.creator.hasOwnProperty("user")) {
                $("#namespace-creator-name").html(details.namespace.creator.user["#text"]);
            }
        }

        if (details.namespace.hasOwnProperty("mmtime")){
            $("#namespace-last-modified").html(details.namespace.mmtime["#text"]);
        }

        if (details.namespace.hasOwnProperty("ctime")){
            $("#namespace-created").html(details.namespace.ctime["#text"]);
        }

        $("#namespace-busy-spinner").addClass("hidden");
    }

    function update_namespace_detail_error(response){

    }

    var args = new arc.xml.XmlStringWriter();
    args.add("namespace",current_path);
    args.add("nscount", 'true');

    var command = livearc_async_generic_handler("asset.namespace.describe", args, update_namespace_detail, update_namespace_detail_error);
}


function show_path_not_found(path, message){

    $("#file-display").addClass("hidden");
    $("#direct-download-view").addClass("hidden");

    $("#path-notfound-view").removeClass("hidden");

    if(message){
        $("#download-error-title").html("<p>"+message+"</p>");
    }

    $("#filesystem-link").click(function(){
        $("#path-notfound-view").addClass("hidden");
        $("#file-display").removeClass("hidden");
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        complex_query_namespace("/", current_page_size, 1);
    });
}


function show_asset_download_page(id, current_path, asset_name) {

    // call backs
    function get_asset_icon_callback(response){
        $("#downloadpage-icon").html("<img width='100%' class='img-responsive' src='"+response+"'>");
    }

    function get_asset_icon_callback_error(response){
        console.log('get_asset_icon_callback_error',response);
    }

    $("#direct-download-view").removeClass("hidden");
    $("#file-display").addClass("hidden");
    var temp = livearc_execute_handler("system.session.self.describe", null);
    var result = xmlToJson(temp._xml);

    var user = result.session.actor['#text'];


    $("#downloadpage-icon").removeClass("hidden");
    if (user === "public:public"){
        $("#downloadpage-icon").addClass("hidden");
    }

    var command_args = new arc.xml.XmlStringWriter();
    command_args.add("id", id);
    var command  = livearc_execute_handler("asset.get", command_args, true);

    if(command.hasOwnProperty("_xml")){
        var metadata = xmlToJson(command._xml);

        $("#downloadpage-link").click(function(){
            download_asset_by_id(metadata.asset["@attributes"].id, metadata.asset["@attributes"].version, metadata.asset.name["#text"]);
        });

        $("#downloadpage-title").text(asset_name);

        $("#downloadpage-returnlink").text("To "+metadata.asset.namespace["#text"]);

        $("#downloadpage-returnlink").click(function(){
            $("#direct-download-view").addClass("hidden");
            $("#file-display").removeClass("hidden");
            //simple_query_namespace(null, metadata.asset.namespace["#text"]);
            var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
            complex_query_namespace(metadata.asset.namespace["#text"], current_page_size, 1);
        });

// DS-552 - Oct 2024 - disable thumbnail retrieval
//        var args = new arc.xml.XmlStringWriter();
//        args.add("id", id);
//        args.add("size", 200);
//        livearc_async_execute_with_output_handler("asset.icon.get", args, get_asset_icon_callback);



    } else {
        show_path_not_found(current_path, 'Public file not found');
    }
}


function show_asset_detail(id, current_path, asset_name) {
    //whoami?

    $("#modal-datametaview").modal('show');

    var command = livearc_execute_handler("system.session.self.describe", null);
    var session_data = parse_asset(command.element('session'));

    $("#private-asset-holder").show();
    $("#public-file-toggle-button").show();

    if(session_data.user === "public"){
        $("#private-asset-holder").hide();
        $("#public-file-toggle-button").hide();
    }

    // This function displays the detail metadata for a selected asset
    $("#metadata-table tbody").empty();

    // get icon
// DS-552 - Oct 2024 - disable thumbnail retrieval
//    var command_args2 = new arc.xml.XmlStringWriter();
//    command_args2.add("id", id);
//    command_args2.add("size", 200);
//    var command2  = livearc_execute_with_output_handler("asset.icon.get", command_args2,null);

    var command_args = new arc.xml.XmlStringWriter();
    command_args.add("id", id);
    command_args.add("get-content-status", "true");
    command_args.add("get-user-meta", "true");

    if(session_data.user != "public"){
            command_args.add("get-versions", "true");
    }

    command  = livearc_execute_handler("asset.get", command_args);

    var dict = {};
    var metadata = parse_asset_recursive(command, dict, "");
    var metadata_json = xmlToJson(command._xml);

    var split_path = metadata.response_asset_namespace.split('/');
    var parent_path = "/"+split_path.slice(0,split_path.length-1).join("/");

    if (split_path[1] === 'projects'){
        public_namespace_path =  "/"+split_path.slice(2,split_path.length).join("/");
    } else {
        public_namespace_path = metadata.response_asset_namespace;
    }


//  get labels

    $("#public-file-toggle-button").attr('data-asset-id',id);

    var published = check_published_status(id);

    if(published){
        $("#public-file-toggle-button").html("<i class='fa fa-ban text-danger'> Don't share file");
        $("#detail-public-path").removeClass("see-through");

    } else {
        $("#public-file-toggle-button").html("<i class='fa fa-share'></i> Share this file");
        $("#detail-public-path").addClass("see-through");
    }

    $("#detail-filename").html(metadata.response_asset_name);

    var asset_path = metadata.response_asset_namespace+"/"+metadata.response_asset_name;
    $("#detail-project-path").text(window.location.origin+"/projects/?path="+ asset_path);

    var asset_public_path = public_namespace_path+"/"+metadata.response_asset_name;
    $("#detail-public-path").text(window.location.origin+"/download"+asset_public_path);

    var tbl = prettyPrint( metadata_json.asset);
    var tds = $(tbl).find("td");

    // cleanup text artifacts from xml2json conversion (for readability)
    $.each(tds, function(){
        if ($(this).text() === "#text"){
            $(this).remove();
        }
        if ($(this).text() === "@attributes"){
            $(this).text("xml attributes");
        }
    });

    $("#metadata-table").append(tbl);

    $('a[href="#tab-datametaview"]').tab('show');

    //$("#btn_back_datatable").data("returnpath",get_current_path());
    $("#detail-download-asset").click(function(){
        download_asset_by_id(metadata.response_asset_id, metadata.response_asset_version, metadata.response_asset_name);
    });
}


function download_asset_by_id(id, version, filename){
    var content_url = arc.mf.client.RemoteServer.attachmentContentUrl(id, version);
    var download_command = download_file(content_url,0,filename);
}


function update_table_after_sort(){
    console.log('times be a changin');
}


function find_filetype_category(datatype){
    for (var key in file_type_obj){
        if (file_type_obj.hasOwnProperty(key)) {
            if($.inArray(datatype, file_type_obj[key]) > -1)
            { return key.toString();}
        }
    }
    return datatype;
}


function calculate_size_in_bytes(size, option){
    var output = null;
    switch (option.toLowerCase()) {
        case "byte":
            output = parseInt(size);
            break;
        case "kb":
            output = parseInt(size * 1024);
            break;
        case "mb":
            output = parseInt(size * 1048576);
            break;
        case "gb":
            output = parseInt(size * 1073741824);
            break;
        case "tb":
            output = parseInt(size * 1099511627776);
            break;
        default:
            output = parseInt(size);
    }
    return output;
}


function disable_all_file_actions() {
    $("#data-edit-set").addClass("disabled");
    $("#edit-mode-set").addClass("disabled");
}


function compress_file_table_view(){
    // hide columns when we need a small view
    $(".date-column").hide();
    $(".control-column").hide();
}


function showall_file_table_view() {
    $(".date-column").show();
    $(".control-column").show();
}


function make_file_name_uneditable(table_data_entry){
    //undo everything we did to make the cell editable
    $plain_paragraph = $("<p></p>");
    $plain_paragraph.addClass("file-name-input-field");
    $plain_paragraph.html($(table_data_entry).attr("original_value"));
    $(".perform-rename-button-style").remove();
    $(".cancel-rename-button-style").remove();
    $(table_data_entry).after($plain_paragraph).remove();

    set_file_row_edit_behaviour();
}


function rename_fileview_name(table_data_entry){
    // perform the rename. error is shown if the user doesn't have permission
    var name = $(table_data_entry).val();
    var table_row = $(table_data_entry).parent().parent();

    if (table_row.hasClass('folder-row')){
        var fullpath = get_current_path()+"/"+table_row.data('pathinfo');
        var new_fullpath = get_current_path()+"/"+name;

        //update the path related data objects on the DOM to match the new name
        $(table_row).data('pathinfo',name);
        $(table_row).data('fullpath',new_fullpath);

        rename_namespace(fullpath,name);
    } else if (table_row.hasClass('file-row')){
        rename_asset(table_row.data('assetid'),name);
    }

    //undo everything we did to make the cell editable
    $plain_paragraph = $("<p></p>");
    $plain_paragraph.addClass("file-name-input-field");
    $plain_paragraph.html(name);

    $(".perform-rename-button-style").remove();
    $(".cancel-rename-button-style").remove();
    $(table_data_entry).after($plain_paragraph).remove();

    set_file_row_edit_behaviour();
}


function make_file_name_editable(table_data_entry){

        var text_contents = $(table_data_entry).html();

        var $renamebutton = $("<span><i class='fa fa-check'></i></span>");
        $renamebutton.addClass("filename-edit-done");
        $renamebutton.addClass("perform-rename-button-style");
        $renamebutton.addClass("pull-right");
        $renamebutton.attr({id: "rename-file-button"});

        $renamebutton.click(function(e){
            e.stopPropagation();
            rename_fileview_name("#currently-edited-name");
        });

        var $cancelbutton = $("<span><i class='fa fa-times'></i></span>");
        $cancelbutton.addClass("filename-edit-done");
        $cancelbutton.addClass("cancel-rename-button-style");
        $cancelbutton.addClass("pull-right");

        $cancelbutton.click(function(e){
            e.stopPropagation();
            make_file_name_uneditable("#currently-edited-name");
        });

        $textarea = $("<textarea></textarea>").attr({
            id: "currently-edited-name",
            original_value: $(table_data_entry).html()
        });

        $textarea.val($(table_data_entry).html());
        $textarea.addClass("edit-filename-view");

        $textarea.click(function(e){e.stopPropagation();});

        $(table_data_entry).after($renamebutton);
        $(table_data_entry).after($cancelbutton).after($textarea).remove();
}


function setup_file_pager_controls_browse(){
    $("#pager-firstpage-button").unbind().click(function(){
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        var current_namespace =  $("#file-table").attr("data-current-namespace");
        complex_query_namespace(current_namespace, current_page_size, 1);
    });

    $("#pager-previouspage-button").unbind().click(function(){
        var current_page =  parseInt($("#file-table").attr("data-current-page"),10);
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        var current_namespace = $("#file-table").attr("data-current-namespace");
        complex_query_namespace(current_namespace, current_page_size, current_page - 1);
    });

    $("#pager-nextpage-button").unbind().click(function(){
        var current_page =  parseInt($("#file-table").attr("data-current-page"),10);
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        var current_namespace = $("#file-table").attr("data-current-namespace");
        complex_query_namespace(current_namespace, current_page_size, current_page + 1);
    });

    $("#pager-lastpage-button").unbind().click(function(){
        var current_page =  parseInt($("#file-table").attr("data-current-page"),10);
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        var current_namespace = $("#file-table").attr("data-current-namespace");
        var last_page = parseInt($("#file-table").attr("data-last-page"),10);
        complex_query_namespace(current_namespace, current_page_size, last_page);
    });

    $("#pager-pagesize-selector").unbind().change(function(e){
        var current_namespace = $("#file-table").attr("data-current-namespace");
        $("#pager-pagesize-selector option:selected").each(function(){
            page_size = $(this).text();
        });
        complex_query_namespace(current_namespace, page_size, 1);
    });
}


function setup_file_pager_controls_search(){
    $("#pager-firstpage-button").unbind().click(function(){
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        var current_search =  $("#file-table").attr("data-current-search");
        simple_text_search(current_search, current_page_size, 1);
    });

    $("#pager-previouspage-button").unbind().click(function(){
        var current_page =  parseInt($("#file-table").attr("data-current-page"),10);
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
                var current_search =  $("#file-table").attr("data-current-search");
        simple_text_search(current_search, current_page_size, current_page - 1);
    });

    $("#pager-nextpage-button").unbind().click(function(){
        var current_page =  parseInt($("#file-table").attr("data-current-page"),10);
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
                var current_search =  $("#file-table").attr("data-current-search");
        simple_text_search(current_search, current_page_size, current_page + 1);
    });

    $("#pager-lastpage-button").unbind().click(function(){
        var current_page =  parseInt($("#file-table").attr("data-current-page"),10);
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        var last_page = parseInt($("#file-table").attr("data-last-page"),10);
                var current_search =  $("#file-table").attr("data-current-search");
                simple_text_search(current_search, current_page_size, last_page);
    });

    $("#pager-pagesize-selector").unbind().change(function(e){
        var current_namespace = $("#file-table").attr("data-current-namespace");
        $("#pager-pagesize-selector option:selected").each(function(){
            page_size = $(this).text();
        });
                var current_search =  $("#file-table").attr("data-current-search");
                simple_text_search(current_search, page_size, 1);
    });
}


function populate_file_search_json(asset_list, search_text){
    $("#file-table tbody").empty();

    update_breadcrumb_for_search(search_text);

    // var path_sections = update_path_breadcrumbs(search_text, false);
    if (asset_list !== null){

        var asset_array = [];
        if(asset_list.hasOwnProperty("length")){
            $.each(asset_list, function(index, item){
                var id = item.id["#text"];
                var name = "null";
                if(item.hasOwnProperty('name')){
                        if (item.name.hasOwnProperty("#text")){
                                name = item.name["#text"];
                        }
                }

                var size = "0";
                if (item.hasOwnProperty("size")){
                        if (item.size.hasOwnProperty("#text")){
                                size = item.size["#text"];
                        }
                }

                var type = "unknown";
                if (item.hasOwnProperty("type")){
                        if (item.type.hasOwnProperty("#text")){
                                type = item.type["#text"];
                        }
                }

                var mod_time = "-1";
                if (item.hasOwnProperty("mtime")){
                        if (item.mtime.hasOwnProperty("#text")){
                                mod_time = item.mtime["#text"];
                        }
                }

                var version = 1;
                if (item.hasOwnProperty("@attributes")){
                        version  = item["@attributes"].version;
                }

                var published = false;
                if (item.hasOwnProperty("published")){
                        published = true;
                }

                var state = "unknown";
                if (item.hasOwnProperty("state")){
                        state = item.state["#text"];
                }

                asset_array.push({'id':id, 'name':name, 'size':size, 'type':type, 'mod_time':mod_time, 'version':version, 'published':published, 'state': state});
            });
        } else {
            var id = asset_list.id["#text"];
            var name = "null";
            if(asset_list.hasOwnProperty('name')){
                    if (asset_list.name.hasOwnProperty("#text")){
                            name = asset_list.name["#text"];
                    }
            }

            var size = "0";
            if (asset_list.hasOwnProperty("size")){
                    if (asset_list.size.hasOwnProperty("#text")){
                            size = asset_list.size["#text"];
                    }
            }

            var type = "unknown";
            if (asset_list.hasOwnProperty("type")){
                    if (asset_list.type.hasOwnProperty("#text")){
                            type = asset_list.type["#text"];
                    }
            }

            var mod_time = "-1";
            if (asset_list.hasOwnProperty("mtime")){
                    if (asset_list.mtime.hasOwnProperty("#text")){
                            mod_time = asset_list.mtime["#text"];
                    }
            }

            var version = 1;
            if (asset_list.hasOwnProperty("@attributes")){
                    version  = asset_list["@attributes"].version;
            }
            var published = false;

            if (asset_list.hasOwnProperty("published")){
                    published = true;
            }
            var state = "unknown";
            if (asset_list.hasOwnProperty("state")){
                    state = asset_list.state["#text"];
            }


            asset_array.push({'id':id, 'name':name, 'size':size, 'type':type, 'mod_time':mod_time, 'version':version, 'published':published, 'state': state});
        }
        populate_file_table_with_assets(asset_array);
    }

    update_selected_assets_info();

    update_file_table_actions(null);
}


function populate_file_table_json(namespace_list, asset_list, context_path){

    $("#file-table tbody").empty();

    var path_sections = update_path_breadcrumbs(context_path, false);

    if(namespace_list !== null){
        var namespace_array = [];

        if(namespace_list.hasOwnProperty("length")){
            $.each(namespace_list, function(index, item){
                var name = item.name["#text"];
                var full_path = context_path + "/" + name;

                full_path.replace('"',"&quot;");
                namespace_array.push({'full_path':full_path, 'name': name});
            });
        } else {
            var name = namespace_list.name["#text"];
            //handle quotes in name
            var full_path = context_path + "/" + name;

            full_path.replace('"',"&quot;");
            namespace_array.push({'full_path':full_path, 'name': name});
        }

        $.each(namespace_array, function (index,item){
            $("#file-table tbody").append('<tr class="folder-row" data-nfiles="-" data-fullpath ="'+item.full_path+'" data-pathinfo="'+item.name+'"><td class="icon-column"><i class="icon fa fa-folder-o fa-lg"></i></td><td class="name-column"><p class="file-name-input-field" >'+item.name+'</p></td><td class="size-column hidden-xs">-</td><td class="hidden-xs hidden-sm date-column">-</td><td class="control-column"></td><td class="control-column"></td><td class="highlight-cell control-column namespace-info-cell"><i class="fa fa-info-circle fa-2x icon-compress"></i><span></td><td class="highlight-cell control-column namespace-open-cell"><i class="fa fa-chevron-right fa-2x icon-compress"></i></td></tr>');
        });
    }

    if (asset_list !== null){

        var asset_array = [];

        if(asset_list.hasOwnProperty("length")){
            $.each(asset_list, function(index, item){
                var id = item.id["#text"];
                var name = "null";
                if(item.hasOwnProperty('name')){
                    if (item.name.hasOwnProperty("#text")){
                        name = item.name["#text"];
                    }
                }

                var size = "0";
                if (item.hasOwnProperty("size")){
                    if (item.size.hasOwnProperty("#text")){
                        size = item.size["#text"];
                    }
                }

                var type = "unknown";
                if (item.hasOwnProperty("type")){
                    if (item.type.hasOwnProperty("#text")){
                        type = item.type["#text"];
                    }
                }

                var mod_time = "-1";
                if (item.hasOwnProperty("mtime")){
                    if (item.mtime.hasOwnProperty("#text")){
                        mod_time = item.mtime["#text"];
                    }
                }

                var version = 1;
                if (item.hasOwnProperty("@attributes")){
                    version  = item["@attributes"].version;
                }

                var published = false;
                if (item.hasOwnProperty("published")){
                    published = true;
                }

                var state = "unknown";
                if (item.hasOwnProperty("state")){
                        state = item.state["#text"];
                }

                asset_array.push({'id':id, 'name':name, 'size':size, 'type':type, 'mod_time':mod_time, 'version':version, 'published':published, 'state': state});
            });
        } else {
            var id = asset_list.id["#text"];
            var name = "null";
            if(asset_list.hasOwnProperty('name')){
                if (asset_list.name.hasOwnProperty("#text")){
                    name = asset_list.name["#text"];
                }
            }

            var size = "0";
            if (asset_list.hasOwnProperty("size")){
                if (asset_list.size.hasOwnProperty("#text")){
                    size = asset_list.size["#text"];
                }
            }

            var type = "unknown";
            if (asset_list.hasOwnProperty("type")){
                if (asset_list.type.hasOwnProperty("#text")){
                    type = asset_list.type["#text"];
                }
            }

            var mod_time = "-1";
            if (asset_list.hasOwnProperty("mtime")){
                if (asset_list.mtime.hasOwnProperty("#text")){
                    mod_time = asset_list.mtime["#text"];
                }
            }

            var version = 1;
            if (asset_list.hasOwnProperty("@attributes")){
                version  = asset_list["@attributes"].version;
            }
            var published = false;

            if (asset_list.hasOwnProperty("published")){
                published = true;
            }

            var state = "unknown";
            if (asset_list.hasOwnProperty("state")){
                    state = asset_list.state["#text"];
            }


            asset_array.push({'id':id, 'name':name, 'size':size, 'type':type, 'mod_time':mod_time, 'version':version, 'published':published, 'state': state});
        }

        populate_file_table_with_assets(asset_array);

    }

    update_selected_assets_info();

    update_file_table_actions(context_path);
}

function populate_file_table_with_assets(asset_array){
    $.each(asset_array, function(index, item){

        var func_meta = "show_asset_detail(" + item.id + ",null,null);";

        var icon_html = "";

        if (item.type.indexOf("image") != -1){
                icon_html = '<i class="icon fa fa-file-image-o fa-lg"></i>';
        } else if(item.type.indexOf("spreadsheet") != -1 || item.type.indexOf("excel") != -1){
                icon_html = '<i class="icon fa fa-file-excel-o fa-lg"></i>';
        } else if(item.type.indexOf("word") != -1){
                icon_html = '<i class="icon fa fa-file-word-o fa-lg"></i>';
        } else if(item.type.indexOf("pdf") != -1){
                icon_html = '<i class="icon fa fa-file-pdf-o fa-lg"></i>';
        } else if(item.type.indexOf("audio") != -1){
                icon_html = '<i class="icon fa fa-file-audio-o fa-lg"></i>';
        } else if(item.type.indexOf("x-zip") != -1 || item.type.indexOf("x-tar") != -1){
                icon_html = '<i class="icon fa fa-file-archive-o fa-lg"></i>';
        } else if(item.type.indexOf("text") != -1){
                icon_html = '<i class="icon fa fa-file-text-o fa-lg"></i>';
        } else if(item.type.indexOf("video") != -1){
                icon_html = '<i class="icon fa fa-file-video-o fa-lg"></i>';
        } else if(item.type.indexOf("javascript") != -1){
                icon_html = '<i class="icon fa fa-file-code-o fa-lg"></i>';
        } else if(item.type.indexOf("presentation") != -1 || item.type.indexOf("powerpoint") != -1){
                icon_html = '<i class="icon fa fa-file-powerpoint-o fa-lg"></i>';
        } else if(item.type.indexOf("unknown") != -1){
                icon_html = '<i class="icon fa fa-question fa-lg"></i>';
        } else {
                icon_html = '<i class="icon fa fa-file-o fa-lg"></i>';
        }

        var sharing_icon_html = '<i class="fa fa-share-alt fa-2x icon-compress public-share-icon text-success hidden"></i>';
        if(item.published){
            sharing_icon_html = '<i class="fa fa-share-alt fa-2x icon-compress public-share-icon text-success"></i>';
        }

        var size_as_text = item.size+" Bytes";

        if (item.size > 1073741824){
            // GBs
            size_as_text = Math.round(item.size/1073741824)+" GB";
        } else if (item.size > 1048576){
            // MBs
            size_as_text = Math.round(item.size/1048576)+" MB";
        } else if (item.size > 1024) {
            // KBs
            size_as_text = Math.round(item.size/1024)+" KB";
        }

        if (item.size === null){
            size_as_text = "null";
        }

        var state_display_class = "hidden";
        var state_display = "";

        // SDF - adding yet another state it could be in ...
        if (item.state === "online" || item.state === "online+offline" || item.state === "reachable" ){
            // do nothing
            // var result = $("#file-table tbody .file-row[data-assetid="+asset_id+"]");
        } else if (item.state === "migrating"){
// SDF - "preparing" could be misleading on a freshly uploaded file that is being migrated to tape
//            state_display = "<i class='fa fa-refresh fa-spin'></i> preparing";
//            state_display_class = "migrating-status-preparing tiny-text";
            state_display = "migrating";
            state_display_class = "migrating-status-preparing tiny-text";

        } else if (item.state === "offline"){
            state_display = "offline";
            state_display_class = "migrating-status-offline tiny-text";
        } else {
            state_display = "missing content";
            state_display_class = "migrating-status-offline tiny-text";
            size_as_text = "--";
        }

        // this defines a file entry in the table
        $("#file-table tbody").append('<tr class="file-row" data-filetype="'+ item.type +'" data-size="'+item.size+'" data-assetid="'+item.id+'" data-version="'+item.version+'" data-filename="'+item.name+'"><td class="icon-column">'+icon_html+'</i></td><td class="name-column"><p class="file-name-input-field">'+item.name+'</p></td><td class="size-column hidden-xs">'+size_as_text+'</td><td class="hidden-xs hidden-sm time-column">'+item.mod_time+'</td><td class="migrate-column"><span class="dmf-indicator '+state_display_class+'">'+state_display+'</span></td><td class="public-share-indicator control-column">'+sharing_icon_html+'</td><td class="highlight-cell control-column" onclick="'+ func_meta +' event.stopPropagation();"><i class="fa fa-info-circle fa-2x icon-compress"></i></td><td></td></tr>');
    });
}


function set_file_row_edit_behaviour(){
    $(".name-column").unbind();

    // click and hold to activate filename editablenessosity
    var tid = 0;
    $(".name-column").bind('mousedown', function(e){
        e.stopPropagation();
        tid = setTimeout(make_file_name_editable, 500, $(this).find(".file-name-input-field"));
    });
    $(".name-column").bind('mouseup mouseout', function(e){
        clearTimeout(tid);
    });
}


function update_file_table_actions(context_path){

    set_file_row_edit_behaviour();

    $( ".folder-row").unbind();

    $( ".folder-row").dblclick(function(e) {
        var page_size = 10;
        $(e.currentTarget).find('.name-column').append("<span class='bubble-label'><i class='fa fa-cog fa-spin'></i> Loading </span>");

        $("#pager-pagesize-selector option:selected").each(function(){
            page_size = $(this).text();
        });

        var page_number = 1;
        complex_query_namespace(context_path+"/"+$(e.currentTarget).data('pathinfo'), page_size, page_number);
    });

    $( ".folder-row").click(function(e) {
        if($(e.currentTarget).hasClass("info")){
            $(e.currentTarget).removeClass("info");
        } else {
            $(e.currentTarget).addClass("info");
        }
        update_selected_assets_info();
    });

    $( ".file-row").unbind();

    $( ".file-row").dblclick(function(e) {
        show_asset_detail($(e.currentTarget).data('assetid'), context_path,$(e.currentTarget).data('filename'));
    });

    $( ".file-row").click(function(e) {
        if($(e.currentTarget).hasClass("info")){
            $(e.currentTarget).removeClass("info");
        } else {
            $(e.currentTarget).addClass("info");
        }
        update_selected_assets_info();
    });

    $(".namespace-info-cell").unbind().click(function(e){
        e.stopPropagation();
        show_namespace_detail(e);
    });

    $(".namespace-open-cell").unbind().click(function(e){
        e.stopPropagation();
        //simple_query_namespace($(e.currentTarget).parent(),$(e.currentTarget).parent().data('fullpath'));
        var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
        complex_query_namespace($(e.currentTarget).parent().data('fullpath'), current_page_size, 1);
    });

    $( ".download-asset-cell").unbind().click(function(e){
        download_asset_by_event(e);
        e.stopPropagation();
    });

        setup_file_pager_controls_browse();
}


function handleImageExifData(asset_xmlelement, asset_metadata, parent_path) {

    if(!(parent_path === "")){
        parent_path = parent_path + "_";
    }

    if(asset_xmlelement.elements() !== null) {
        $.each(asset_xmlelement.elements(), function(index, xml_element) {
           if(xml_element.name().indexOf("attribute") > -1 && xml_element.value() !== null && xml_element.attributes() !== null){
                var attr_name = "";
                $.each(xml_element.attributes(), function(index, attribute){
                    if(attribute.name() == "name"){
                        attr_name = parent_path + asset_xmlelement.name() + "_" + attribute.value();
                    }
                });

                if(!(attr_name in asset_metadata)){
                    asset_metadata[attr_name] = xml_element.value();
                }
            }
        });
    }
    return asset_metadata;
}


function parse_asset_recursive(asset_xmlelement, asset_metadata, parent_path) {

    if(!(parent_path === "")){
        parent_path = parent_path + "_";
    }

    //bandaid
    if (typeof asset_xmlelement === "string"){
        console.log('asset_xmlelement failed');
        return null;
    }

    if(asset_xmlelement.attributes() !== null){
        $.each(asset_xmlelement.attributes(), function(index, attribute){
            var attr_name = parent_path + asset_xmlelement.name() + "_" + attribute.name();
            if(!(attr_name in asset_metadata))
            {
                asset_metadata[attr_name] = attribute.value();
            }
        });
    }

    if(asset_xmlelement.elements() !== null) {
        $.each(asset_xmlelement.elements(), function(index, xml_element) {
            //checking for mf-image-tiff & mf-image-exif
            if(xml_element.name().indexOf("mf-image-") > -1){
                asset_metadata = handleImageExifData(xml_element, asset_metadata, (parent_path + asset_xmlelement.name()));
            } else {
                asset_metadata = parse_asset_recursive(xml_element, asset_metadata, (parent_path + asset_xmlelement.name()));
            }
        });
    } else if(asset_xmlelement.value() !== null) {
        var element_name = parent_path + asset_xmlelement.name();
        //!(element_name in asset_metadata)
        asset_metadata[element_name] = asset_xmlelement.value();
    }
    return asset_metadata;
}

function xmlToJson(xml) {

    // Create the return object
    var obj = {};

    if (xml.nodeType == 1) { // element
        // do attributes
        if (xml.attributes.length > 0) {
        obj["@attributes"] = {};
            for (var j = 0; j < xml.attributes.length; j++) {
                var attribute = xml.attributes.item(j);
                obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
            }
        }
    } else if (xml.nodeType == 3) { // text
        obj = xml.nodeValue;
    }

    // do children
    if (xml.hasChildNodes()) {
        for(var i = 0; i < xml.childNodes.length; i++) {
            var item = xml.childNodes.item(i);
            var nodeName = item.nodeName;
            if (typeof(obj[nodeName]) == "undefined") {
                obj[nodeName] = xmlToJson(item);
            } else {
                if (typeof(obj[nodeName].push) == "undefined") {
                    var old = obj[nodeName];
                    obj[nodeName] = [];
                    obj[nodeName].push(old);
                }
                obj[nodeName].push(xmlToJson(item));
            }
        }
    }
    return obj;
}


function parse_asset(asset_xmlelement){
    // grabs all xml elements and attributes into JSON for easy access.
    // Nested elements simply get concatenated names.

    var asset_metadata = {};

    if(asset_xmlelement.hasAttributes()){
        $.each(asset_xmlelement.attributes(), function(index, attribute){
            asset_metadata[attribute.name()] = attribute.value();
        });
    }

    if(asset_xmlelement.name()){
        var asset_name = '';
        asset_name = asset_xmlelement.name();

        if(asset_xmlelement.hasAttributes()){
            $.each(asset_xmlelement.attributes(), function(index, attribute){
                if (attribute.name() === 'id'){
                    asset_name = asset_name + "_" + attribute.value();
                }
                asset_metadata[attribute.name()] = attribute.value();
            });
        }
        if (asset_xmlelement.value()){
            asset_metadata[asset_name] = asset_xmlelement.value();
        }
    }

    if (asset_xmlelement.hasElement()){
        $.each(asset_xmlelement.elements(), function(index, metadata) {

            var id_text = null;
            var local_name = '';

            local_name = metadata.name();

            if(metadata.hasAttributes()){
                $.each(metadata.attributes(), function(index, attribute){
                    if (attribute.name() === 'id'){
                        local_name = local_name + "_" + attribute.value();
                    }
                });
            }

            if(metadata.elements()){
                local_json = {};

                $.each(metadata.elements(), function(index, submetadata) {
                    if(submetadata.elements()){
                        //recurse as needed
                        local_json[submetadata.name()] = parse_asset(submetadata);
                    } else {
                        local_json[submetadata.name()] = submetadata.value();
                    }

                    if(metadata.hasAttributes()){
                        $.each(metadata.attributes(), function(index, attribute){
                            if(attribute.value()){
                                local_json[attribute.name()] = attribute.value();
                            }
                        });
                    }

                    asset_metadata[local_name] = local_json;
                });
            } else {
                if(metadata.hasAttributes()){
                    $.each(metadata.attributes(), function(index, attribute){
                        if(attribute.value()){
                            asset_metadata[attribute.name()] = attribute.value();
                        }
                    });
                }

                asset_metadata[local_name] = metadata.value();
            }
        });
    }
    return asset_metadata;
}

function parse_command_xml(asset_xmlelement){
    // grabs all xml elements and attributes into JSON for easy access.
    // Nested elements simply get concatenated names.

    // same as parse_asset without certain asset parsing hacks. so, yeah, not ideal.

    if (typeof asset_xmlelement != "object"){
        return null;
    }

    var asset_metadata = {};

    if(asset_xmlelement.hasAttributes()){
        $.each(asset_xmlelement.attributes(), function(index, attribute){
            asset_metadata[attribute.name()] = attribute.value();
        });
    }

    if(asset_xmlelement.name()){
        var asset_name = '';
        asset_name = asset_xmlelement.name();

        if(asset_xmlelement.hasAttributes()){
            $.each(asset_xmlelement.attributes(), function(index, attribute){
                asset_metadata[attribute.name()] = attribute.value();
            });
        }
        if (asset_xmlelement.value()){
            asset_metadata[asset_name] = asset_xmlelement.value();
        }
    }


    $.each(asset_xmlelement.elements(), function(index, metadata) {

        var id_text = null;
        var local_name = '';

        local_name = metadata.name();

        if(metadata.elements()){
            local_json = {};

            $.each(metadata.elements(), function(index, submetadata) {
                if(submetadata.elements()){
                    //recurse as needed
                    local_json[submetadata.name()] = parse_command_xml(submetadata);
                } else {
                    local_json[submetadata.name()] = submetadata.value();
                }

                if(metadata.hasAttributes()){
                    $.each(metadata.attributes(), function(index, attribute){
                        if(attribute.value()){
                            local_json[attribute.name()] = attribute.value();
                        }
                    });
                }

                asset_metadata[local_name] = local_json;
            });
        } else {
            if(metadata.hasAttributes()){
                $.each(metadata.attributes(), function(index, attribute){
                    if(attribute.value()){
                        asset_metadata[attribute.name()] = attribute.value();
                    }
                });
            }

            asset_metadata[local_name] = metadata.value();
        }
    });
    return asset_metadata;
}

function asset_metadata_lookup(asset_xmlelement, label_data) {
    // does xpath queries for specific metadata needed for listing
    // handles cases where the xpath doesn't exist because the asset metadata is missing
    // (which usually means a bad file of some sort.  This can appear if a file upload is incomplete)

    var asset_metadata = {};

    // check main metadata
    if (asset_xmlelement.hasElement('value')){

        var asset_values = asset_xmlelement.elements('value');

        if (asset_values.length >= 4){
            asset_metadata['name'] = asset_values[0].value();
            asset_metadata['type'] = asset_values[1].value();
            asset_metadata['mtime']  = asset_values[2].value();
            asset_metadata['content_size'] = asset_values[3].value();
        } else {
            asset_metadata['name'] = "?";
            asset_metadata['type'] = "?";
            asset_metadata['mtime'] = "?";
            asset_metadata['content_size'] = "?";
        }
        if (!asset_values){
            asset_metadata['name'] = "?";
            asset_metadata['type'] = "?";
            asset_metadata['mtime'] = "?";
            asset_metadata['content_size'] = "?";
        }
        if (asset_metadata['name'] === null){asset_metadata['name'] = "?";}
        if (asset_metadata['type'] === null){asset_metadata['type'] = "?";}
        if (asset_metadata['mtime'] === null){asset_metadata['mtime'] = "?";}
        if (asset_metadata['content_size'] === null){asset_metadata['content_size'] = "?";}
    }

    if (asset_xmlelement.hasElement('@id')){
        asset_metadata['id'] = asset_xmlelement.value('@id');
    } else {
        asset_metadata['id'] = 'missing id';
    }

    if (asset_xmlelement.hasElement('@version')){
        asset_metadata['version'] = asset_xmlelement.value('@version');
    } else {
        asset_metadata['version'] = undefined;
    }

    asset_metadata['published'] = false;

    // check labels, if there are any
    if(label_data){
        if (label_data.hasElement('label')){
            $.each(label_data.elements(), function(index, label){
                if (label.value('@name')==="PUBLISHED"){
                    asset_metadata['published'] = true;
                }
            });
        }
    }

    return asset_metadata;
}

function check_published_status(asset_id) {
    var command_args = new arc.xml.XmlStringWriter();
    command_args.add("id", asset_id);

    var is_published = false;

    var command  = livearc_execute_handler("asset.label.list", command_args);

    if (command.hasElement('asset/label')){
        $.each(command.elements('asset/label'), function(index, label){
            if (label.value('@name') === "PUBLISHED"){
                is_published = true;
            }
        });
    }

    return is_published;
}


function download_asset_by_event(event) {
    var main_event = $(event.currentTarget.parentElement);
    var content_url = arc.mf.client.RemoteServer.attachmentContentUrl(main_event.data('assetid'), main_event.data('version'));
    var command = arc.mf.client.RemoteServer.download(content_url, main_event.data('filename'));
}


function expanded_row_display(target) {
    // handled by a popover for now

}

function get_asset_id(path){
    var args = new arc.xml.XmlStringWriter();
    args.add("id", "path="+path);
    var command = livearc_execute_handler("asset.get", args, true);
    var asset_id = -1;

    if (command._xml){
        var asset_info  = xmlToJson(command._xml);

        if(asset_info) {
            asset_id  = asset_info.asset["@attributes"].id;
        }
    } else {
        console.log('get_asset_id error',command,path);
    }
    return asset_id;
}

function get_asset_latest_version(asset_id){
    var args = new arc.xml.XmlStringWriter();
    args.add("id", asset_id);
    var command = livearc_execute_handler("asset.get", args, true);
    var version = -1;

    if (command._xml){
        var asset_info  = xmlToJson(command._xml);

        if(asset_info) {
            version  = asset_info.asset["@attributes"].version;
        }
    } else {
        console.log('get_asset_latest_version error',command,asset_id);
    }
    return version;
}

function query_asset(asset_path){

    var url_split = asset_path.split('/');

    show_asset_download_page("name="+asset_path,"/",url_split[url_split.length-1]);
}

function open_parent_namespace() {
    var current_path = get_current_path();

    current_path = current_path.replace("//", "/");
    var path_sections = current_path.split("/");

    var parent_path = "/"+path_sections.slice(0,path_sections.length-1).join("/");

    var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
    complex_query_namespace(parent_path, current_page_size, 1);
}


function update_pager_controls(current_page, object_count, last_page, page_size){
    var first_index = 0;
    var last_index = 0;

    if (current_page > 1){
        first_index = (current_page - 1) * page_size + 1;
    } else {
        first_index = 1;
    }
    last_index = first_index + page_size - 1;

    if (current_page == last_page){
        last_index = first_index + object_count - (last_page - 1) * page_size - 1;
    }

    $("#file-table").attr("data-last-page", last_page);
    // update buttons if needed
    $("#pager-firstpage-button").removeClass("disabled");
    $("#pager-previouspage-button").removeClass("disabled");
    $("#pager-nextpage-button").removeClass("disabled");
    $("#pager-lastpage-button").removeClass("disabled");

    if(current_page === 1){
        $("#pager-firstpage-button").addClass("disabled");
        $("#pager-previouspage-button").addClass("disabled");
    }

    if (current_page === last_page){
        $("#pager-nextpage-button").addClass("disabled");
        $("#pager-lastpage-button").addClass("disabled");
    }

    $("#pager-currentpage-view").text(first_index+ "-" + last_index + " of " + object_count);
}


function complex_query_namespace(namespace, page_size, page_number){

    // NaN can happen for some startup conditions.
    if (isNaN(page_size)){page_size = 10;}
    if (isNaN(page_number)){page_number = 1;}

    $("#file-table").attr("data-current-page",page_number);
    $("#file-table").attr("data-current-page-size",page_size);

    $("#pager-currentpage-view").html("<i class='fa fa-cog fa-spin'></i> computing");

    // callbacks for asset.namespace.list
    function complex_query_namespace_success(response){
        // note:    don't be tempted to use the 'cursor' agurment in the response. It only indexes assets.
        //              so life will get complicated if you have namespaces in the list

        var contents_list = xmlToJson(response._xml);

        var namespace_list = null;
        var asset_list = null;

        if (contents_list.hasOwnProperty("namespace")){
            namespace_list = contents_list.namespace;
        }

        if (contents_list.hasOwnProperty("asset")){
            asset_list = contents_list.asset;
        }
        var total_object_count = parseInt(contents_list.parent.assets["#text"]) + parseInt(contents_list.parent.namespaces["#text"]);

        populate_file_table_json(namespace_list,asset_list,contents_list.parent.name["#text"]);

        update_pager_controls(parseInt(contents_list.parent.page["#text"]), total_object_count, parseInt(contents_list.parent.last["#text"]), parseInt(contents_list.parent.size["#text"]));

        $("#refresh-filelist-button a i").removeClass("fa-spin");
    }

    function complex_query_namespace_failure(response){
        $('#alert_error_human_text').text('I had trouble trying to read that folder. See details below');
        $("#alert_error_detail_text").text('Response: '+response.message());

        var email_text = "mailto:help@pawsey.org.au?&subject=Data%20Website%20Error&body="+response.message();
        $("#error-email-link").attr('href',email_text);
        $('#myAlertMessageModal').modal('show');
        $("#refresh-filelist-button a i").removeClass("fa-spin");
    }

    $("#file-table").attr("data-current-namespace",namespace);

    if (page_number < 1){page_number = 1;}
    if (page_size < 10){page_size = 10;}

    namespace = namespace.replace("//", "/");

    var args = new arc.xml.XmlStringWriter();
    args.add("namespace",namespace);
    args.add("size",page_size);
    args.add("page",page_number);

    livearc_async_generic_handler("www.list", args, complex_query_namespace_success, complex_query_namespace_failure);
}


function count_number_of_directories_selected() {
    var selected_rows = index_selected_assets();
    var number_of_directories = 0;
    var number_of_files = 0;

    $.each(selected_rows, function(index, row){
        if($(row).hasClass("folder-row")){
            number_of_directories = number_of_directories + 1;
            number_of_files = number_of_files + parseInt($(row).data('nfiles'),10);
        } else if ($(row).hasClass("file-row")){
            number_of_files = number_of_files + 1;
        }
    });
    return number_of_directories;
}

// file selection control
function index_selected_assets() {
    // get a list of selected items.
    var selected_assets = [];

    $.each($("#file-table tbody tr"), function(index, tablerow){
        if( $(tablerow).hasClass("info") || $(tablerow).hasClass("danger") ){
            selected_assets.push(tablerow);
        }
    });
    return selected_assets;
}

function select_all_files() {
    $.each($("#file-table tbody tr"), function(index, tablerow){
        if( $(tablerow).hasClass("file-row") ){
            $(tablerow).addClass("info");
        }
    });
    update_selected_assets_info();

}

function select_all_folders() {
    $.each($("#file-table tbody tr"), function(index, tablerow){
        if( $(tablerow).hasClass("folder-row") ){
            $(tablerow).addClass("info");
        }
    });
    update_selected_assets_info();

}

function clear_all_selected_items() {
    var selected_assets = index_selected_assets();

    if(selected_assets.length > 0){
        $.each(selected_assets, function(index, asset){
            $(asset).removeClass("info");
            $(asset).removeClass("danger");
        });
    }
    update_selected_assets_info();
}



function share_selected_assets() {
    $("#share-files-done").text('working...');
    $("#share-files-done").fadeIn().removeClass("hidden");
    var selected_assets = index_selected_assets();

    if(selected_assets.length > 0){
        $.each(selected_assets, function(index, asset){
            if ($(asset).hasClass("file-row")){
                var share_result = share_asset($(asset).data('assetid'));
                $(asset).find("i.public-share-icon").removeClass('hidden');
            }
            if ($(asset).hasClass("folder-row")){
                share_namespace(get_current_path()+"/"+$(asset).data('pathinfo'));
            }
        });
    }

    $("#share-files-done").fadeOut().removeClass("hidden");
}


function share_namespace(path){
    var w4 = new arc.xml.XmlStringWriter();
    w4.add("where","namespace>='"+path+"'");

    w4.add("action","pipe");
    w4.push("service",["name","asset.label.add"]);
    w4.add("label","PUBLISHED");

    var share_data = livearc_execute_handler("asset.query", w4);
}


function unshare_namespace(path){
    var w4 = new arc.xml.XmlStringWriter();
    w4.add("where","namespace>='"+path+"'");

    w4.add("action","pipe");
    w4.push("service",["name","asset.label.remove"]);
    w4.add("label","PUBLISHED");

    var share_data = livearc_execute_handler("asset.query", w4);
}


function share_asset(id){
    var args = new arc.xml.XmlStringWriter();
    args.add("id",id);
    args.add("label","PUBLISHED");
    var share_result = livearc_execute_handler("asset.label.add", args);
    return share_result;
}


function unshare_asset(id){
    var args = new arc.xml.XmlStringWriter();
    args.add("id",id);
    args.add("label","PUBLISHED");
    var share_result = livearc_execute_handler("asset.label.remove", args);
    return share_result;
}


function unshare_selected_assets() {

    $("#unshare-files-done").text('working...');
    $("#unshare-files-done").fadeIn().removeClass("hidden");

    var selected_assets = index_selected_assets();

    if(selected_assets.length > 0){
        $.each(selected_assets, function(index, asset){
            if ($(asset).hasClass("file-row")){
                var share_result = unshare_asset($(asset).data('assetid'));
                $(asset).find("i.public-share-icon").addClass('hidden');
            }
            if ($(asset).hasClass("folder-row")){
                unshare_namespace(get_current_path()+"/"+$(asset).data('pathinfo'));
            }
        });
    }
    $("#unshare-files-done").fadeOut().removeClass("hidden");

}

function toggle_search_list_mode() {

}

function toggle_upload_mode() {
    $("#upload-files-interface").toggle();
}


function show_selected_items_menu() {
    $("#selected-items-button").addClass("command-item-active");
    $("#selected-items-group").fadeIn().addClass("command-group-active");
}

function close_selected_items_menu() {
    if($("#selected-items-button").hasClass("command-item-active")) {
        $("#selected-items-button").removeClass("command-item-active");
        $("#selected-items-group").removeClass("command-group-active");
    }
}


function toggle_selected_items_menu() {
    if($("#selected-items-button").hasClass("command-bar-title-active")){
        $("#selected-items-group").removeClass("command-group-active");
    } else {
        $("#selected-items-group").addClass("command-group-active");
    }
}

function show_move_help(event){
    event.stopPropagation();

    $("#move-items-button").popover('destroy');

    if(move_file_array.length > 0){
         $("#move-items-button").popover({
            placement: 'left',
            html:'true',
            content:function() {
                return $('#moving-file-popover-content').html();
            },
            title:'What am I moving?',
            trigger: 'manual'
        }).parent().delegate("#close-move-list","click", function(){
            $("#move-items-button").popover('destroy');
        });
    } else {
        $("#move-items-button").popover({
            placement: 'left',
            html:'true',
            content:function() {
                return $('#moving-file-popover-help-content').html();
            },
            title:'Help with Moving Files',
            trigger: 'manual'
        }).parent().delegate("#close-move-help","click", function(){
            $("#move-items-button").popover('destroy');
        });
    }
     $("#move-items-button").popover('show');
}

function cancel_current_move(event) {
    move_file_array = [];
    move_file_descriptor_array = [];

    $("#move-items-button").removeClass("command-item-underway").addClass("command-item");
    $("#move-target-button-label").html("here");
    $("#moving-items-count").html("0");
    $("#moving-items-count").addClass("hidden");

    $("#moving-file-list").empty();

    $("#moving-items-interface").fadeOut().addClass("hidden");
    event.stopPropagation();

    $("#move-items-button").popover('destroy');

    update_selected_assets_info();
}


function toggle_move_items_interface() {

    // if there are items in move array, show the UI to control the move. Otherwise, hide and reset the UI
    var selected_assets = index_selected_assets();
    move_file_array = [];
    move_file_descriptor_array = [];

    $.each(selected_assets, function(index, item){
        if ($(item).hasClass('file-row')){
            // this is an asset
            move_file_array.push($(item).data('assetid'));
            move_file_descriptor_array.push($(item).data('filename'));
        } else if ($(item).hasClass('folder-row')) {
            // this is a namespace
            move_file_array.push(get_current_path()+"/"+$(item).data('pathinfo'));
            move_file_descriptor_array.push('<i class="fa fa-folder-o"> '+$(item).data('pathinfo')+'</i>');
        }
    });


    $("#moving-items-count").html(move_file_array.length);
    $("#moving-items-count").removeClass("hidden");
    if (move_file_array.length > 0){
        $("#moving-items-interface").fadeIn().removeClass("hidden");
        $("#move-items-button").addClass("command-item-underway").removeClass("command-item");
        $("#moving-file-list").empty();
        $.each(move_file_descriptor_array, function(index, item){
            $("#moving-file-list").append("<p>"+item+"</p>");
        });
    } else {
        $("#move-items-button").removeClass("command-item-underway").addClass("command-item");
        $("#move-target-button-label").html("here");
        $("#moving-items-interface").fadeOut().addClass("hidden");
    }

    $("#move-items-button").popover('destroy');

}


function activate_move_selected_files() {

    //this function moves a list of asset IDs to a selected namespace
    var move_result = "";
    var args = "";

    $.each(move_file_array, function(index,item){
        if(typeof item === "number"){
            // asset.move :id NNNN :namespace "asodjashd"
            args = new arc.xml.XmlStringWriter();
            args.add("id",item);
            args.add("namespace",get_current_path());
            move_result = livearc_execute_handler("asset.move", args);

        } else {
            // asset.namespace.move :namespace "/projects/test test" :to "/projects/test test"
            args = new arc.xml.XmlStringWriter();
            args.add("namespace",item);
            args.add("to",get_current_path());
            move_result = livearc_execute_handler("asset.namespace.move", args);
        }
    });

    //cleanup
    move_file_array = [];
    toggle_move_items_interface();

    var current_page_size =  parseInt($("#file-table").attr("data-current-page-size"),10);
    complex_query_namespace(get_current_path(), current_page_size, 1);
}


function rename_namespace(full_path, new_name) {
    var args = new arc.xml.XmlStringWriter();
    args.add("name",new_name);
    args.add("namespace",full_path);
    var asset_data = livearc_execute_handler("asset.namespace.rename", args);
}


function rename_asset(asset_id, new_name) {
    var args = new arc.xml.XmlStringWriter();
    args.add("id",asset_id);
    args.add("name",new_name);
    var asset_data = livearc_execute_handler("asset.set", args);
}



function display_error_dialog(human_message, detailed_message, email_addendum){
        $('#alert_error_human_text').text(human_message);
        $("#alert_error_detail_text").text(detailed_message);

        var email_text = "mailto:help@pawsey.org.au?&subject=Data%20Website%20Error&body="+detailed_message+".  Extra detail:"+email_addendum;
        $("#error-email-link").attr('href',email_text);
        $('#myAlertMessageModal').modal('show');
}

// file download control
// Reminder:  If you make a change here, test this will all file type downloads
// we've seen some unexpected side effects from test changes that present as
// file type specific download failure.


var download_file = function download_file(url,index, filename) {

    var hiddenIFrameID = 'hiddenDownloader-'+index;
        var iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        iframe.onload = function() {
            // the iframe is only loaded if the download fails.
            var x = document.getElementById(hiddenIFrameID);

            var download_error = $('#hiddenDownloader-'+index).contents().find('tr').map(function() {
                // $(this) is used more than once; cache it for performance.
                var $row = $(this);

                // For each row that's "mapped", return an object that
                //  describes the first and second <td> in the row.
                return {
                    key: $row.find('td:nth-child(1)').text(),
                    value: $row.find('td:nth-child(2)').text()
                };
            }).get();


            if(download_error != ''){
                download_file_error(download_error);
            }
        };
        document.body.appendChild(iframe);
    }
    iframe.src = url+'&filename='+filename;
    return filename;
};


function download_file_error(response){

        var message = null;
        var cause = null;
        var filename = null;

        $.each(response, function(index, item){
            if(/error/i.test(item.key)){
                message = item.value;
            }
            if(/cause/i.test(item.key)){
                cause = item.value;
            }
            if(/target/i.test(item.key)){
                filename = item.value;
            }
        });

        var human_message = "I can't seem to download that file.";
        var detailed_message = "The server said:"+cause+". The Mediaflux error was: "+message;

        display_error_dialog(human_message, detailed_message, filename);

}

function archive_download_callback(response){
    var download_command = download_file(response,0,"archive.zip");
}


function download_archived_assets() {
    //

    var selected_assets = index_selected_assets();
    var query_string = '';

    if(selected_assets.length > 0){
        $.each(selected_assets, function(index, item){

            if ($(item).hasClass('file-row')){
                // this is an asset
                if(index < selected_assets.length-1){
                    query_string = query_string+'id='+ $(item).data('assetid') +' OR ';
                } else {
                    query_string = query_string+'id='+ $(item).data('assetid');
                }
            } else if ($(item).hasClass('folder-row')) {

                var local_current_path =  get_current_path();
                var local_item_name = $(item).data('pathinfo');

                if(index < selected_assets.length-1){
                    query_string = query_string+'namespace>=\'' + local_current_path+'/'+local_item_name +'\' OR ';
                } else {
                    query_string = query_string+'namespace>=\'' + local_current_path +'/'+local_item_name+'\'';
                }
            }

        });
    }
    query_string = query_string+'';

    var w2 = new arc.xml.XmlStringWriter();
    w2.add("where",query_string);
    w2.add("for","user");
    w2.add("format","zip");
    w2.add("include-destroyed","false");
    w2.add("include-version-numbers","false");
    w2.add("parts","content");

    var command_args = w2.document();
    var command = arc.mf.client.RemoteServer.execute("asset.archive.create", command_args,null, new arc.mf.client.OutputHandler(archive_download_callback));
}

function migrate_asset_online(asset_id){

        function migrate_asset_success(response){
                var migrate_info = xmlToJson(response._xml);
        }
        function migrate_asset_failure(response){
            console.log('migrate_asset_online error',response);
        }

        var args = new arc.xml.XmlStringWriter();
        args.add("id",asset_id);
        args.add("destination","online");

        livearc_async_generic_handler("asset.content.migrate", args, migrate_asset_success, migrate_asset_failure);
}


function download_selected_assets() {
    // download file/s to the local drive.  mediaflux returns results as mime-type 'attachment'.  Most browsers
    // will deposit the files in the default download location.  All downloads will be similtaneously launched.

        $("#download-indicator-text").html('<i class="fa fa-refresh fa-spin"></i>');
        $("#download-indicator").removeClass().addClass("c100 p00 small");

        function check_state_success(response){
            var file_info = xmlToJson(response._xml);
            $("#download-files-button").tooltip("show");
            setTimeout(function(){$("#download-files-button").tooltip('hide');}, 2000);
            migrate_asset_online(file_info.asset["@attributes"].id);
            park_asset_for_staged_download(file_info.asset["@attributes"].id, file_info.asset.name["#text"]);
        }
        function check_state_failure(response){

        }
        // if the asset has state=offline, it will be staged online and download triggered once staged
    var selected_assets = index_selected_assets();
        _global_download_size = selected_assets.length;

    if(selected_assets.length > 0){
        $.each(selected_assets, function(index, asset){
            var args = new arc.xml.XmlStringWriter();
            args.add("get-content-status","true");
            args.add("id",$(asset).data('assetid'));

            livearc_async_generic_handler("asset.get", args, check_state_success, check_state_failure);
        });
    }
}

function park_asset_for_staged_download(asset_id,filename){
        var datetext = new Date().toUTCString();
        _global_download_list.push({asset_id: asset_id});

        $("#table-download-log tbody").append("<tr class='progress-log-name-style' data-assetid='"+asset_id+"'><td>"+filename+"</td><td>"+datetext+"</td><td class='progress-status-item'><i class='fa fa-circle-o-notch fa-spin'></i> Preparing</td></tr>");
        // update_interface_for_pending_downloads();
        _global_staging_check_timer = setInterval(function () {update_interface_for_pending_downloads();}, 3000);

        update_interface_for_pending_downloads();
}


function update_interface_for_pending_downloads(){
    // iterates through all pending download files and checks state.  If state is online, starts the download
    function check_state_success(response){
        var file_info = xmlToJson(response._xml);

        // if the file is now online, initiate the download and update the relevant table row
        // SDF - included external content (eg Versity S3) that is "reachable" in order to prevent infinite hang
        if (file_info.asset.content.state["#text"] === "online" || file_info.asset.content.state["#text"] === "online+offline" || file_info.asset.content.state["#text"] === "reachable"){
            var remove_index = -1;
            $.each(_global_download_list,  function(index, asset){
                if(asset.asset_id == file_info.asset["@attributes"].id){
                        remove_index = index;
                }
            });

            if (remove_index > -1) {
                _global_download_list.splice(remove_index, 1);
            }
            download_asset_by_reference(file_info.asset["@attributes"].id, file_info.asset["@attributes"].version, file_info.asset.name["#text"]);
            $("#table-download-log tbody tr[data-assetid='" + file_info.asset["@attributes"].id + "']").find("td:last").html("Download started");
            $("#download-task-count").text(_global_download_list.length);

            var percentage = Math.round(_global_download_list.length/_global_upload_size*100);

            var percentage_label = "p"+percentage;
            $("#upload-indicator").removeClass().addClass("c100"+" "+percentage_label+" small");

            if (_global_download_list.length ===  0){
                $("#download-indicator-text").html('idle');
                $("#download-indicator").removeClass().addClass("c100 p100 small");
                _global_download_size = 0;
            }

        }
    }
    function check_state_failure(response){

    }


    if (_global_download_list.length > 0){
            $("#download-task-count").text(_global_download_list.length);
    } else {
            $("#download-task-count").text("0");
    }


    if (_global_download_list.length === 0) {
        // all done
        window.clearInterval(_global_staging_check_timer);
        _global_staging_check_timer = 0;
    } else {
        // check each pending download, get the expanded meta data
        $.each(_global_download_list, function(index, asset_info){
            var args = new arc.xml.XmlStringWriter();
            args.add("get-content-status","true");
            args.add("id",asset_info.asset_id);
            livearc_async_generic_handler("asset.get", args, check_state_success, check_state_failure);
        });
    }
}


function download_asset_by_reference(asset_id,version,filename){
    var content_url = arc.mf.client.RemoteServer.attachmentContentUrl(asset_id,version);
    // asset_id in download_file() is used to label the hidden frame. just needs to be unique in this session
    var download_command = download_file(content_url,asset_id,filename);
}

function update_current_task_total() {
    var total_tasks = _global_current_download_size +_global_current_upload_size;
    $("#toolbar-tasks-count").text(total_tasks);
}


function toggle_tasks_view(e){

    if (!$("#file-display").hasClass("hidden")){
        $("#file-display").addClass("hidden");
        $("#full-screen-task-view").removeClass("hidden");
    } else {
        $("#file-display").removeClass("hidden");
        $("#full-screen-task-view").addClass("hidden");
    }
}


function check_current_user() {
     var command = livearc_execute_handler("system.session.self.describe", null);
     var session_data = parse_asset(command.element('session'));
     update_profile_page(session_data.user);
}


function tools_download_callback(response){
    var download_command = download_file(response,0,"archive.zip");
    quiet_logoff();
}


// file list
function get_filelist_for_current_state(){
    console.log('get_filelist_for_current_state');
    $("#export-text-list-button").popover('destroy');

    // show popup`x
    $("#export-text-list-button").popover({
        placement: 'left',
        html:'true',
        content:function() {
            return $('#generate-filelist-content').html();
        },
        title:'Export File List',
        trigger: 'manual'
        }).parent().delegate("#cancel-download-list","click", function(){
            $("#export-text-list-button").popover('destroy');
        });
    $("#export-text-list-button").popover('show');
}


function check_for_list_type(){
    console.log('check_for_list_type');
    var current_path = $("#file-table").attr("data-current-namespace");
    var current_search = $("#file-table").attr("data-current-search");
    console.log('current_path',current_path);
    console.log('current_search',current_search);
    if (current_path.length){
        console.log('current_path is',current_path);
    }
    if (current_search !== undefined){
        if(current_search.length > 0){
            console.log('current_search is valid');
        }
    }
}


function get_filelist_as_textfile(path){
    console.log('get_filelist_as_csv for',path);

    // start spinner
    $("#filelist-spinner").addClass("fa-spin");

    // how many items?

    // end spinner, download file
    $("#filelist-spinner").removeClass("fa-spin");

}


function get_queryresult_as_textfile(querystring){
    console.log('get_queryresult_as_csv for',querystring);

}


// Utility routines.

function get_cookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) != -1;
};

function sanitise_string_for_xml(string){
    var clean_string = "";
    clean_string = string.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
    return clean_string;
}
