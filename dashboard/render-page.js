function render_mainview_for_user(username){
    $.get('project-browser.mustache', function(template) {
        var rendered = Mustache.render(template, {username: username});
        $('#main-project-browser-view').html(rendered);
        
//        var filter = '*';
//        project_list_filter(filter, render_project_browser_for_list, render_error_message);
        
//        var projectName = linkObject.id;
        var projectName = null;
        var showMembers = true;

        project_get(projectName, showMembers, render_project_browser_for_list, render_error_message);
    });
    $.get('project-detail.mustache',function(grid_template){
        var rendered = Mustache.render(grid_template, {projectName: ''});
        $('#main-project-detail-view').html(rendered);
    });
    $('#detailpanel').hide();
}


function render_project_details(details) {
//    console.log('details', details);
    var projectName = details.name["#text"];
    var projectDesc = details.name.description["#text"];
    var administer = details.name.administer["#text"];
    
//    console.log('administer', administer);
    
    var allowAdmin = null;
    
    if (administer === "true") {
        allowAdmin = true;
    }
    
    var public = false;
    var userCount = 0;
    
//    var quota = details.name.quota["#text"];
//    var quota = quotaBytes_to_GB(details.name.quota["@attributes"].bytes);
    
    var used = details.name.used["@attributes"].bytes;
    var quota = details.name.quota["@attributes"].bytes;
    
//    console.log('used', used);
//    console.log('quota', quota);
    
    var projectpercent = (used/quota)*100.;
    projectpercent = projectpercent.toFixed(2);
    

    var roles = details.name.members.role;
    
    var actors = '[';  

    $.each(roles, function( index, value ) {
        // Get the role name, removing the prefix
        role = value["#text"];
        var roleArray = role.split(":");
        role = roleArray[1];

        // If an "actor" property is found 
        if (value.hasOwnProperty("actor")) {
            
            // An array of Actors
            if (value.actor.hasOwnProperty("length")){
                // For each actor
                $.each(value.actor, function( nestedIndex, nestedValue ) {
                    var actor = nestedValue["#text"];
                    var actorArray = actor.split(":");
                    var actorSuffix = actorArray[1];

                    if (actorSuffix === "public") {
                        public = true;
                    }
                    else {
                        if (userCount > 0) {
                            actors = actors + ', ';
                        }

                        actors = actors + '{ "rowident": "' + userCount + 
                                          '", "' + role + '": "' + role + 
                                          '", "role": "' + role + '", "name": "' + actor + '" }';
                        userCount++;
                    }

                });
            }
            // Single Actor
            else {
                var actor = value.actor["#text"];
                var actorArray = actor.split(":");
                var actorSuffix = actorArray[1];

                if (actorSuffix === "public") {
                    public = true;
                }
                else {
                    if (userCount > 0) {
                        actors = actors + ', ';
                    }

                    actors = actors + '{ "rowident": "' + userCount + 
                                      '", "' + role + '": "' + role + 
                                      '", "role": "' + role + '", "name": "' + actor + '" }';
                    
                    userCount++;
                }

            }
            
        }
//        else {
//            console.log('NO ACTORS');
//        }

    });

//var actors =     
//'[ { "role": "Administrator", "name": "Moe" }, { "role": "Administrator", "name": "Larry" }, { "role": "Read-only", "name": "Curly" } ]';  

    actors = actors + ']';
//    console.log('actors', actors);
    
    var actorsJSON = JSON.parse(actors);   

//    console.log('actorsJSON', actorsJSON);
    
    $.get('project-detail.mustache',function(grid_template){
        var rendered = Mustache.render(grid_template, {projectName: projectName, projectDesc: projectDesc, allowAdmin: allowAdmin, actors: actorsJSON, userCount: userCount, public: public, projectpercent: projectpercent});
        $('#main-project-detail-view').html(rendered);
        
        // Required for select input fields to render
        $('.selectpicker').selectpicker({
          size: 3
        });
        
    });
    
}


function grant_role(project, name, role){
//    console.log("grant_role", project + ", " + name + ", " + role);
    
    var administer = null;
    var readonly = null;
    var readwrite = null;
    
    if (role === "administer") {
        administer = name;
    }
    if (role === "readwrite") {
        readwrite = name;
    }
    if (role === "readonly") {
        readonly = name;
    }

    // Grant user role to Project
    project_grant(administer, project, readonly, readwrite, updated_project, render_error_message);
}


function revoke_role(project, name, role){
//    console.log("revoke_role", project + ", " + name + ", " + role);
    
    var administer = null;
    var readonly = null;
    var readwrite = null;
    
    if (role === "administer") {
        administer = name;
    }
    if (role === "readwrite") {
        readwrite = name;
    }
    if (role === "readonly") {
        readonly = name;
    }
    
    // Revoke user role from Project
    project_revoke(administer, project, readonly, readwrite, updated_project, render_error_message);
}


function update_role(project, name, initialRole, newRole){
//    console.log("update_role", project + ", " + name + ", " + initialRole+ ", " + newRole);

    // Revoke user's existing role
    revoke_role(project, name, initialRole);
    
    // Grant user new role
    grant_role(project, name, newRole);
}

function display_add_role(project, name, role) {
    $("#addMemberRoleProject").val(project);
    $("#addMemberRoleUser").val(name);

    // Set selectList selection and refresh
    $("#addMemberRoleSelect").val(role);
    $('.addMemberRoleSelect').selectpicker('refresh');
}


function display_modify_role(project, name, role) {
    $("#modifyMemberRoleProject").val(project);
    $("#modifyMemberRoleUser").val(name);
    $("#modifyMemberRole").val(role);

    // Set selectList selection and refresh
    $("#modifyMemberRoleSelect").val(role);
    $('.modifyMemberRoleSelect').selectpicker('refresh');
}
function updated_project(resultObject) {
    var project = $("#selectedProjectName").val();

    if (project != "undefined") {
        // Refresh Project detail display
        var showMembers = true;
        project_get(project, showMembers, render_project_details, render_error_message);
    }
}
function render_project_browser_for_list(list){
    // if list.project otherwise list is project object (for case of 1)
    
    var projectpercent = parseInt(0);
    var projectcount = parseInt(0);
    var totalquota = parseInt(0);
    var totalused = parseInt(0);
    var totalpercent = parseInt(0);

//    var sysmessage = "Add Filter by Project, User, Institution, Store (Icon/Clay)";

    // No records found
    if(list.name == null) {
        totalquota = parseInt(0);
        totalused = parseInt(0);

        // Set running total fields not already populated
        projectcount = parseInt(0);
        projectpercent = parseInt(0);

        // Format to remove unnecessary decimals
        projectpercent = projectpercent.toFixed(2);
    }
    // Record/s found
    else {
        // If result is more than one record (Array)
        if (list.name.hasOwnProperty("length")){
            $.each(list.name, function( index, value ) {
                
                if(value.mounted["#text"] === "true"){
                    
                    var quota = parseInt(0);
                    var used = parseInt(0);
                    
                    value.projectName = value["#text"];
                    
                    if(value.hasOwnProperty("quota")){
                        quota = parseInt(value.quota["@attributes"].bytes);
                        value.quotaInGB = quotaBytes_to_GB(quota);
                        totalquota = totalquota + quota;
                    }

                    if(value.hasOwnProperty("used")){
                        used = parseInt(value.used["@attributes"].bytes);
                        value.usedInGB = quotaBytes_to_GB(used);
                        totalused = totalused + used;
                    }

                    // Check for divide by zero error
                    if (quota > 0) {
                        value.projectpercent = used/quota*100.;
                    }
                    else {
                        value.projectpercent = parseInt(0);
                    }
                    
                    // Format to 2 decimal points
                    value.projectpercent = value.projectpercent.toFixed(2);
                    
                    value.rowident = index;
                    
                }

                // Add to running totals
                projectcount = projectcount + parseInt(1);
                
            });
        }
        // If result is one record (non-Array)
        else {
            if (list.name.hasOwnProperty("name")){

                var quota = parseInt(0);
                var used = parseInt(0);
                
                list.name.projectName = list.name["#text"];
                
                        
                if(list.name.hasOwnProperty("mounted")){
                    if(list.name.mounted["#text"] === "true") {

                        // There is only one record, so totals match result values
                        if(list.name.hasOwnProperty("quota")){
                            quota = parseInt(list.name.quota["@attributes"].bytes);
                            list.name.quotaInGB = quotaBytes_to_GB(quota);
                            totalquota = quota;
                        }

                        if(list.name.hasOwnProperty("used")){
                            used = parseInt(list.name.used["@attributes"].bytes);
                            list.name.usedInGB = quotaBytes_to_GB(used);
                            totalused = used;
                        }

                    }
                }

                // Set running total fields not already populated
                projectcount = parseInt(1);
                
                // Check for divide by zero error
                if (quota > 0) {
                    projectpercent = used/quota*100.;
                }
                else {
                    projectpercent = parseInt(0);
                }
                    
                // Format to 2 decimal points
                projectpercent = projectpercent.toFixed(2);
                
                list.name.rowident = 0;
            }
        }
    }

    var storageunits = "GB";
    
    // Only calculate/format totals if non-zero
    if(totalquota>0) {
        // Calculate total percentage using unmodified totals
        totalpercent = totalused/totalquota*100;
        totalpercent = totalpercent.toFixed(2);
        
        // Convert from bytes to Units
        totalquota = quotaBytes_to_GB(totalquota);
        totalused = quotaBytes_to_GB(totalused);
    }
    
    
    var searchStr = $("#input-project-filter-field").val();
    
    $.get('project-browser.mustache',function(grid_template){
        var rendered = Mustache.render(grid_template, {projects: list.name, projectpercent: projectpercent});
        $('#main-project-browser-view').html(rendered);
        
        // Configures the DataTable component for sorting, etc. - turned-off Searching to use the custom filtering input fields
        $('#example').DataTable( {
          "searching": true,
          "scrollY": "310px",
//          "scrollCollapse": true,   
          "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
          "pagingType": "full_numbers",
//          "pageLength": 2,
        } );        
        
        // init action buttons
        $("#project-filter-clear-button").click(function(){
            clear_search_field();
            // show_hide_refine_search(false);
//            show_hide_bootstrap_alert_message(false, null);
        });
        $("#project-filter-clear-button").hide();
        $("#project-filter-execute-button").click(function(){
            simple_text_search($("#input-project-filter-field").val(), $("#file-table").attr("data-current-page-size"),1);
        });

        $("#input-project-filter-field").keydown(function(event) {validate_search_text(event);});
        
        $("#input-project-filter-field").val(searchStr);
        
    });
    
//    console.log('projects', list.project);
    
    var systemDataStr = '{"projectcount": "' + projectcount + '", ' +
                        '"totalquota": "' + totalquota + '", ' +
                        '"totalused": "' + totalused + '", ' +
                        '"totalpercent": "' + totalpercent + '", ' +
                        '"storageunits": "' + storageunits + '" ' +
                      '}';
    
//    console.log('systemDataStr', systemDataStr);

    var systemdata = JSON.parse(systemDataStr);   
    
    $.get('system-overview.mustache',function(grid_template){
        var rendered = Mustache.render(grid_template, {systemdata: systemdata});
        $('#main-system-view').html(rendered);
        
//        console.log($("#input-project-filter-field"));
    });
    
}


function render_footer(){
    render_common_footer("Pawsey - Data Project Dashboard");
}

